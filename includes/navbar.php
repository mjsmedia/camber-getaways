<div id="loader">
    <div class="artwork">
        <?php if($classyMode != false){echo'
        <img src="/assets/questlogo-mobile.png" class="bounce" alt="Loading Logo">
        ';}else{echo'
        <img src="/assets/menulogo-mobile.png" class="bounce" alt="Loading Logo">
        ';}?>
        <img class="dots" src="/assets/loader.svg">
    </div>
</div>
<div class="container-fluid menubar <?php if($menuTransparent == true){echo'menubarTransparent';}?> fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-2">
                <a href="/">
                    <?php if($classyMode != false){echo'
                    <img class="logo-large" src="/assets/questlogo-large.png" alt="Logo Large">
                    <img class="logo-mobile" src="/assets/questlogo-mobile.png" alt="Logo Small">
                    ';}else{echo'
                    <img class="logo-large" src="/assets/menulogo-large.png" alt="Logo Large">
                    <img class="logo-mobile" src="/assets/menulogo-mobile.png" alt="Logo Small">
                    ';}?>
                    <img class="logo-mobile-light" src="/assets/menulogo-mobile-light.png" alt="Logo Small Light">
                </a>
                <button class="mobilemenutoggle menu-btn">Menu</button>
            </div>
            <div class="col-lg-10 d-none d-lg-block">
                <ul class="menu-items float-right">
                    <a href="/" <?php if($pageSlug == 'home'){echo'class="active"';}?>><li>Home</li></a>
                    <a href="/holiday-park" <?php if($pageSlug == 'parkdean'){echo'class="active"';}?>><li>Camber Holiday Park</li></a>
                    <a href="https://questcottages.co.uk/" <?php if($pageSlug == 'holiday-homes'){echo'class="active"';}?>><li>Holiday Homes</li></a>
                    <a href="/contact" <?php if($pageSlug == 'contact'){echo'class="active"';}?>><li>Contact</li></a>
                    <a target="_blank" href="https://www.mystayplanner.com/cambergetaways/"><li class="guest-login"><i class="fas fa-sign-in-alt"></i> Guest Login</li></a>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php if($classyMode != true){
    if($menuTransparent != true){
        echo'<div class="menu-spacer"></div>';
    }
}?>

<nav class="pushy pushy-left">
    <div class="pushy-content">
        <ul>
            <li class="pushy-link<?php if($pageSlug == 'home'){echo' active';}?>"><a href="/">Home</a></li>
            <li class="pushy-link<?php if($pageSlug == 'parkdean'){echo' active';}?>"><a href="/holiday-park">Camber Holiday Park</a></li>
            <li class="pushy-link<?php if($pageSlug == 'holiday-homes'){echo' active';}?>"><a href="https://questcottages.co.uk/">Holiday Homes</a></li>
            <li class="pushy-link<?php if($pageSlug == 'contact'){echo' active';}?>"><a href="/contact">Contact</a></li>
            <li class="pushy-link"><a target="_blank" href="https://www.mystayplanner.com/cambergetaways/"><i class="fas fa-sign-in-alt"></i> Guest Login</a></li>
        </ul>
    </div>
</nav>
<div class="site-overlay"></div>

<?php if($classyMode == 'large'){
    echo'<div class="classy-menu-spacer-large"></div>';
}elseif($classyMode == 'small'){
    echo'<div class="classy-menu-spacer-small"></div>';
}?>