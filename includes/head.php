<?php
if(empty($pageTitle)){$pageTitle = '';}else{$pageTitle = $pageTitle.' - ';}

if(empty($pageTitleOverride)){
    $fullPageTitle = $pageTitle.$siteTitle;
}else{
    $fullPageTitle = $pageTitleOverride;
}

if(empty($seoMetaURL)){
    $seoMetaURL = $currentUrl;
}
if(empty($seoMetaImageURL)){
    $seoMetaImageURL = $rootUrl.'/images/seo/default.jpg';
}

if($seoActive === true){
    $metaTags = '
    <meta name="description" content="'.$seoMetaDescription.'"/>
    <meta name="robots" content="noodp"/>
    <link rel="canonical" href="'.$seoMetaURL.'"/>
    <meta property="og:locale" content="en_GB"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="'.$fullPageTitle.'"/>
    <meta property="og:description" content="'.$seoMetaDescription.'"/>
    <meta property="og:url" content="'.$seoMetaURL.'"/>
    <meta property="og:image" content="'.$seoMetaImageURL.'"/>
    ';
}else{
    $metaTags = '';
}
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $fullPageTitle;?></title>
    <?php echo $metaTags;?>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/pushy.min.css">
    
    <link href="https://fonts.googleapis.com/css?family=Pacifico|Roboto+Condensed:400,400i,700,700i|Roboto+Slab:100,300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i|Raleway:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    
    <link rel="stylesheet" href="/css/animate.min.css">
    <link rel="stylesheet" href="/css/flatpickr.min.css">
    <link rel="stylesheet" href="/css/fancybox.min.css">
    
    <link rel="stylesheet" href="/css/navbar.min.css">
    <link rel="stylesheet" href="/css/typography.min.css">
    <link rel="stylesheet" href="/css/components.min.css?v=2">
    <link rel="stylesheet" href="/css/stylesheet.min.css?v=2">
    <link rel="stylesheet" href="/css/mjsfont.min.css">
    <link rel="stylesheet" href="/css/graphite.min.css">
    
    <link rel="icon" type="image/png" href="/assets/favicon2.png">
    
    <?php if($classyMode != false){
        echo '<link rel="stylesheet" href="/css/classy.min.css">';
    }?>
    
    <!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-W52X57R');</script>

<!-- End Google Tag Manager -->
</head>