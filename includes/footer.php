<div class="container-fluid footer background-blue">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 text-center text-md-left">
                <ul class="footer-menu">
                    <a href="/search?caravan=1"><li>Caravans</li></a>
                    <a href="/search?mini-lodge=1&full-lodge=1"><li>Lodges</li></a>
                    <a href="/search?chalet=1"><li>Chalets</li></a>
                    <a href="https://questcottages.co.uk/"><li>Holiday Homes</li></a>
                </ul>
            </div>
            <div class="col-12 col-md-3 text-center text-md-left">
                <ul class="footer-menu">
                    <a href="/caravans/"><li>Caravans for Sale</li></a>
                    <a href="/local-attractions"><li>Local Attractions</li></a>
                    <a href="/dog-friendly"><li>Dog Friendly</li></a>
                    <a href="/owners"><li>Owners</li></a>
                </ul>
            </div>
            <div class="col-12 col-md-3 text-center text-md-left">
                <ul class="footer-menu">
                    <a href="/blog"><li>Blog</li></a>
                    <a href="/reviews"><li>Reviews</li></a>
                    <a href="/contact"><li>Contact us</li></a>
                </ul>
            </div>
            <div class="col-12 col-md-3 mt-4 mt-md-0 text-center text-md-right">
                <a href="/">
                    <?php if($classyMode != false){echo'
                    <img class="footerlogo" src="/assets/questfooterlogo.png" alt="Footer Logo">
                    ';}else{echo'
                    <img class="footerlogo" src="/assets/footerlogo-light.png" alt="Footer Logo">
                    ';}?>
                </a>
            </div>
        </div>
        <div class="row credit">
            <div class="col-12 col-md-8 text-center text-md-left mb-4">
                <ul class="footer-small-menu">
                    <li><a href="/terms">Terms and Conditions</a></li>
                    <li><a href="/policies/privacy">Privacy Policy</a></li>
                    <li><a href="/policies/cancellation">Cancellation / Refund Policy</a></li>
                    <li><a href="/policies/delivery">Delivery Policy</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 text-center text-md-right">
                <ul class="footer-small-menu">
                    <li><a target="_blank" href="https://www.facebook.com/Cambergetaways/"><i class="fab fa-facebook"></i></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/cambergetaways/"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid footer footer-darkblue">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center text-md-left mb-4">
                <p>Copyright Camber Getaways &copy; <?php echo date('Y');?></p>
                <p class="mjs-credit">Made with <i class="mjs-heart"></i> by <a href="https://mjsmedia.co.uk" target="_blank">MJS Media</a></p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
                <ul class="accepted-cards">
                    <li><img src="/assets/card-mastercard.png" alt="Mastercard"></li>
                    <li><img src="/assets/card-maestro.png" alt="Maestro"></li>
                    <li><img src="/assets/card-visa.png" alt="Visa"></li>
                    <li><img src="/assets/card-jcb.png" alt="JCB"></li>
                </ul>
            </div>
        </div>
    </div>
</div>