<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W52X57R"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->

<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/jqueryui.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/pushy.min.js"></script>
<script src="/js/flatpickr.min.js"></script>
<script src="/js/fancybox.min.js"></script>

<?php
$arrivalI = 0;
$arrivalF = '';

foreach ($arrivalDays as &$day){
    if($arrivalI > 0){
        $arrivalF .= ' || date.getDay() === '.$day;
    }else{
        $arrivalF .= 'date.getDay() === '.$day;
    }
    $arrivalI ++;
}


$departureI = 0;
$departureF = '';

foreach ($departureDays as &$day){
    if($departureI > 0){
        $departureF .= ' || date.getDay() === '.$day;
    }else{
        $departureF .= 'date.getDay() === '.$day;
    }
    $departureI ++;
}
?>

<script>
    flatpickr(".datePickerStart", {
        minDate: 'today',
        "disable": [
            <?php echo getArrivalClosedDays();?>
            <?php if(empty($arrivalDays)){
    echo '';
}else{
    echo'
    function(date) {
                // return true to disable
                return (
                    '.$arrivalF.'
                );
                //return (date.getDay() === 0);

            }
    ';
}?>
        ],
        "locale": {
            "firstDayOfWeek": 1 // start week on Monday
        }
    });
</script>

<script src="/js/scrollreveal.min.js"></script>
<script>
    window.sr = ScrollReveal();
    sr.reveal('.scrollReveal',{scale:1});
</script>

<script src="/js/navbarScroll.min.js"></script>

<script src="/js/fittext.min.js"></script>
<script>
    jQuery("#responsive_headline").fitText(2, { minFontSize: '20px', maxFontSize: '100px' });
</script>
<script>

    window.onload = function () { $("#loader").fadeOut(); }
</script>

<script>
    if(MINNIGHTS == undefined){
        var MINNIGHTS = 2;
    }
    
    var startDate = document.getElementById('startDate').value;
    var minDate = new Date(startDate);
    var maxDate = new Date(startDate);
    if(MINNIGHTS == 3){
        minDate.setDate(minDate.getDate() + 3);
    }else{
        minDate.setDate(minDate.getDate() + 2);
    }
    maxDate.setDate(maxDate.getDate() + 28);
    console.log(maxDate);

    flatpickr(".datePickerEnd", {
        minDate: minDate,
        maxDate: maxDate,
        "disable": [
            <?php echo getArrivalClosedDays();?>
            <?php if(empty($departureDays)){
    echo '';
}else{
    echo'
                    function(date) {
                                // return true to disable
                                return (
                                    '.$departureF.'
                                );
                                //return (date.getDay() === 0);

                            }
                    ';
}?>
            ,startDate],
        //"disable": [startDate],
        "locale": {
            "firstDayOfWeek": 1 // start week on Monday
        }
    });
</script>

<script>
    function unlockDeparture(){
        
        
        document.getElementById("endDate").placeholder = "Please Select";
        document.getElementById("endDate").disabled = false;
        $("#endDate").removeClass("custom-disabled");

        var startDate = document.getElementById('startDate').value;
        var minDate = new Date(startDate);
        var maxDate = new Date(startDate);
        if(MINNIGHTS == 3){
            minDate.setDate(minDate.getDate() + 3);
        }else{
            minDate.setDate(minDate.getDate() + 2);
        }
        maxDate.setDate(maxDate.getDate() + 28);
        console.log(maxDate);

        flatpickr(".datePickerEnd", {
            minDate: minDate,
            maxDate: maxDate,
            "disable": [
                <?php echo getArrivalClosedDays();?>
                <?php if(empty($departureDays)){
    echo '';
}else{
    echo'
                    function(date) {
                                // return true to disable
                                return (
                                    '.$departureF.'
                                );
                                //return (date.getDay() === 0);

                            }
                    ';
}?>
                ,startDate],
            //"disable": [startDate],
            "locale": {
                "firstDayOfWeek": 1 // start week on Monday
            }
        });
    }
    function validateDeparture(){
        $("#endDate").removeClass("custom-form-invalid");
    }
</script>

<script src="//code.tidio.co/zfqdbr83afq8mhohhhbo58ox07hszzzz.js"></script>