<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Camber Sands';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Camber Sands boasts one of the most beautiful beaches in Britain with golden sand and shingle stretching for near 7 miles! Camber beach is dog friendly all year round, however between May and September they have restricted access.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(/images/beach2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Camber Sands</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('0D4O7BN12XM');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="half-half background-yellow background-stitch-white">
            <div class="image-half" style="background-image: url(/images/beach3.jpg)"></div>
            <div class="text-half">
                <div class="container">
                    <?php graphite_pageSection('BY0EBLNQZ3T');?>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row">
                    <?php graphite_gallery('MK7KCT1OSC0');?>
                </div>
            </div>
        </div>
        <div class="container-fluid background-default map-section">
            <div id="map"></div>
            <script>
                var map;
                function initMap() {
                    var location = {lat: 50.9357278, lng: 0.7950803};
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: location,
                        zoom: 15.78
                    });
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABc5r_DEMHXtnMJPHsABqg1pHwaSlaf_k&callback=initMap"
                    async defer></script>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php
        graphite_GraphiteFooter_AddItem('Edit Gallery','gallery/album/?id=MK7KCT1OSC0');
        graphite_GraphiteFooter();
        ?>
    </body>
</html>