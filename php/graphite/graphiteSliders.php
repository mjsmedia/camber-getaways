<?php
function graphite_slider_slides($sliderid){
    
    graphite_GraphiteFooter_AddItem('Edit Slider','sliders/edit/?id='.$sliderid);
    
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from sliders_slides WHERE sliderid = '$sliderid'";
    $result = $conn->query($query);
    $count = 0;
    
    while($row = $result->fetch_array()){
        $count ++;
        if($count == 1){$active = 'active';}else{$active = '';}
        if(!empty($row[primaryButtonLabel])){
            $primaryButton = '<a href="'.$row[primaryButtonLink].'" class="button">'.$row[primaryButtonLabel].'</a>';
        }else{
            $primaryButton = '';
        }
        if(!empty($row[secondaryButtonLabel])){
            $secondaryButton = '<a href="'.$row[secondaryButtonLink].'" class="button-text">'.$row[secondaryButtonLabel].'</a>';
        }else{
            $secondaryButton = '';
        }
        echo '
        <div class="carousel-item hero hero-75 '.$active.'" style="background: linear-gradient(rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)),url('.graphite_getImagePath().'uploads/'.$row[imageid].'-full.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center">
                        <h1 class="mb-2">'.$row[title].'</h1>
                        <p class="mb-4">'.$row[content].'</p>
                        '.$primaryButton.'
                        '.$secondaryButton.'
                    </div>
                </div>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
}
function graphite_slider_indicators($sliderid){
    
    if( graphite_database_resultCount('sliders_slides', 'sliderid', $sliderid) > 1){
        //Init connection
        $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

        // check connection
        if ($conn->connect_errno) {
            die("Connect failed: ".$conn->connect_error);
        }

        $query = "SELECT * from sliders_slides WHERE sliderid = '$sliderid'";
        $result = $conn->query($query);
        $count = 0;

        while($row = $result->fetch_array()){
            if($count == 0){$active = ' class="active"';}else{$active = '';}
            echo '
            <li data-target="#'.$sliderid.'" data-slide-to="'.$count.'"'.$active.'></li>
            ';
            $count ++;
        }
        mysqli_close($conn);
    }
}
function graphite_slider_arrows($sliderid){
    
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from sliders_slides WHERE sliderid = '$sliderid'";
    $result = $conn->query($query);
    $count = 0;
    
    while($row = $result->fetch_array()){
        $count ++;
    }
    mysqli_close($conn);
    
    if($count > 1){
        echo'
        <a class="carousel-control-prev" href="#'.$sliderid.'" role="button" data-slide="prev">
            <span class="lnr lnr-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#'.$sliderid.'" role="button" data-slide="next">
            <span class="lnr lnr-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
        ';
    }
}

function graphite_slider($sliderid){
    echo'
    <div id="'.$sliderid.'" class="carousel slide" data-ride="carousel" data-pause=false data-interval="7000">
            <ol class="carousel-indicators">';
                graphite_slider_indicators($sliderid);
            echo'</ol>
            <div class="carousel-inner">';
                graphite_slider_slides($sliderid);
            echo'</div>';
            graphite_slider_arrows($sliderid);
        echo'</div>';
    
}
?>