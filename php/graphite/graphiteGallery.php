<?php
function graphite_gallery($album){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from albums_images WHERE albumid = '$album'";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        $image = graphite_getImagePath().'gallery/'.$row[imageid].'.jpg';
        $thumbnail = graphite_getImagePath().'gallery/'.$row[imageid].'-thumb.jpg';
        
        echo '
        <div class="col-6 col-md-4 col-lg-3">
            <div class="gallery-image" style="background-image:url('.$thumbnail.');" data-fancybox="gallery" data-caption="" href="'.$image.'">
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
}
?>