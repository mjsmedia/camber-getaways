<?php
function graphite_blogPostValue($postid,$type){
    switch($type){
        case 'publishDate':
            return date($GLOBALS['GENERAL_SETTINGS']->get('general', 'dateFormat'), strtotime(graphite_database_lookupValue('blog', 'slug', $postid, 'publishDate')));
            break;
        case 'featuredImage':
            return graphite_getImagePath().'uploads/'.graphite_database_lookupValue('blog', 'slug', $postid, 'imageid').'-full.jpg';
            break;
        case 'metaImageURL':
            if(!empty(graphite_database_lookupValue('blog', 'slug', $postid, 'metaImageUrl'))){
                return graphite_database_lookupValue('blog', 'slug', $postid, 'metaImageUrl');
            }else{
                if(!empty(graphite_database_lookupValue('blog', 'slug', $postid, 'imageid'))){
                    return graphite_getImagePath().'uploads/'.graphite_database_lookupValue('blog', 'slug', $postid, 'imageid').'-seo.jpg';
                }else{
                    return '';
                }
            }
            break;
        default:
            return graphite_database_lookupValue('blog', 'slug', $postid, $type);
            break;
    }
}

function graphite_blogPosts($category){
    if(isset($category)){
        $WHERE = "WHERE status = 1 AND category = '".$category."'";
    }else{
        $WHERE = "WHERE status = 1";
    }
    
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }
    
    $query = "SELECT * from blog ".$WHERE." ORDER BY publishDate DESC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        $title = $row[title];
        $date = date($GLOBALS['GENERAL_SETTINGS']->get('general', 'dateFormat'), strtotime($row[publishDate]));
        $url = $GLOBALS['GRAPHITE_SETTINGS']->get('blog', 'archiveLocation').$row[slug];
        $author = $row[author];
        $thumbnail = graphite_getImagePath().'uploads/'.$row[imageid].'-thumbnail.jpg';
        $custom1 = $row[customField1];
        $custom2 = $row[customField2];
        if(!empty($row[summary])){
            if(strlen($row[summary]) > 350){
                $summaryContent = substr($row[summary], 0, 350).'...';
            }else{
                $summaryContent = $row[summary];
            }
            $summary = '<p>'.$summaryContent.'</p>';
        }else{
            $summary = '';
        }
        
        if(!empty($row[imageid])){
            echo '
            <a href="'.$url.'">
                <div class="blog-post">
                    <div class="row">
                        <div class="col-12 col-sm-4 col-lg-3">
                            <img src="'.$thumbnail.'" alt="'.$title.'">
                        </div>
                        <div class="col-12 col-sm-8 col-lg-9 text-center text-sm-left">
                            <p class="title">'.$title.'</p>
                            <p class="date">'.$date.'</p>
                            <p class="summary">'.$summary.'</p>
                        </div>
                    </div>
                </div>
            </a>
            ';
        }else{
            echo '
            <a href="'.$url.'">
                <div class="blog-post">
                    <div class="row">
                        <div class="col-12 text-center text-sm-left">
                            <p class="title">'.$title.'</p>
                            <p class="date">'.$date.'</p>
                            <p class="summary">'.$summary.'</p>
                        </div>
                    </div>
                </div>
            </a>
            ';
        }
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="col-12 text-center">
            <p class="h2">Coming Soon</p>
            <p>We currently have no blog posts to show you, but check back soon!</p>
        </div>
        ';
    }
}
?>