<?php
function graphite_portfolioPostValue($postid,$type){
    switch($type){
        case 'publishDate':
            return date($GLOBALS['GENERAL_SETTINGS']->get('general', 'dateFormat'), strtotime(graphite_database_lookupValue('portfolio', 'slug', $postid, 'publishDate')));
            break;
        case 'featuredImage':
            return graphite_getImagePath().'uploads/'.graphite_database_lookupValue('portfolio', 'slug', $postid, 'imageid').'-full.jpg';
            break;
        case 'metaImageURL':
            if(!empty(graphite_database_lookupValue('portfolio', 'slug', $postid, 'metaImageUrl'))){
                return graphite_database_lookupValue('portfolio', 'slug', $postid, 'metaImageUrl');
            }else{
                if(!empty(graphite_database_lookupValue('portfolio', 'slug', $postid, 'imageid'))){
                    return graphite_getImagePath().'uploads/'.graphite_database_lookupValue('portfolio', 'slug', $postid, 'imageid').'-seo.jpg';
                }else{
                    return '';
                }
            }
            break;
        default:
            return graphite_database_lookupValue('portfolio', 'slug', $postid, $type);
            break;
    }
}

function graphite_portfolioPosts($category){
    if(isset($category)){
        $WHERE = "WHERE status = 1 AND category = '".$category."'";
    }else{
        $WHERE = "WHERE status = 1";
    }
    
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from portfolio ".$WHERE." ORDER BY publishDate DESC";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        $title = $row[title];
        $date = date($GLOBALS['GENERAL_SETTINGS']->get('general', 'dateFormat'), strtotime($row[publishDate]));
        $url = $GLOBALS['GRAPHITE_SETTINGS']->get('portfolio', 'archiveLocation').$row[slug];
        $author = $row[author];
        $thumbnail = graphite_getImagePath().'uploads/'.$row[imageid].'-thumbnail.jpg';
        $custom1 = $row[customField1];
        $custom2 = $row[customField2];
        
        echo '
        PORTFOLIO MARKUP
        ';
    }
    mysqli_close($conn);
}
?>