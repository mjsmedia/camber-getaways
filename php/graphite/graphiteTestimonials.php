<?php
function graphite_testimonials(){
    
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from testimonials ORDER BY testimonialDate DESC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        $firstname = $row[firstname];
        $lastname = $row[lastname];
        $company = $row[company];
        $website = $row[website];
        $date = date($GLOBALS['GENERAL_SETTINGS']->get('general', 'dateFormat'), strtotime($row[testimonialDate]));
        $customfield = $row[customfield];
        $content = $row[content];
        $headline = $row[headline];
        
        switch($row[platform]){
            case 'other':
                $platformLogo = 'other.png';
                break;
            case 'tripadvisor':
                $platformLogo = 'trip-advisor.png';
                break;
            case 'airbnb':
                $platformLogo = 'airbnb.png';
                break;
            case 'homeaway':
                $platformLogo = 'home-away.png';
                break;
        }
        
        echo '
        <div class="review">
            <div class="row mb-4">
                <div class="col-6">
                    <img class="platform-logo" src="/images/review-logos/'.$platformLogo.'">
                </div>
                <div class="col-6 text-right">
                    <p class="reviewer">Reviewer:</p>
                    <p class="reviewer-name">'.$firstname.' '.$lastname.'</p>
                </div>
            </div>
            <p class="date">Submitted: <span>'.$date.'</span></p>
            <p class="headline">'.$headline.'</p>
            <p>'.$content.'</p>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <p class="lead">Nothing here yet!</p>
        <p>Please check back later.</p>
        ';
    }
}
?>