<?php

function graphite_database_lookupValue($table, $column, $row, $lookup){
    if(is_numeric($row)){
    }else{
        $row = "'".$row."'";
    }

    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $query = "SELECT * from $table WHERE $column = $row";
    $result = $conn->query($query);

    if ($result) {
        while($row = $result->fetch_object()) {
            $value = $row->$lookup;
        }
    } else {
        die("Error: " . $query . "<br>" . $conn->error);
    }

    mysqli_close($conn);

    return $value;
}

function graphite_database_resultCount($table, $column, $parameter){
    
    if(empty($column)){
        $where = "";
    }else{
        $where = " WHERE $column = '$parameter'";
    }

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from $table".$where;
    $result = $conn->query($query);
    $count = 0;

    while($row = $result->fetch_array()){
        $count ++;
    }
    mysqli_close($conn);

    return $count;
}

function graphite_getImagePath(){
    if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
        $graphiteSiteID = $GLOBALS[GENERAL_SETTINGS]->get('aws', 'graphiteSiteID');
        $requestUrl = $GLOBALS[GENERAL_SETTINGS]->get('aws', 'requestUrl');
        return $requestUrl.$graphiteSiteID.'/';
    }else{
        return $GLOBALS[rootUrl].'/images/';
    }
}

function graphite_GraphiteFooter_AddItem($name,$link){
    array_push($GLOBALS[footeritems], array($name,$link));
}

function graphite_GraphiteFooter(){
    
    if(!empty($GLOBALS[pageid])){
        array_push($GLOBALS[footeritems], array('Edit Page','pages/edit/?id='.$GLOBALS[pageid]));
    }
    
    $itemsProcessed = '';
    foreach($GLOBALS[footeritems] as $item){
        $itemsProcessed .= '<a class="GraphiteFooterLinkAction" target="_blank" href="/admin/'.$item[1].'"><div class="GraphiteFooterItem">'.$item[0].'</span></div></a>';
    }
    
    if($GLOBALS['GRAPHITE_SETTINGS']->get('graphite', 'installed') != 'true'){
        echo'
        <div class="GraphiteFooterButton">
            <a class="GraphiteFooterLink" target="_blank" href="/admin/install"><div class="GraphiteFooterItem"><i class="mjs-graphite"></i> Install</div></a>
        </div>
        ';
    }elseif(isset($_SESSION['userid']) or isset($_COOKIE['userid'])){
        echo'
        <div class="GraphiteFooterButton">
            <a class="GraphiteFooterLink" target="_blank" href="/admin/"><div class="GraphiteFooterItem"><i class="mjs-graphite"></i><span class="GraphiteFooterDashboard"> Dashboard</span></div></a>
            '.$itemsProcessed.'
            <a class="GraphiteFooterLinkAction" href="/admin/login/logout.php?return=front"><div class="GraphiteFooterItem">Log Out</span></div></a>
        </div>
        ';
    }
    
}

require_once 'graphiteBlog.php';
require_once 'graphiteVariables.php';
require_once 'graphitePages.php';
require_once 'graphiteTestimonials.php';
require_once 'graphitePortfolio.php';
require_once 'graphiteGallery.php';
require_once 'graphiteSliders.php';
?>