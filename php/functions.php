<?php

function getApiXml($path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    $retValue = curl_exec($ch);
    curl_close($ch);
    return $retValue;
}

function discountPercentage($original,$discountPercent){
    $a = $original / 100;
    $b = $a * $discountPercent;
    $newPrice = $original - $b;
    $newPriceFormat = number_format($newPrice,2);
    return $newPriceFormat;
}

function createBookNowLink($startdate,$nights,$property){
    $enddate=date_create($startdate);
    date_add($enddate,date_interval_create_from_date_string($nights." days"));

    $begin = new DateTime( $startdate );
    $end = $enddate;
    //$end = $end->modify( '+1 day' );

    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);

    $dates = array();

    foreach($daterange as $date){
        array_push($dates, $date->format("d/m/Y") . "|" . $property);
    }

    return 'https://secure.supercontrol.co.uk/availability/booking_insert.asp?ownerID=20618&siteID=42986&cottageID=cottageID_'.$property.'&portalID=&date_sb='.implode(',',$dates);
}

function getPropertyPrice($propertyCode,$startDate,$numberNights){
    $xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/filter3.asp?siteID=42986&startdate=".$startDate."&numbernights=".$numberNights."&propertycode=".$propertyCode) or die("Error: Cannot create object");

    if(empty($startDate) || empty($numberNights)){
		$minprice = $xml->property->minprice;

		//Manually removes refundable deposit
		$minprice = $minprice - 100;

        $prices['originalPrice'] = number_format($minprice,2);
        $prices['discountedPrice'] = '';
    }else{
        $price = $xml->property->prices->price->rate;
        $discount = $xml->property->prices->price->discount;
        //$extras = $xml->property->prices->price->extras;

        //$originalPrice = $price + $extras;
        //$discountedPrice = ($price + $extras) - $discount;
        $originalPrice = $price + 0;
        $discountedPrice = ($price + 0) - $discount;

        $prices['originalPrice'] = number_format($originalPrice,2);
        $prices['discountedPrice'] = number_format($discountedPrice,2);
    }
    return $prices;
}

function getProperties($params){
    $xml=simplexml_load_file("http://api.supercontrol.co.uk/xml/filter3.asp?siteID=42986&sleeps".$params) or die("Error: Cannot create object");

    foreach ($xml->property as $element) {
        $propertyName = $element->propertyname;
        $propertyClass = $element->propertygroup;
        $groupID = $element->groupID;
        $capacityNotes = $element->capacitynotes;

        switch($groupID){
            case '35130':
                $dataClass = 'gold';
                break;
            case '35127':
                $dataClass = 'platinum';
                break;
            case '35129':
                $dataClass = 'platinumplus';
                break;

        }
        echo'
        <div class="property">
            <div class="propertyClass" data-class="'.$dataClass.'">
                '.$propertyClass.'
            </div>
            <p class="title">'.$propertyName.'</p>
            <p class="description">'.$capacityNotes.'</p>
        </div>
        ';
    }
}

function getCaravanFeaturedImage($caravanid){
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $query = "SELECT * from caravans_images WHERE caravanid = '$caravanid' ORDER BY position ASC LIMIT 1";
    $result = $conn->query($query);

    if ($result) {
        while($row = $result->fetch_object()) {
            $value = $row->imageid;
        }
    } else {
        die("Error: " . $query . "<br>" . $conn->error);
    }

    mysqli_close($conn);

    return $value;
}

function getCaravanImages($caravanid){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from caravans_images WHERE caravanid = '$caravanid' ORDER BY position ASC";
    $result = $conn->query($query);

    $images = '';

    while($row = $result->fetch_array()){

        $thumbnail = graphite_getImagePath().'uploads/'.$row[imageid].'-thumbnail.jpg';
        $full = graphite_getImagePath().'uploads/'.$row[imageid].'-full.jpg';

        $images .= '
        <div class="col-6 col-md-4 col-lg-3">
            <div class="gallery-image" style="background-image:url('.$thumbnail.');" data-fancybox="gallery" data-caption="'.$row[caption].'" href="'.$full.'">
            </div>
        </div>';
    }
    mysqli_close($conn);

    return $images;
}

function getCaravans(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from caravans ORDER BY id DESC";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){

        $thumbnail = graphite_getImagePath().'uploads/'.getCaravanFeaturedImage($row[caravanid]).'-thumbnail.jpg';

        if(strlen($row[description]) > 200){
            $shortDescription = substr($row[description], 0,200).'...';
        }else{
            $shortDescription = $row[description];
        }

        echo '
        <div class="col-12 col-md-6 col-xl-4">
            <div class="property" style="background-image:url('.$thumbnail.')" data-toggle="modal" data-target="#'.$row[caravanid].'">
                <div class="details">
                    <p class="name">'.$row[name].'</p>
                    <p class="price">'.$row[price].'</p>
                    <p class="notes">'.$shortDescription.'</p>
                </div>
            </div>
        </div>

        <div class="modal fade" id="'.$row[caravanid].'" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="caravan-modal-title">'.$row[name].'</p>
                        <p class="caravan-modal-price">'.$row[price].'</p>
                        <p>'.nl2br($row[description]).'</p>
                        <div id="caravanModalImages" class="row">
                            '.getCaravanImages($row[caravanid]).'
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button button-modal-close" data-dismiss="modal">Close</button>
                        <a href="#" class="button button-modal-done">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
}

function getNextEvents(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from events WHERE enddate >= CURDATE() ORDER BY startdate ASC LIMIT 4";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;

        $startDate = date('jS M', strtotime($row[startdate]));
        $endDate = date('jS M', strtotime($row[enddate]));

        if(date('Y-m-d',strtotime($row[startdate])) <= date('Y-m-d')){
            $linkStartDate = nextArrivalDate('current');
        }else{
            $linkStartDate = nextArrivalDate($row[startdate]);
        }

        $linkEndDate = $row[enddate];

        $link = '/search/?start='.$linkStartDate.'&end='.$linkEndDate;

        if($count == 1){
            echo'
            <a href="'.$link.'">
                <li class="featured">
                    <div class="name">'.$row[name].'</div>
                    <div class="dates">'.$startDate.' - '.$endDate.'</div>
                    <div class="action">
                        <i class="fas fa-long-arrow-alt-right"></i>
                    </div>
                </li>
            </a>
            ';
        }else{
            echo'
            <a href="'.$link.'">
                <li>
                    <div class="name">'.$row[name].'</div>
                    <div class="dates">'.$startDate.' - '.$endDate.'</div>
                    <div class="action">
                        <i class="fas fa-long-arrow-alt-right"></i>
                    </div>
                </li>
            </a>
            ';
        }
    }
    mysqli_close($conn);

    if($count == 0){
        echo'
        <li class="note">
            <p>Nothing here yet, check back later.</p>
        </li>
        ';
    }
}

function getFeaturedProperty($propertyid,$tag){
    $xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/filter3.asp?siteID=42986&propertycode=".$propertyid);
    $count = 0;

    $properties = array();

    foreach ($xml->property as $property) {
        $count ++;
        $propertycode = (string)$property->propertycode;
        $propertyname = (string)$property->propertyname;
        $photocount = (string)$property->photos->photocount;
        $bedrooms = (string)$property->bedrooms_new;
        $sleeps = (string)$property->sleeps;
        $variables = array();

        foreach($property->propertyvariables->propertyvariable as $variable){
            array_push($variables,$variable);
        }
        $variableids = array();

        foreach ($variables as $variable){
            array_push($variableids, $variable[id]);
        }

        if (in_array('variableID184206', $variableids) || in_array('variableID184205', $variableids)) {
            $pets = '<li><i class="fas fa-paw"></i></li>';
        }else{
            $pets = '';
        }

        $photocountNum = (int)$photocount;

        if($photocountNum < 3){
            $photo1 = (string)$property->photos->img->main[url];
            $photo2 = 'none';
            $photo3 = 'none';
        }else{
            $photosXML = simplexml_load_file("https://api.supercontrol.co.uk/xml/property_main_gallery.asp?id=".$propertycode) or produceError('Could not connect to photo list. E:6','Could not connect to Supercontrol gallery.');
            $photocount = 1;
            foreach($photosXML->image as $image){
                switch($photocount){
                    case 1:
                        $photo1 = (string)$image->url;
                        break;
                    case 2:
                        $photo2 = (string)$image->url;
                        break;
                    case 3:
                        $photo3 = (string)$image->url;
                        break;
                }
                $photocount++;
                if($photocount > 3){break;}
            }
        }

        if(!empty($tag)){
            $tagLabel = '<div class="infobadge infobadge-featured">'.$tag.'</div>';
        }

        echo'
        <a href="/holiday-park/'.$propertycode.'">
            <div class="property" style="background-image:url('.$photo1.')">
            '.$tagLabel.'
                <div class="details">
                    <p class="name">'.$propertyname.'</p>
                    <ul class="details-icons">
                        <li><i class="fas fa-male"></i> '.$sleeps.'</li>
                        <li><i class="fas fa-bed"></i> '.$bedrooms.'</li>
                        <li><i class="fas fa-camera"></i> '.$photocount.'</li>
                        '.$pets.'
                    </ul>
                </div>
            </div>
        </a>
        ';
    }
}

function nextArrivalDate($inputDate){
    if($inputDate == 'current'){
        $inputDate = date("Y-m-d");
    }

    $day = date('D',strtotime($inputDate));

    switch($day){
        case 'Mon':
            $nextArrivalDate = date("Y-m-d",strtotime($inputDate));
            break;
        case 'Tue':
            $nextArrivalDate = date("Y-m-d",strtotime($inputDate."+3 days"));
            break;
        case 'Wed':
            $nextArrivalDate = date("Y-m-d",strtotime($inputDate."+2 days"));
            break;
        case 'Thu':
            $nextArrivalDate = date("Y-m-d",strtotime($inputDate."+1 days"));
            break;
        case 'Fri':
            $nextArrivalDate = date("Y-m-d",strtotime($inputDate));
            break;
        case 'Sat':
            $nextArrivalDate = date("Y-m-d",strtotime($inputDate."+2 days"));
            break;
        case 'Sun':
            $nextArrivalDate = date("Y-m-d",strtotime($inputDate."+1 days"));
            break;
    }

    return $nextArrivalDate;
}

function get360Player($propertyid){
    $xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/property_xml.asp?siteID=42986&id=".$propertyid);
    $customfields = $xml->property->customfields->field;
    foreach($customfields as $field){
        $fieldid = $field->title[id];
        if($fieldid == 'customfield3106'){
            return (string)$field->caption;
        }
    }
}

function getArrivalClosedDays(){

    $arrivalCloseDays = array();
    $return = '';

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from dateselector_ranges WHERE enddate >= CURDATE()";
    $result = $conn->query($query);
    while($row = $result->fetch_array()){

        $startdate = date("Y-m-d",strtotime($row[startdate]));
        $enddate = date("Y-m-d",strtotime($row[enddate]));

        $currentdate = $startdate;

        while($currentdate != $enddate){
            if($row[arrivalMonday] == 0){
                if(date("D",strtotime($currentdate)) == 'Mon'){
                    array_push($arrivalCloseDays,$currentdate);
                }
            }
            if($row[arrivalTuesday] == 0){
                if(date("D",strtotime($currentdate)) == 'Tue'){
                    array_push($arrivalCloseDays,$currentdate);
                }
            }
            if($row[arrivalWednesday] == 0){
                if(date("D",strtotime($currentdate)) == 'Wed'){
                    array_push($arrivalCloseDays,$currentdate);
                }
            }
            if($row[arrivalThursday] == 0){
                if(date("D",strtotime($currentdate)) == 'Thu'){
                    array_push($arrivalCloseDays,$currentdate);
                }
            }
            if($row[arrivalFriday] == 0){
                if(date("D",strtotime($currentdate)) == 'Fri'){
                    array_push($arrivalCloseDays,$currentdate);
                }
            }
            if($row[arrivalSaturday] == 0){
                if(date("D",strtotime($currentdate)) == 'Sat'){
                    array_push($arrivalCloseDays,$currentdate);
                }
            }
            if($row[arrivalSunday] == 0){
                if(date("D",strtotime($currentdate)) == 'Sun'){
                    array_push($arrivalCloseDays,$currentdate);
                }
            }
            $currentdate = date("Y-m-d",strtotime($currentdate."+1 days"));
        }
    }
    mysqli_close($conn);

    foreach($arrivalCloseDays as &$day){
        $return .= '"'.$day.'",';
    }

    return $return;
}

function getDepartureClosedDays(){

    $departureCloseDays = array();
    $return = '';

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from dateselector_ranges WHERE enddate >= CURDATE()";
    $result = $conn->query($query);
    while($row = $result->fetch_array()){

        $startdate = date("Y-m-d",strtotime($row[startdate]));
        $enddate = date("Y-m-d",strtotime($row[enddate]));

        $currentdate = $startdate;

        while($currentdate != $enddate){
            if($row[departureMonday] == 0){
                if(date("D",strtotime($currentdate)) == 'Mon'){
                    array_push($departureCloseDays,$currentdate);
                }
            }
            if($row[departureTuesday] == 0){
                if(date("D",strtotime($currentdate)) == 'Tue'){
                    array_push($departureCloseDays,$currentdate);
                }
            }
            if($row[departureWednesday] == 0){
                if(date("D",strtotime($currentdate)) == 'Wed'){
                    array_push($departureCloseDays,$currentdate);
                }
            }
            if($row[departureThursday] == 0){
                if(date("D",strtotime($currentdate)) == 'Thu'){
                    array_push($departureCloseDays,$currentdate);
                }
            }
            if($row[departureFriday] == 0){
                if(date("D",strtotime($currentdate)) == 'Fri'){
                    array_push($departureCloseDays,$currentdate);
                }
            }
            if($row[departureSaturday] == 0){
                if(date("D",strtotime($currentdate)) == 'Sat'){
                    array_push($departureCloseDays,$currentdate);
                }
            }
            if($row[departureSunday] == 0){
                if(date("D",strtotime($currentdate)) == 'Sun'){
                    array_push($departureCloseDays,$currentdate);
                }
            }
            $currentdate = date("Y-m-d",strtotime($currentdate."+1 days"));
        }
    }
    mysqli_close($conn);

    foreach($departureCloseDays as &$day){
        $return .= '"'.$day.'",';
    }

    return $return;
}

function checkPropertyLocal($propertycode){
    $count = graphite_database_resultCount('properties', 'propertycode', $propertycode);
    if($count > 0){
        return true;
    }else{
        return false;
    }
}

function checkLocalUpdate($propertycode,$lastupdate){
    $localLastUpdate = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'lastupdate');
    if(strtotime($lastupdate) > strtotime($localLastUpdate)){
        return false; //Local is NOT updated.
    }else{
        return true; //Local is updated.
    }
}

function getPropertyDataRemote($propertycode){
    $propertyXML=simplexml_load_file($GLOBALS["apiUrl"]."filter3.asp?siteID=42986&propertycode=".$propertycode) or produceError('Could not connect to property list. E:3','Could not connect to Supercontrol filter3.');
    foreach ($propertyXML->property as $node) {
        $typeID = (string)$node->typename[typeID];
        $propertyname = (string)$node->propertyname;
        $sleeps = (string)$node->sleeps;
        $photocount = (string)$node->photos->photocount;
        $minprice = (string)$node->minprice;
        $bedrooms = (string)$node->bedrooms_new;
        $variables = array();

        foreach($node->propertyvariables->propertyvariable as $variable){
            array_push($variables,$variable);
        }
        $variableids = array();

        foreach ($variables as $variable){
            array_push($variableids, $variable[id]);
        }

        if (in_array('variableID184206', $variableids) || in_array('variableID184205', $variableids)) {
            $pets = 1;
        }else{
            $pets = 0;
        }

        $photocountNum = (int)$photocount;

        if($photocountNum < 1){
            $photo1 = 'none';
            $photo2 = 'none';
            $photo3 = 'none';
        }elseif($photocountNum < 3){
            $photo1 = (string)$node->photos->img->main[url];
            $photo2 = 'none';
            $photo3 = 'none';
        }else{
            $photosXML = simplexml_load_file($GLOBALS["apiUrl"]."property_main_gallery.asp?id=".$propertycode) or produceError('Could not connect to photo list. E:6','Could not connect to Supercontrol gallery.');
            $count = 1;
            foreach($photosXML->image as $image){
                switch($count){
                    case 1:
                        $photo1 = (string)$image->url;
                        break;
                    case 2:
                        $photo2 = (string)$image->url;
                        break;
                    case 3:
                        $photo3 = (string)$image->url;
                        break;
                }
                $count++;
                if($count > 3){break;}
            }
        }

        $propertyNodes['typeID'] = $typeID;
        $propertyNodes['propertyname'] = $propertyname;
        $propertyNodes['sleeps'] = $sleeps;
        $propertyNodes['photocount'] = $photocount;
        $propertyNodes['minprice'] = $minprice;
        $propertyNodes['bedrooms'] = $bedrooms;
        $propertyNodes['pets'] = $pets;
        $propertyNodes['photo1'] = $photo1;
        $propertyNodes['photo2'] = $photo2;
        $propertyNodes['photo3'] = $photo3;

    }
    return $propertyNodes;
}

function getHomepageBanners(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from banners WHERE type = 0 AND (startdate <= CURDATE() AND enddate >= CURDATE()) ORDER BY startdate DESC LIMIT 4";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;

        switch($row['style']){
            default:
                $bannerStyle = '';
                $bannerIcon = '';
                break;
            case 1:
                $bannerStyle = ' banner--info';
                $bannerIcon = '<i class="fas fa-info-circle"></i> ';
                break;
            case 2:
                $bannerStyle = ' banner--warning';
                $bannerIcon = '<i class="fas fa-exclamation-triangle"></i> ';
                break;
        }
        if(!empty($row['buttonLabel'])){
            $button = '<a href="'.$row['buttonUrl'].'" class="banner__button">'.$row['buttonLabel'].' <i class="fas fa-long-arrow-alt-right"></i></a>';
        }else{
            $button = '';
        }
        switch($row['textAlign']){
            default:
                $textAlign = 'text-left';
                break;
            case 1:
                $textAlign = 'text-center';
                break;
            case 2:
                $textAlign = 'text-right';
                break;
        }
        echo'
        <div class="banner'.$bannerStyle.' '.$textAlign.'">
            <p class="banner__title">'.$bannerIcon.$row['title'].'</p>
            <p class="banner__content">'.$row['content'].'</p>
            '.$button.'
        </div>
        ';
    }
    mysqli_close($conn);
}

function getSearchBanners($searchStartdate, $searchEnddate){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from banners WHERE type = 1 AND (startdate <= '$searchEnddate' AND enddate >= '$searchStartdate') ORDER BY startdate DESC LIMIT 4";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;

        switch($row['style']){
            default:
                $bannerStyle = '';
                $bannerIcon = '';
                break;
            case 1:
                $bannerStyle = ' banner--info';
                $bannerIcon = '<i class="fas fa-info-circle"></i> ';
                break;
            case 2:
                $bannerStyle = ' banner--warning';
                $bannerIcon = '<i class="fas fa-exclamation-triangle"></i> ';
                break;
        }
        echo'
        <div class="banner'.$bannerStyle.'">
            <p class="banner__title">'.$bannerIcon.$row['title'].'</p>
            <p class="banner__content">'.$row['content'].'</p>
        </div>
        ';
    }
    mysqli_close($conn);
}
?>
