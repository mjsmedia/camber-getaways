<?php
require_once 'Config/Lite.php';
$GENERAL_SETTINGS = new Config_Lite($ROOTLOCATION.'config/general.ini');
$GRAPHITE_SETTINGS = new Config_Lite($ROOTLOCATION.'config/graphite.ini');


if($_SERVER['HTTP_HOST'] == 'camber:8888' || $_SERVER['HTTP_HOST'] == '192.168.1.58:8888'){
    $sqlHOST = $GENERAL_SETTINGS->get('databaselocal', 'host');
    $sqlUSER = $GENERAL_SETTINGS->get('databaselocal', 'user');
    $sqlPASS = $GENERAL_SETTINGS->get('databaselocal', 'pass');
    $sqlDATA = $GENERAL_SETTINGS->get('databaselocal', 'data');
}else{
    $sqlHOST = $GENERAL_SETTINGS->get('database', 'host');
    $sqlUSER = $GENERAL_SETTINGS->get('database', 'user');
    $sqlPASS = $GENERAL_SETTINGS->get('database', 'pass');
    $sqlDATA = $GENERAL_SETTINGS->get('database', 'data');
}

$link = mysqli_connect($sqlHOST, $sqlUSER, $sqlPASS, $sqlDATA);

if (!$link) {
    if($_GET['debug'] == 1){
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
    }else{
        include($ROOTLOCATION.'500.shtml');
        exit;
    }
}
mysqli_close($link);

if($GENERAL_SETTINGS->get('general', 'maintenance') == 'true'){
    
    
    if(isset($_SESSION['userid']) or isset($_COOKIE['userid'])){
        
    }else{
        include($ROOTLOCATION.'maintenance.shtml');
        exit;
    }
}

$enableHTTPS = $GENERAL_SETTINGS->get('general', 'enableHTTPS');
$siteTitle = $GENERAL_SETTINGS->get('general', 'siteTitle');

//--HTTPS Settings
if($enableHTTPS == 'true'){
    if($_SERVER["HTTPS"] != "on")
    {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
    }
}
$currentUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$rootUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

$GLOBALS['footeritems'] = array();
require_once 'graphite/graphiteMain.php';
session_start();

$apiUrl = 'http://api.supercontrol.co.uk/xml/';

$arrivalDays = array();
if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalSunday') != 'true'){
    array_push($arrivalDays,0);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalMonday') != 'true'){
    array_push($arrivalDays,1);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalTuesday') != 'true'){
    array_push($arrivalDays,2);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalWednesday') != 'true'){
    array_push($arrivalDays,3);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalThursday') != 'true'){
    array_push($arrivalDays,4);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalFriday') != 'true'){
    array_push($arrivalDays,5);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalSaturday') != 'true'){
    array_push($arrivalDays,6);
}

$departureDays = array();
if($GRAPHITE_SETTINGS->get('dateselection', 'departureSunday') != 'true'){
    array_push($departureDays,0);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'departureMonday') != 'true'){
    array_push($departureDays,1);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'departureTuesday') != 'true'){
    array_push($departureDays,2);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'departureWednesday') != 'true'){
    array_push($departureDays,3);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'departureThursday') != 'true'){
    array_push($departureDays,4);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'departureFriday') != 'true'){
    array_push($departureDays,5);
}
if($GRAPHITE_SETTINGS->get('dateselection', 'departureSaturday') != 'true'){
    array_push($departureDays,6);
}

require_once 'functions.php';
?>