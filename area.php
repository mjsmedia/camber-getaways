<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Surrounding Area';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'What can you expect to find while exploring the surrounding area during your stay?';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-75" style="background: linear-gradient(rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0.15),rgb(255, 255, 255)),url(/images/camber-map.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center">
                        <h1>Surrounding Area</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section pt-0 background-default">
            <div class="container section-pullup">
                <div class="row">

                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <a href="/camber-sands">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/camber-sands-2.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Camber Sands</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>With 7 miles of sand and sea, Camber Beach is the perfect day out for all the family. Whether you enjoy a game of hide and seek in the rolling dunes, a swim in the cool sea or just to kick back and relax, Camber is perfect.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6">
                        <a data-toggle="modal" data-target="#ryeModal">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/rye.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Rye</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>One of the best preserved medieval towns in England, Rye is home to the enchanting cobbled Mermaid Street, the impressive Norman church of St Mary’s, a rich selection of  specialist shops and a thriving fishing fleet.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!--
                    <div class="col-12 col-md-6">
                        <a data-toggle="modal" data-target="#dungenessModal">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dungeness0.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Dungeness</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>One of the best preserved medieval towns in England, Rye is home to the enchanting cobbled Mermaid Street, the impressive Norman church of St Mary’s, a rich selection of  specialist shops and a thriving fishing fleet.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    -->
                    <div class="col-12 col-md-6">
                        <a data-toggle="modal" data-target="#hastingsModal">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/hastings.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Hastings</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Hastings offers a lovely mix of things to do, certain to please everyone. The Seafront offers a choice of Adventure Golf courses, fun packed amusement arcades, newly refurbished Pier, a small funfair and much more.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="ryeModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="caravan-modal-title mb-4">Rye</p>
                        <p class="lead">The ancient and historical town of Rye, located 3 miles from Camber, is home to enchanting cobbled streets, ancient buildings and secret passageways. Rye was once the haunt of notorious smugglers and highwaymen and often attracts film crews.</p>
                        <p>This medieval market town is the perfect setting for a day out exploring thebeautiful cobbled Mermaid Street, historical Gun Gardens or impressive church of St Mary’s.</p>
                        <p>Rye has a Jempsons supermarket and an array of shops down Rye High Street.</p>
                        <p>There is a big Market on Thursday’s right next to the train and bus stations.</p>
                        <p>There is a bus to Rye every half an hour or a Cycle path which is about a half hour ride with scenic views of the lakes and sheep fields that surround Camber - Have you thought about renting one of our <a href="/e-bikes">E-Bikes</a>?</p>
                        <div class="row">
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/rye.jpg);" data-fancybox="ryeGallery" data-caption="" href="/images/rye.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/rye1.jpg);" data-fancybox="ryeGallery" data-caption="© Fred in't Hout" href="/images/rye1.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/rye2.jpg);" data-fancybox="ryeGallery" data-caption="© Hans Littel" href="/images/rye2.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/rye3.jpg);" data-fancybox="ryeGallery" data-caption="© Fred in't Hout" href="/images/rye3.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button button-modal-close" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="dungenessModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="caravan-modal-title mb-4">Dungeness</p>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <div class="row">
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/dungeness0.jpg);" data-fancybox="dungenessGallery" data-caption="© Ashley Gorringe" href="/images/dungeness0.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/dungeness1.jpg);" data-fancybox="dungenessGallery" data-caption="© Ashley Gorringe" href="/images/dungeness1.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/dungeness2.jpg);" data-fancybox="dungenessGallery" data-caption="© Andrew Marks" href="/images/dungeness2.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button button-modal-close" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="hastingsModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="caravan-modal-title mb-4">Hastings</p>
                        <p class="lead">Hastings offers a lovely mix of things to do, certain to please everyone.</p>
                        <p>The Seafront offers a choice of Adventure Golf courses, fun packed amusement arcades, newly refurbished Pier, a small funfair and much more. The Old Town is bustling with quirky shops, restaurants, bars and hidden little gems. Other places to visit include Blue Reef Aquarium, Smugglers Caves, Clambers and Priory Meadow Shopping Centre.</p>
                        <div class="row">
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/hastings.jpg);" data-fancybox="hastingsGallery" data-caption="" href="/images/hastings.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/hastings1.jpg);" data-fancybox="hastingsGallery" data-caption="" href="/images/hastings1.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/hastings2.jpg);" data-fancybox="hastingsGallery" data-caption="" href="/images/hastings2.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/hastings3.jpg);" data-fancybox="hastingsGallery" data-caption="" href="/images/hastings3.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/hastings4.jpg);" data-fancybox="hastingsGallery" data-caption="" href="/images/hastings4.jpg">
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="gallery-image" style="background-image:url(/images/hastings5.jpg);" data-fancybox="hastingsGallery" data-caption="" href="/images/hastings5.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button button-modal-close" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <script src="/js/simpleSearchForm.js"></script>
    </body>
</html>