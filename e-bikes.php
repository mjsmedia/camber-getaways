<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'eBikes';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = $rootUrl.'/images/seo/eBike.jpg';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Our E-bikes can be booked for a half day or a full day and prices start from £25.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(/images/bikes0.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>eBikes</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-yellow background-stitch-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('ETGNTARW0PG');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <h2 class="text-blue mb-4">Availability and Pricing</h2>
                        <p class="lead">Our E-bikes can be booked for a half day or a full day and prices start from £25.</p>
                        <p>Please get in touch to book your E-bike! Call 01797 227431 or email info@cambergetaways.co.uk. Please be aware you need to be over the age of 14 to ride the E-Bikes</p>
                        <h5>£25 for a half day (9:30-13:00 or 13:30-17:00)</h5>
                        <h5>£40 full day</h5>
                        <p>Discounts for multiple days on request</p>
                        <p><small>We will need ID and will pre-authorise a credit or debit card for a £200 deposit at the time of hire which is then released as soon as the bike is returned. No actual money is taken.</small></p>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
    </body>
</html>