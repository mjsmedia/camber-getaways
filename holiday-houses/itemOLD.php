<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$date = $_GET['d'];
$day = date("D", strtotime($date));


if($day == 'Mon'){
    if(empty($_GET['n'])){
        $nights = 4;
        $nightslabel = 'Any';
    }elseif($_GET['n'] == '3-4'){
        $nights = 4;
        $nightslabel = '4 Nights';
    }else{
        $nights = $_GET['n'];
        $nightslabel = $nights.' Nights';
    }
}elseif($day == 'Fri'){
    if(empty($_GET['n'])){
        $nights = 3;
        $nightslabel = 'Any';
    }elseif($_GET['n'] == '3-4'){
        $nights = 3;
        $nightslabel = '3 Nights';
    }else{
        $nights = $_GET['n'];
        $nightslabel = $nights.' Nights';
    }
}

if(empty($_GET['d']) || empty($_GET['n'])){
    $bookNowLink = 'https://secure.supercontrol.co.uk/availability/availability_weekly.asp?ownerID=20618&siteID=42986&cottageID=cottageID_'.$_GET['id'].'&dDate='.$_GET['d'].'&TID=';
}else{
    $bookNowLink = createBookNowLink($date,$nights,$_GET['id']);
}

$xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/property_xml.asp?siteID=42986&id=".$_GET['id']."&startdate=".$date."&numbernights=".$nights) or die("Error: Cannot create object");

if(empty($xml->property->propertycode)){
    include $ROOTLOCATION.'404.shtml';
    exit or die("Error: 404 Not Found");
}

$pageTitle = $xml->property->propertyname.' - Holiday Houses';            //Page Title
$pageSlug = 'holiday-homes';             //Page Slug
$menuTransparent = false;
$classyMode = 'large';

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = 'https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$xml->property->photos->img->photoid.'.jpg';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = $xml->property->shortdescription;   //Meta Description

if(empty($_GET['d'])){
    $minprice = (string)$xml->property->prices->minprice;
}else{
    $minprice = (string)$xml->property->prices->pricestay[week];
}

if($xml->property->latediscount != 0){
    $originalPrice = $xml->property->prices->pricestay[week];
    $discount = $xml->property->latediscount;
    $discountPrice = $originalPrice - $discount;
    $discountPrice = number_format($discountPrice,2);
    $minprice = $discountPrice;
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <link rel="stylesheet" href="/css/classy.min.css">
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-75" style="background: linear-gradient(rgba(0, 0, 0, 0.0),rgba(0, 0, 0, 0.0)),url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/<?php echo $xml->property->photos->img->photoid;?>.jpg); background-position:center!important;">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-12 align-self-center text-center text-md-right d-none d-md-block">
                        <div class="propertyTitleBlock">
                            <h1><?php echo $xml->property->propertyname;?></h1>
                            <p><?php echo $xml->property->propertyaddress;?></p>
                        </div>
                    </div>
                    <div class="col-12 align-self-end text-center text-md-right d-md-none mb-4">
                        <div class="propertyTitleBlock">
                            <h1><?php echo $xml->property->propertyname;?></h1>
                            <p><?php echo $xml->property->propertyaddress;?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid background-yellow background-stitch-white mobileBookingBox d-block d-lg-none">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 mb-4 mb-md-4">
                        <a class="button m-0 w-100 text-center" href="<?php echo $bookNowLink;?>"><i class="fas fa-shopping-cart"></i> Book Now <strong>From £<?php echo $minprice;?></strong></a>
                    </div>
                    <div class="col-12 col-md-6 mb-4 mb-md-4">
                        <a class="button m-0 w-100 text-center" href="https://secure.supercontrol.co.uk/availability/availability_weekly.asp?ownerID=20618&siteID=42986&cottageID=cottageID_<?php echo $_GET['id'];?>&dDate=<?php echo $_GET['d'];?>&TID="><i class="far fa-calendar-check"></i> Check availablity</a>
                    </div>
                    <div class="col-12 col-md-6 mb-4 mb-md-0">
                        <a class="button m-0 w-100 text-center" href="/contact/<?php echo $_GET['id'];?>"><i class="fas fa-question-circle"></i> Ask a question</a>
                    </div>
                    <div class="col-12 col-md-6">
                        <a class="button m-0 w-100 text-center" href="/contact/<?php echo $_GET['id'];?>"><i class="fas fa-envelope"></i> Contact us</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid background-default background-stitch-blue">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8 page-section">
                        <p class="lead mb-4 mt-4"><?php echo $xml->property->shortdescription;?></p>
                    </div>
                    <div class="col-12 col-md-4 d-none d-lg-block">
                        <div class="bookingBox">
                            <?php if(!empty($_GET['d'])){echo'
                            <p class="title mb-2">Your Booking...</p>
                            <p>Date: <strong>'.date("D d M Y", strtotime($date)).'</strong></p>
                            <p class="mb-4">Duration: <strong>'.$nightslabel.'</strong></p>
                            ';}?>
                            <ul>
                                <a href="<?php echo $bookNowLink;?>"><li><i class="fas fa-shopping-cart"></i> Book Now <strong>From £<?php echo $minprice;?></strong></li></a>
                                <a href="https://secure.supercontrol.co.uk/availability/availability_weekly.asp?ownerID=20618&siteID=42986&cottageID=cottageID_<?php echo $_GET['id'];?>&dDate=<?php echo $_GET['d'];?>&TID="><li><i class="far fa-calendar-check"></i> Check availablity</li></a>
                                <a href="/contact/<?php echo $_GET['id'];?>"><li><i class="fas fa-question-circle"></i> Ask a question</li></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid background-secondary page-section">
            <div class="container">
                <div class="row">
                    <?php foreach ($xml->property->variables->varcat as $element) {
    switch($element->varcatname){
        case 'Internal Features':
            $categoryIcon = 'fas fa-tv';
            break;
        case 'External Features':
            $categoryIcon = 'fas fa-tree';
            break;
        case 'Accommodation':
            $categoryIcon = 'fas fa-bed';
            break;
        case 'Park Location':
            $categoryIcon = 'fas fa-map-marker-alt';
            break;
    }
    echo'
                        <div class="col-12 col-md-6 col-lg-3 mb-4 text-center text-md-left">
                        <i class="'.$categoryIcon.' fa-2x mb-2 d-block d-md-none"></i>
                        <h6 class="mb-3"><i class="'.$categoryIcon.' d-none d-md-inline-block"></i> '.$element->varcatname.'</h6>
                        <ul class="propertyFeatures">';
    foreach ($element->varcatitems->varcatitem as $varcatitem){
        echo'<li><span>'.$varcatitem->variable.'</span></li>';
    }
    echo'
                        </ul>
                        </div>
                        ';
}?>
                </div>
            </div>
        </div>
        <?php
        $count = 0;
        $imageIDs = array();
        foreach ($xml->property->photos->img as $element) {
            $count ++;
            array_push($imageIDs,$element->photoid);
        }
        if($count >= 4){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[0].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[1].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[2].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[3].'.jpg)">
                        </div>
                    </div>
                </div>
                <div class="galleryLinkOverlay">
                    <div class="content text-center">
                        <h2 class="mb-2">Photo Gallery</h2>
                        <button class="button m-0" onclick="openGallery()">View '.$count.' Photos</button>
                    </div>
                </div>
            </div>
            ';
        }elseif($count == 3){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-4">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[0].'.jpg)">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[1].'.jpg)">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[2].'.jpg)">
                        </div>
                    </div>
                </div>
                <div class="galleryLinkOverlay">
                    <div class="content text-center">
                        <h2 class="mb-2">Photo Gallery</h2>
                        <button class="button m-0" onclick="openGallery()">View '.$count.' Photos</button>
                    </div>
                </div>
            </div>
            ';
        }elseif($count == 2){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-6">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[0].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[1].'.jpg)">
                        </div>
                    </div>
                </div>
                <div class="galleryLinkOverlay">
                    <div class="content text-center">
                        <h2 class="mb-2">Photo Gallery</h2>
                        <button class="button m-0" onclick="openGallery()">View '.$count.' Photos</button>
                    </div>
                </div>
            </div>
            ';
        }elseif($count == 1){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-12">
                        <img src="https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/'.$imageIDs[0].'.jpg" style="width:100%;">
                    </div>
                </div>
            </div>
            ';
        }
        
        ?>
        <div class="container-fluid background-secondary page-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 webdescription">
                        <?php echo $xml->property->webdescription;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid background-default page-section d-none d-md-block">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="text-blue mb-2">When do you want to stay?</h2>
                        <p class="lead mb-4">Check availability and plan your break</p>
                        <div class="sc-availabilty-box">
                        <script type="text/javascript" src="//secure.supercontrol.co.uk/avail_ajax/js/load.js?ownerID=20618&siteID=42986&cottageID=<?php echo $_GET['id'];?>"></script><div id="supercontrol-availability"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid background-default map-section">
            <div id="map"></div>
            <script>
                var map;
                function initMap() {
                    var location = {lat: <?php echo $xml->property->latitude;?>, lng: <?php echo $xml->property->longitude;?>};
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: location,
                        zoom: 17,
                        styles: [
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f7f1df"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#d0e3b4"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    
    {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dfd2ae"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#93817c"
                                    }
                                ]
                            },
                            {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#bde6ab"
            }
        ]
    },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#447530"
                                    }
                                ]
                            },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffe15f"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#efd151"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "black"
            }
        ]
    },
    {
        "featureType": "transit.station.airport",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#cfb2db"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#a2daf2"
            }
        ]
    },
                            {
                                "featureType": "water",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            }
]
                    });
                    var marker = new google.maps.Marker({
                        position: location,
                        map: map
                    });
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABc5r_DEMHXtnMJPHsABqg1pHwaSlaf_k&callback=initMap"
                    async defer></script>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <style>
            #hideFeaturesButton{
                display: none;
            }
        </style>
        <script>
            function openGallery(){
                $.fancybox.open([
                    <?php
                    foreach ($xml->property->photos->img as $element) {
                        echo"
                        {
                            src  : 'https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/".$element->photoid.".jpg',
                            opts : {
                                thumb   : 'https://c621446.ssl.cf3.rackcdn.com/images/cottages/".$element->photoid.".jpg'
                            }
                        },
                        ";
                    }
                    ?>
                ], {
                    loop : true,
                    thumbs : {
                        autoStart : false
                    }
                });
            }
        </script>
    </body>
</html>