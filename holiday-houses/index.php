<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Holiday Houses';            //Page Title
$pageSlug = 'holiday-homes';             //Page Slug
$menuTransparent = true;
$classyMode = 'small';

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = $rootUrl.'/images/seo/holiday-homes.jpg';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'We have a selection of holiday homes throughout Camber and the beautiful nearby town of Rye. All the homes are privately owned and include some lovely unique features.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)),url(https://cdn.mjsmedia.co.uk/ross-francis-interiors/uploads/RPVQJZCZEMP-full.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Holiday Homes</h1>
                        <p class="mb-0">Your home from home</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-blue">
            <div class="container mb-4">
                <div class="row justify-content-center mb-4">
                    <div class="col-12 col-md-10 text-center">
                        <h2 class="text-yellow">What are you looking for?</h2>
                    </div>
                </div>
                <form id="searchForm" action="/search/holiday-houses" method="get">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                            <div class="form-group">
                                <label>How many people?</label>
                                <select name="people" id="people" class="form-control custom-form-select">
                                    <option <?php if($_GET['people'] == ''){echo'selected';}?> value="">Any</option>
                                    <option <?php if($_GET['people'] == '1'){echo'selected';}?> value="1">1</option>
                                    <option <?php if($_GET['people'] == '2'){echo'selected';}?> value="2">2</option>
                                    <option <?php if($_GET['people'] == '3'){echo'selected';}?> value="3">3</option>
                                    <option <?php if($_GET['people'] == '4'){echo'selected';}?> value="4">4</option>
                                    <option <?php if($_GET['people'] == '5'){echo'selected';}?> value="5">5</option>
                                    <option <?php if($_GET['people'] == '6'){echo'selected';}?> value="6">6</option>
                                    <option <?php if($_GET['people'] == '7'){echo'selected';}?> value="7">7</option>
                                    <option <?php if($_GET['people'] == '8'){echo'selected';}?> value="8">8</option>
                                    <option <?php if($_GET['people'] == '9'){echo'selected';}?> value="9">9</option>
                                    <option <?php if($_GET['people'] == '10'){echo'selected';}?> value="10">10</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Arrival Date?</label>
                                <input name="start" id="startDate" class="form-control custom-form-control datePickerStart" placeholder="Any" onchange="unlockDeparture()">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Departure Date?</label>
                                <input name="end" id="endDate" class="form-control custom-form-control datePickerEnd custom-disabled" placeholder="Select Arrival" disabled onchange="validateDeparture()">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                            <div class="form-group">
                                <label>Features</label>
                                <button type="button" id="moreOptionsButton" class="form-control custom-form-control hover-point" data-toggle="modal" data-target="#moreOptions">None Selected</button>
                            </div>
                        </div>
                        <div class="col-12 col-xl-2 text-center text-xl-right">
                            <button type="button" id="submitbutton" class="button button-search" onclick="basicSubmitForm()">Search <i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="modal" id="moreOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button class="custom-modal-close" data-dismiss="modal" onclick="countSelectedOptions()"><i class="fas fa-times"></i></button>
                                    <div class="row">
                                        <div class="col-12 col-md-4 mb-4">
                                            <h6 class="mb-4">External Features</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Decking
                                                    <input id="options-decking" class="optionsCheckbox" type="checkbox" name="decking" value="1" <?php if($_GET['decking'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Double Glazing
                                                    <input id="options-double-glazing" class="optionsCheckbox" type="checkbox" name="double-glazing" value="1" <?php if($_GET['double-glazing'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Private Hot Tub
                                                    <input id="options-hot-tub" class="optionsCheckbox" type="checkbox" name="hot-tub" value="1" <?php if($_GET['hot-tub'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Private Parking
                                                    <input id="options-private-parking" class="optionsCheckbox" type="checkbox" name="private-parking" value="1" <?php if($_GET['private-parking'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Patio
                                                    <input id="options-patio" class="optionsCheckbox" type="checkbox" name="patio" value="1" <?php if($_GET['patio'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Garden
                                                    <input id="garden" class="optionsCheckbox" type="checkbox" name="garden" value="1" <?php if($_GET['garden'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4 mb-4">
                                            <h6 class="mb-4">Internal Features</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Bath
                                                    <input id="options-bath" class="optionsCheckbox" type="checkbox" name="bath" value="1" <?php if($_GET['bath'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Bedroom TV
                                                    <input id="options-bedroom-tv" class="optionsCheckbox" type="checkbox" name="bedroom-tv" value="1" <?php if($_GET['bedroom-tv'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Central Heating
                                                    <input id="options-central-heating" class="optionsCheckbox" type="checkbox" name="central-heating" value="1" <?php if($_GET['central-heating'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Dishwasher
                                                    <input id="options-dishwasher" class="optionsCheckbox" type="checkbox" name="dishwasher" value="1" <?php if($_GET['dishwasher'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">En Suite
                                                    <input id="options-en-suite" class="optionsCheckbox" type="checkbox" name="en-suite" value="1" <?php if($_GET['en-suite'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Second WC
                                                    <input id="options-second-wc" class="optionsCheckbox" type="checkbox" name="second-wc" value="1" <?php if($_GET['second-wc'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Sky TV
                                                    <input id="options-sky-tv" class="optionsCheckbox" type="checkbox" name="sky-tv" value="1" <?php if($_GET['sky-tv'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Tumble Dryer
                                                    <input id="options-tumble-dryer" class="optionsCheckbox" type="checkbox" name="tumble-dryer" value="1" <?php if($_GET['tumble-dryer'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Washing Machine
                                                    <input id="options-washing-machine" class="optionsCheckbox" type="checkbox" name="washing-machine" value="1" <?php if($_GET['washing-machine'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">1 Small Pet Friendly
                                                    <input id="options-small-pets" class="optionsCheckbox" type="checkbox" name="small-pets" value="1" <?php if($_GET['small-pets'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">1 or 2 Pets (Any Size) Friendly
                                                    <input id="options-large-pets" class="optionsCheckbox" type="checkbox" name="large-pets" value="1" <?php if($_GET['large-pets'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            
                                        </div>
                                        <div class="col-12 col-md-4 mb-4">
                                            <h6 class="mb-4">Location</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Close to Beach
                                                    <input id="options-close-to-beach" class="optionsCheckbox" type="checkbox" name="close-to-beach" value="1" <?php if($_GET['close-to-beach'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Views
                                                    <input id="options-views" class="optionsCheckbox" type="checkbox" name="views" value="1" <?php if($_GET['views'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="button button-modal-done" data-dismiss="modal" onclick="countSelectedOptions(); basicSubmitForm()">Search <i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container-fluid page-section background-default">
            <div class="container mb-4">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 mb-4 text-center">
                        <?php graphite_pageSection('TJ974VLIDY9');?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <p><strong>As standard we make sure all holiday homes include the following:</strong></p>
                        <ul class="featureList">
                            <li>Cleaned and Dressed ready for your arrival</li>
                            <li>Linens & Towels</li>
                            <li>Tea, Coffee, Sugar</li>
                            <li>Toilet Rolls and Washing up Liquid</li>
                            <li>Television with DVD & Freeview, Fully Equipped Kitchen</li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-secondary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <?php graphite_pageSection('GZV0IUH7KCI');?>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <script>
            var MINNIGHTS = 2;
        </script>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        
        <script src="/js/searchForm.js?v3"></script>
    </body>
</html>