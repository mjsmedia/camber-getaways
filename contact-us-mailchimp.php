<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Contact us';            //Page Title
$pageSlug = 'contact';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Got a question or concern? We would love to hear from you!';   //Meta Description

if(isset($_GET['id'])){
    $xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/property_xml.asp?siteID=42986&id=".$_GET['id']) or die("Error: Cannot create object");

    $propertyName = $xml->property->propertyname;
}else{
    $propertyName = '';
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 50%,rgb(255, 255, 255) 100%),url(/images/beach0.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center">
                        <h1>Contact us</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section pt-0 background-default">
            <div class="container section-pullup">
                <form id="contactFormMailchimp" class="contact-form" style="background-image: linear-gradient(to top, #48c6ef 0%, #6f86d6 100%); color:white; display: block;z-index: 2;position: relative;">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label>Your Name</label>
                                <input id="name" name="name" type="text" class="form-control custom-form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label>Your Email Address</label>
                                <input id="email" name="email" type="email" class="form-control custom-form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label>Your Phone Number</label>
                                <input id="phone" name="phone" type="phone" class="form-control custom-form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <label>Your Message</label>
                                <textarea id="message" name="message" class="form-control custom-form-control" rows="5" style="height:auto!important;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <button id="submit" type="button" class="button button-search m-0 mt-2" onclick="submitForm()">Send <i class="fas fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center text-md-left">
                        <h2>How to find us:</h2>
                        <?php graphite_pageSection('5N5DP36KR2P');?>
                        <style>
                            #map{
                                width: 100%;
                                height: 400px;
                                border-radius: 10px;
                            }
                        </style>
                        <div id="map"></div>
                        <a target="_blank" href="https://goo.gl/maps/YpLCw8gaqAU2" class="button m-0 mt-4">Get Directions</a>
                        <script>
                            var map;
                            function initMap() {
                                var location = {lat: 50.934972, lng: 0.798251};
                                map = new google.maps.Map(document.getElementById('map'), {
                                    center: location,
                                    zoom: 17
                                });
                                var marker = new google.maps.Marker({
                                    position: location,
                                    map: map
                                });
                            }
                        </script>
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABc5r_DEMHXtnMJPHsABqg1pHwaSlaf_k&callback=initMap"
                                async defer></script>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="/js/contactFormMailchimp.js"></script>
    </body>
</html>