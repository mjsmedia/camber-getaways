<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';


function produceError($response,$debug){
    $JSONResponse->results = '';
    $JSONResponse->status = 'error';
    if($_GET['debug'] == 'true'){
        $JSONResponse->errorResponse = $response.' ['.$debug.']';
    }else{
        $JSONResponse->errorResponse = $response;
    }
    echo json_encode($JSONResponse);
    exit;
}

if(empty($_POST['query'])){
    produceError('Your Property Code was missing.','');
}

$xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/filter3.asp?siteID=42986&propertyname=".$_POST['query']) or produceError('Could not connect to property list. E:1','Could not connect to Supercontrol filter3.');
$count = 0;

$properties = array();

foreach ($xml->property as $property) {
    $count ++;
    $propertycode = (string)$property->propertycode;
    $propertyname = (string)$property->propertyname;
    $photocount = (string)$property->photos->photocount;
    $bedrooms = (string)$property->bedrooms_new;
    $sleeps = (string)$property->sleeps;
    $variables = array();

    foreach($property->propertyvariables->propertyvariable as $variable){
        array_push($variables,$variable);
    }
    $variableids = array();

    foreach ($variables as $variable){
        array_push($variableids, $variable[id]);
    }

    if (in_array('variableID184206', $variableids) || in_array('variableID184205', $variableids)) {
        $pets = 1;
    }else{
        $pets = 0;
    }

    $photocountNum = (int)$photocount;

    if($photocountNum < 3){
        $photo1 = (string)$property->photos->img->main[url];
        $photo2 = 'none';
        $photo3 = 'none';
    }else{
        $photosXML = simplexml_load_file("https://api.supercontrol.co.uk/xml/property_main_gallery.asp?id=".$propertycode) or produceError('Could not connect to photo list. E:6','Could not connect to Supercontrol gallery.');
        $photocount = 1;
        foreach($photosXML->image as $image){
            switch($photocount){
                case 1:
                    $photo1 = (string)$image->url;
                    break;
                case 2:
                    $photo2 = (string)$image->url;
                    break;
                case 3:
                    $photo3 = (string)$image->url;
                    break;
            }
            $photocount++;
            if($photocount > 3){break;}
        }
    }

    array_push($properties,array($propertycode,$propertyname,$photocount,$bedrooms,$sleeps,$pets,$photo1,$photo2,$photo3));
}

if($count == 1){
    $JSONResponse->singleResultCode = $propertycode;
}else{
    $JSONResponse->results = $properties;
}

$JSONResponse->status = 'success';
$JSONResponse->count = $count;

echo json_encode($JSONResponse);
exit;

?>