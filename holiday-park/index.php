<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Camber Holiday Park';            //Page Title
$pageSlug = 'parkdean';             //Page Slug
$menuTransparent = true;
$classyMode = false;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = $rootUrl.'/images/seo/holiday-park.jpg';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Our Caravans, Lodges and Chalets are all based within the Parkdean Resorts site in Camber Sands. It is situated a couple of minutes walk from the beach and houses plenty of fun activities for the whole family.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)),url(/images/park-dean.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Caravans, Lodges and Chalets</h1>
                        <p class="mb-0">All fully equipped and beautifully presented</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-blue background-stitch-white">
            <div class="container mb-4">
                <div class="row justify-content-center mb-4">
                    <div class="col-12 col-md-10 text-center">
                        <h2 class="text-yellow">What are you looking for?</h2>
                    </div>
                </div>
                <form id="searchForm" action="/search/holiday-park" method="get">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                            <div class="form-group">
                                <label>How many people?</label>
                                <select id="people" name="people" class="form-control custom-form-select">
                                    <option <?php if($_GET['people'] == ''){echo'selected';}?> value="">Any</option>
                                    <option <?php if($_GET['people'] == '1'){echo'selected';}?> value="1">1</option>
                                    <option <?php if($_GET['people'] == '2'){echo'selected';}?> value="2">2</option>
                                    <option <?php if($_GET['people'] == '3'){echo'selected';}?> value="3">3</option>
                                    <option <?php if($_GET['people'] == '4'){echo'selected';}?> value="4">4</option>
                                    <option <?php if($_GET['people'] == '5'){echo'selected';}?> value="5">5</option>
                                    <option <?php if($_GET['people'] == '6'){echo'selected';}?> value="6">6</option>
                                    <option <?php if($_GET['people'] == '7'){echo'selected';}?> value="7">7</option>
                                    <option <?php if($_GET['people'] == '8'){echo'selected';}?> value="8">8</option>
                                    <option <?php if($_GET['people'] == '9'){echo'selected';}?> value="9">9</option>
                                    <option <?php if($_GET['people'] == '10'){echo'selected';}?> value="10">10</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Arrival Date?</label>
                                <input name="start" id="startDate" class="form-control custom-form-control datePickerStart" placeholder="Any" onchange="unlockDeparture()">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Departure Date?</label>
                                <input name="end" id="endDate" class="form-control custom-form-control datePickerEnd custom-disabled" placeholder="Select Arrival" disabled onchange="validateDeparture()">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                            <div class="form-group">
                                <label>Features</label>
                                <button type="button" id="moreOptionsButton" class="form-control custom-form-control" data-toggle="modal" data-target="#moreOptions">None Selected</button>
                            </div>
                        </div>
                        <div class="col-12 col-xl-2 text-center text-xl-right">
                            <button type="button" id="submitbutton" class="button button-search" onclick="basicSubmitForm()">Search <i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="modal" id="moreOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button class="custom-modal-close" data-dismiss="modal" onclick="countSelectedOptions()"><i class="fas fa-times"></i></button>
                                    <div class="row">
                                        <div class="col-12 col-lg-4 mb-4">
                                            <h6 class="mb-4">External Features</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Decking
                                                    <input id="options-decking" class="optionsCheckbox" type="checkbox" name="decking" value="1" <?php if($_GET['decking'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Double Glazing
                                                    <input id="options-double-glazing" class="optionsCheckbox" type="checkbox" name="double-glazing" value="1" <?php if($_GET['double-glazing'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Private Hot Tub
                                                    <input id="options-hot-tub" class="optionsCheckbox" type="checkbox" name="hot-tub" value="1" <?php if($_GET['hot-tub'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Private Parking
                                                    <input id="options-private-parking" class="optionsCheckbox" type="checkbox" name="private-parking" value="1" <?php if($_GET['private-parking'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Patio
                                                    <input id="options-patio" class="optionsCheckbox" type="checkbox" name="patio" value="1" <?php if($_GET['patio'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <h6 class="mb-4 mt-4">Property Type</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Caravan
                                                    <input id="options-caravan" class="optionsCheckbox" type="checkbox" name="caravan" value="1" <?php if($_GET['caravan'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Chalet
                                                    <input id="options-chalet" class="optionsCheckbox" type="checkbox" name="chalet" value="1" <?php if($_GET['chalet'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Mini Lodge
                                                    <input id="options-mini-lodge" class="optionsCheckbox" type="checkbox" name="mini-lodge" value="1" <?php if($_GET['mini-lodge'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Full Lodge
                                                    <input id="options-full-lodge" class="optionsCheckbox" type="checkbox" name="full-lodge" value="1" <?php if($_GET['full-lodge'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4 mb-4">
                                            <h6 class="mb-4">Internal Features</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Bath
                                                    <input id="options-bath" class="optionsCheckbox" type="checkbox" name="bath" value="1" <?php if($_GET['bath'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Bedroom TV
                                                    <input id="options-bedroom-tv" class="optionsCheckbox" type="checkbox" name="bedroom-tv" value="1" <?php if($_GET['bedroom-tv'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Central Heating
                                                    <input id="options-central-heating" class="optionsCheckbox" type="checkbox" name="central-heating" value="1" <?php if($_GET['central-heating'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Dishwasher
                                                    <input id="options-dishwasher" class="optionsCheckbox" type="checkbox" name="dishwasher" value="1" <?php if($_GET['dishwasher'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">En Suite
                                                    <input id="options-en-suite" class="optionsCheckbox" type="checkbox" name="en-suite" value="1" <?php if($_GET['en-suite'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Second WC
                                                    <input id="options-second-wc" class="optionsCheckbox" type="checkbox" name="second-wc" value="1" <?php if($_GET['second-wc'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Sky TV
                                                    <input id="options-sky-tv" class="optionsCheckbox" type="checkbox" name="sky-tv" value="1" <?php if($_GET['sky-tv'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Tumble Dryer
                                                    <input id="options-tumble-dryer" class="optionsCheckbox" type="checkbox" name="tumble-dryer" value="1" <?php if($_GET['tumble-dryer'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Washing Machine
                                                    <input id="options-washing-machine" class="optionsCheckbox" type="checkbox" name="washing-machine" value="1" <?php if($_GET['washing-machine'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">1 Small Pet Friendly
                                                    <input id="options-small-pets" class="optionsCheckbox" type="checkbox" name="small-pets" value="1" <?php if($_GET['small-pets'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">1 or 2 Pets (Any Size) Friendly
                                                    <input id="options-large-pets" class="optionsCheckbox" type="checkbox" name="large-pets" value="1" <?php if($_GET['large-pets'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>

                                        </div>
                                        <div class="col-12 col-lg-4 mb-4">
                                            <h6 class="mb-4">Park Location</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Close To Entertainment
                                                    <input id="options-close-to-entertainment" class="optionsCheckbox" type="checkbox" name="close-to-entertainment" value="1" <?php if($_GET['close-to-entertainment'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Close to Beach
                                                    <input id="options-close-to-beach" class="optionsCheckbox" type="checkbox" name="close-to-beach" value="1" <?php if($_GET['close-to-beach'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Central
                                                    <input id="options-central" class="optionsCheckbox" type="checkbox" name="central" value="1" <?php if($_GET['central'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Edge of Park
                                                    <input id="options-edge-of-park" class="optionsCheckbox" type="checkbox" name="edge-of-park" value="1" <?php if($_GET['edge-of-park'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Views
                                                    <input id="options-views" class="optionsCheckbox" type="checkbox" name="views" value="1" <?php if($_GET['views'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Ash View
                                                    <input id="options-ash-view" class="optionsCheckbox" type="checkbox" name="ash-view" value="1" <?php if($_GET['ash-view'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Kentish View
                                                    <input id="options-kentish-view" class="optionsCheckbox" type="checkbox" name="kentish-view" value="1" <?php if($_GET['kentish-view'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Long Beach Landings
                                                    <input id="options-long-beach-landings" class="optionsCheckbox" type="checkbox" name="long-beach-landings" value="1" <?php if($_GET['long-beach-landings'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Maple Park / Maple Ridge
                                                    <input id="options-maple-park-ridge" class="optionsCheckbox" type="checkbox" name="maple-park-ridge" value="1" <?php if($_GET['maple-park-ridge'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">South Beach Landings
                                                    <input id="options-south-beach-landings" class="optionsCheckbox" type="checkbox" name="south-beach-landings" value="1" <?php if($_GET['south-beach-landings'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Sussex Park
                                                    <input id="options-sussex-park" class="optionsCheckbox" type="checkbox" name="sussex-park" value="1" <?php if($_GET['sussex-park'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Willow Way
                                                    <input id="options-willow-way" class="optionsCheckbox" type="checkbox" name="willow-way" value="1" <?php if($_GET['willow-way'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="button button-modal-done" data-dismiss="modal" onclick="countSelectedOptions(); basicSubmitForm()">Search <i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container" style="margin-top:80px;">
                <div class="row justify-content-center mb-4">
                    <div class="col-12 col-md-10 text-center">
                        <h3 class="text-white">Or find a specific property</h3>
                    </div>
                </div>
                <form id="specificSearchForm" onsubmit="specificSearch(); return false;">
                    <input type="hidden" name="test">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Property Name or Code</label>
                                <input id="query" class="form-control custom-form-control">
                            </div>
                        </div>
                        <div class="col-12 col-xl-2 text-center text-xl-right">
                            <button type="button" id="submitbuttonspecific" class="button button-search" onclick="specificSearch()">Find</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container mb-4">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 mb-4 text-center">
                        <?php graphite_pageSection('EUEDWCOPKRL');?>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <p><strong>All our Holiday Park Accommodation include the following as standard:</strong></p>
                        <ul class="featureList">
                            <li>Television with Freeview</li>
                            <li>Fully Equipped Kitchen</li>
                            <li>Cleaning Pack</li>
                            <li>Bed Linen & Towels</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-secondary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <?php graphite_pageSection('6ZLR0JI62V9');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="half-half background-yellow background-stitch-white">
            <div class="text-half">
                <div class="container">
                    <?php graphite_pageSection('3YBT3LG4RID');?>
                    <img src="/images/park5.jpg" alt="">
                </div>
            </div>
            <div class="image-half" style="background-image: url(/images/park5.jpg)"></div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <?php graphite_pageSection('0VY1GM5RM4N');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row">
                    <?php graphite_gallery('12FWIDZ91HF');?>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-secondary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <?php graphite_pageSection('NOS58B1F9EI');?>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="container-fluid page-section background-default">
            <div class="container mb-4">
                <div class="row justify-content-center mb-4">
                    <div class="col-12 col-md-10 text-center">
                        <h2 class="text-blue">Find a specific property</h2>
                    </div>
                </div>
                <form id="specificSearchForm" onsubmit="specificSearch(); return false;">
                    <input type="hidden" name="test">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Property Name or Code</label>
                                <input id="query" class="form-control custom-form-control">
                            </div>
                        </div>
                        <div class="col-12 col-xl-2 text-center text-xl-right">
                            <button type="button" id="submitbuttonspecific" class="button button-search" onclick="specificSearch()">Find</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
-->
        <div class="modal fade" id="specificSearchResults" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="caravan-modal-title">Search Results for '<strong id="resultQuery"></strong>' <i id="spinner" class="fas fa-spinner fa-pulse"></i></p>
                        <div class="row" id="propertyResults">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button button-modal-close" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php
        graphite_GraphiteFooter_AddItem('Edit Gallery','gallery/album/?id=12FWIDZ91HF');
        graphite_GraphiteFooter();
        ?>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="/js/searchForm.js?v4"></script>
    </body>
</html>
