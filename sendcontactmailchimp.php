<?php
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require $ROOTLOCATION.'php/PHPMailer/src/Exception.php';
require $ROOTLOCATION.'php/PHPMailer/src/PHPMailer.php';
require $ROOTLOCATION.'php/PHPMailer/src/SMTP.php';

$formValid = true;

if (empty($_POST["name"])) {
    $contactForm->status = 'error';
    $contactForm->response = 'Your name is missing.';
    $formValid = false;
    echo json_encode($contactForm);
    exit;
} else {
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$_POST["name"])) {
        $contactForm->status = 'error';
        $contactForm->response = 'Your name is invalid.';
        $formValid = false;
        echo json_encode($contactForm);
        exit;
    }
}

if (empty($_POST["email"])) {
    $contactForm->status = 'error';
    $contactForm->response = 'Your email address is missing.';
    $formValid = false;
    echo json_encode($contactForm);
    exit;
} else {
    // check if e-mail address is well-formed
    if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $contactForm->status = 'error';
        $contactForm->response = 'Your email address is invalid.';
        $formValid = false;
        echo json_encode($contactForm);
        exit;
    }
}

if (empty($_POST["phone"])) {
    $contactForm->status = 'error';
    $contactForm->response = 'Your phone number is missing.';
    $formValid = false;
    echo json_encode($contactForm);
    exit;
} else {
    $phone = str_replace(" ","",$_POST["phone"]);
    if(!preg_match("/^[0-9]{11}$/", $phone)) {
        $contactForm->status = 'error';
        $contactForm->response = 'Your phone number is invalid.';
        $formValid = false;
        echo json_encode($contactForm);
        exit;
    }
}

if(empty($_POST['message'])){
    $contactForm->status = 'error';
    $contactForm->response = 'Your message is missing.';
    $formValid = false;
    echo json_encode($contactForm);
    exit;
}else{
    $_POST['message'] = nl2br($_POST['message']);
}

if($formValid === true){
    
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $GENERAL_SETTINGS->get('smtp', 'host');  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $GENERAL_SETTINGS->get('smtp', 'username');                 // SMTP username
        $mail->Password = $GENERAL_SETTINGS->get('smtp', 'password');                           // SMTP password
        $mail->SMTPSecure = $GENERAL_SETTINGS->get('smtp', 'SMTPSecure');                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $GENERAL_SETTINGS->get('smtp', 'port');                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($_POST["email"], $_POST["name"]);
        //$mail->addAddress('ashley@mjsmedia.co.uk');     // Add a recipient
        $mail->addAddress('info@cambergetaways.co.uk');     // Add a recipient
        $mail->addBCC('ashley@mjsmedia.co.uk');

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Camber Getaways Mailchimp Contact Form';
        $mail->Body    = '
        <p><strong>Name:</strong> '.$_POST["name"].'</p>
        <p><strong>Email:</strong> '.$_POST["email"].'</p>
        <p><strong>Phone:</strong> '.$phone.'</p>
        <p>'.$_POST["message"].'</p>
        ';

        $mail->send();
        
        $contactForm->status = 'success';
        $contactForm->response = 'Your message has been sent.';
        echo json_encode($contactForm);
        exit;
        
    } catch (Exception $e) {
        
        $contactForm->status = 'error';
        $contactForm->response = 'Mailer Error: '.$mail->ErrorInfo;
        echo json_encode($contactForm);
        exit;
    }
    
}else{
    $contactForm->status = 'error';
    $contactForm->response = 'An unknown error occured and your message could not be sent.';
    echo json_encode($contactForm);
    exit;
}

$contactForm->status = 'error';
$contactForm->response = 'An unknown error occured.';
echo json_encode($contactForm);
exit;

?>