<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'About us';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Camber Getaways is a local business, originally built up from 2 self owned caravans in Camber Sands.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(/images/dogs0.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>About Us</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <h2 class="mb-4">Who are we?</h2>
                        <p class="lead">Camber Getaways is a local business, originally built up from 2 self owned caravans in Camber Sands.</p>
                        <p>We now manage almost 100 properties, yet still maintain the same high levels of service and personal touches as we did in the beginning. We are a mixed bunch, who all believe in offering the best local accommodation to the many people who love Camber Sands as much as we do.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-secondary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <h2 class="mb-4">Our Properties</h2>
                        <p class="lead">Camber Getaways offers a huge selection of caravans & lodges on Parkdean Resorts, all privately owned but managed by us. We also look after a unique few local houses, which enables us to give everyone wanting to visit a fantastic choice of accommodation.</p>
                        <p>We ensure all our properties are well equipped for self catering, along with everything to make your break comfortable and enjoyable.</p>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
    </body>
</html>