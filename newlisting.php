<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Search';            //Page Title
$pageSlug = '';             //Page Slug

$seoActive = false;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = '';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid search-section background-blue background-stitch-white" style="transition-duration:1s">
            <div class="container">
                <div class="collapse dont-collapse-desktop" id="searchOptions">
                    <form>
                        <div class="row justify-content-center mb-4">
                            <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                                <div class="form-group">
                                    <label>How many people?</label>
                                    <select id="people" class="form-control custom-form-select">
                                        <option <?php if($_GET['people'] == ''){echo'selected';}?> value="">Any</option>
                                        <option <?php if($_GET['people'] == '1'){echo'selected';}?> value="1">1</option>
                                        <option <?php if($_GET['people'] == '2'){echo'selected';}?> value="2">2</option>
                                        <option <?php if($_GET['people'] == '3'){echo'selected';}?> value="3">3</option>
                                        <option <?php if($_GET['people'] == '4'){echo'selected';}?> value="4">4</option>
                                        <option <?php if($_GET['people'] == '5'){echo'selected';}?> value="5">5</option>
                                        <option <?php if($_GET['people'] == '6'){echo'selected';}?> value="6">6</option>
                                        <option <?php if($_GET['people'] == '7'){echo'selected';}?> value="7">7</option>
                                        <option <?php if($_GET['people'] == '8'){echo'selected';}?> value="8">8</option>
                                        <option <?php if($_GET['people'] == '9'){echo'selected';}?> value="9">9</option>
                                        <option <?php if($_GET['people'] == '10'){echo'selected';}?> value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3 col-xl-3">
                                <div class="form-group">
                                    <label>When?</label>
                                    <input id="startDate" class="form-control custom-form-control datePicker" placeholder="Any" value="<?php echo $_GET['when'];?>">
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                                <div class="form-group">
                                    <label>How long?</label>
                                    <select id="nights" class="form-control custom-form-select">
                                        <option <?php if($_GET['nights'] == ''){echo'selected';}?> value="">Any</option>
                                        <option <?php if($_GET['nights'] == '3-4'){echo'selected';}?> value="3-4">3 or 4 Nights</option>
                                        <option <?php if($_GET['nights'] == '7'){echo'selected';}?> value="7">7 Nights</option>
                                        <option <?php if($_GET['nights'] == '14'){echo'selected';}?> value="14">14 Nights</option>
                                        <option <?php if($_GET['nights'] == '21'){echo'selected';}?> value="21">21 Nights</option>
                                        <option <?php if($_GET['nights'] == '28'){echo'selected';}?> value="28">28 Nights</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3 col-xl-3">
                                <div class="form-group">
                                    <label>Features</label>
                                    <button type="button" id="moreOptionsButton" class="form-control custom-form-control" data-toggle="modal" data-target="#moreOptions">None Selected</button>
                                </div>
                            </div>
                            <div class="col-12 col-xl-2 text-center text-xl-right">
                                <button type="button" id="submit" class="button button-search" onclick="submitFormGeneral();hideSearchOptions()">Search</button>
                            </div>
                        </div>
                        <div class="modal" id="moreOptions" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12 col-lg-4 mb-4">
                                                <h6 class="mb-4">External Features</h6>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Decking
                                                        <input id="options-decking" class="optionsCheckbox" type="checkbox" name="decking" value="1" <?php if($_GET['decking'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Double Glazing
                                                        <input id="options-double-glazing" class="optionsCheckbox" type="checkbox" name="double-glazing" value="1" <?php if($_GET['double-glazing'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Private Hot Tub
                                                        <input id="options-hot-tub" class="optionsCheckbox" type="checkbox" name="hot-tub" value="1" <?php if($_GET['hot-tub'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Private Parking
                                                        <input id="options-private-parking" class="optionsCheckbox" type="checkbox" name="private-parking" value="1" <?php if($_GET['private-parking'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Patio
                                                        <input id="options-patio" class="optionsCheckbox" type="checkbox" name="patio" value="1" <?php if($_GET['patio'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <h6 class="mb-4 mt-4">Property Type</h6>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Caravan
                                                        <input id="options-caravan" class="optionsCheckbox" type="checkbox" name="caravan" value="1" <?php if($_GET['caravan'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Chalet
                                                        <input id="options-chalet" class="optionsCheckbox" type="checkbox" name="chalet" value="1" <?php if($_GET['chalet'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Mini Lodge
                                                        <input id="options-mini-lodge" class="optionsCheckbox" type="checkbox" name="mini-lodge" value="1" <?php if($_GET['mini-lodge'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Full Lodge
                                                        <input id="options-full-lodge" class="optionsCheckbox" type="checkbox" name="full-lodge" value="1" <?php if($_GET['full-lodge'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Holiday Home
                                                        <input id="options-holiday-home" class="optionsCheckbox" type="checkbox" name="holiday-home" value="1" <?php if($_GET['holiday-home'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4 mb-4">
                                                <h6 class="mb-4">Internal Features</h6>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Bath
                                                        <input id="options-bath" class="optionsCheckbox" type="checkbox" name="bath" value="1" <?php if($_GET['bath'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Bedroom TV
                                                        <input id="options-bedroom-tv" class="optionsCheckbox" type="checkbox" name="bedroom-tv" value="1" <?php if($_GET['bedroom-tv'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Central Heating
                                                        <input id="options-central-heating" class="optionsCheckbox" type="checkbox" name="central-heating" value="1" <?php if($_GET['central-heating'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Dishwasher
                                                        <input id="options-dishwasher" class="optionsCheckbox" type="checkbox" name="dishwasher" value="1" <?php if($_GET['dishwasher'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">En Suite
                                                        <input id="options-en-suite" class="optionsCheckbox" type="checkbox" name="en-suite" value="1" <?php if($_GET['en-suite'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Second WC
                                                        <input id="options-second-wc" class="optionsCheckbox" type="checkbox" name="second-wc" value="1" <?php if($_GET['second-wc'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Sky TV
                                                        <input id="options-sky-tv" class="optionsCheckbox" type="checkbox" name="sky-tv" value="1" <?php if($_GET['sky-tv'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Tumble Dryer
                                                        <input id="options-tumble-dryer" class="optionsCheckbox" type="checkbox" name="tumble-dryer" value="1" <?php if($_GET['tumble-dryer'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Washing Machine
                                                        <input id="options-washing-machine" class="optionsCheckbox" type="checkbox" name="washing-machine" value="1" <?php if($_GET['washing-machine'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">1 Small Pet Friendly
                                                        <input id="options-small-pets" class="optionsCheckbox" type="checkbox" name="small-pets" value="1" <?php if($_GET['small-pets'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">1 or 2 Pets (Any Size) Friendly
                                                        <input id="options-large-pets" class="optionsCheckbox" type="checkbox" name="large-pets" value="1" <?php if($_GET['large-pets'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>

                                            </div>
                                            <div class="col-12 col-lg-4 mb-4">
                                                <h6 class="mb-4">Location</h6>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Close To Entertainment
                                                        <input id="options-close-to-entertainment" class="optionsCheckbox" type="checkbox" name="close-to-entertainment" value="1" <?php if($_GET['close-to-entertainment'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Close to Beach
                                                        <input id="options-close-to-beach" class="optionsCheckbox" type="checkbox" name="close-to-beach" value="1" <?php if($_GET['close-to-beach'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Central
                                                        <input id="options-central" class="optionsCheckbox" type="checkbox" name="central" value="1" <?php if($_GET['central'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Edge of Park
                                                        <input id="options-edge-of-park" class="optionsCheckbox" type="checkbox" name="edge-of-park" value="1" <?php if($_GET['edge-of-park'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Views
                                                        <input id="options-views" class="optionsCheckbox" type="checkbox" name="views" value="1" <?php if($_GET['views'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Ash View
                                                        <input id="options-ash-view" class="optionsCheckbox" type="checkbox" name="ash-view" value="1" <?php if($_GET['ash-view'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Kentish View
                                                        <input id="options-kentish-view" class="optionsCheckbox" type="checkbox" name="kentish-view" value="1" <?php if($_GET['kentish-view'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Long Beach Landings
                                                        <input id="options-long-beach-landings" class="optionsCheckbox" type="checkbox" name="long-beach-landings" value="1" <?php if($_GET['long-beach-landings'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Maple Park / Maple Ridge
                                                        <input id="options-maple-park-ridge" class="optionsCheckbox" type="checkbox" name="maple-park-ridge" value="1" <?php if($_GET['maple-park-ridge'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">South Beach Landings
                                                        <input id="options-south-beach-landings" class="optionsCheckbox" type="checkbox" name="south-beach-landings" value="1" <?php if($_GET['south-beach-landings'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Sussex Park
                                                        <input id="options-sussex-park" class="optionsCheckbox" type="checkbox" name="sussex-park" value="1" <?php if($_GET['sussex-park'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="new-checkbox">
                                                    <label class="new-checkbox-container">Willow Way
                                                        <input id="options-willow-way" class="optionsCheckbox" type="checkbox" name="willow-way" value="1" <?php if($_GET['willow-way'] == 1){echo 'checked';}?>>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="button button-modal-close" data-dismiss="modal" onclick="countSelectedOptions()">Close</button>
                                        <button type="button" class="button button-modal-done" data-dismiss="modal" onclick="countSelectedOptions();submitFormGeneral();hideSearchOptions()">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <button id="searchOptionsButton" class="button m-0 w-100" type="button" onclick="showSearchOptions()">Search Options</button>
                    </div>
                </div>
                <div class="row justify-content-center mt-4">
                    <div class="col-12 col-md-4 col-lg-3">
                        <div class="form-group m-0">
                            <select id="sortBy" class="custom-outline-select" onchange="submitFormGeneral()">
                                <option value="priceAsc">Price: Low to High</option>
                                <option value="priceDesc">Price: High to Low</option>
                                <option value="sleepsAsc">Capacity: Low to High</option>
                                <option value="sleepsDesc">Capacity: High to Low</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid search-section background-default background-stitch-blue">
            <div class="container">
                <div class="row" id="properties">
                    <!--
                    <div class="col-12 col-md-6 col-xl-4 animated pulse">
                        <a href="#">
                            <div class="property" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/171129638.jpg)">
                                <div class="infobadge infobadge-price">From £158.00</div>
                                <div class="details">
                                    <p class="name">Willerby Vacation 2B - AV30</p>
                                    <ul class="details-icons">
                                        <li><i class="fas fa-male"></i> 16</li>
                                        <li><i class="fas fa-bed"></i> 2</li>
                                        <li><i class="fas fa-camera"></i> 10</li>
                                        <li><i class="fas fa-paw"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    -->
                    <div class="col-12 col-md-6 col-xl-4 animated pulse">
                        <a href="#">
                            <div class="property">
                                <div class="main-img" style="background: url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/171543097.jpg)"></div>
                                <div class="second-img" style="background: url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/171543098.jpg)"></div>
                                <div class="third-img" style="background: url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/171543099.jpg)"></div>
                                <div class="infobadge infobadge-price">From <span class="strike">£158.00</span></div>
                                <div class="infobadge infobadge-discount">£130.00</div>
                                <div class="details">
                                    <p class="name">Willerby Vacation 2B - AV30</p>
                                    <ul class="details-icons">
                                        <li><i class="fas fa-male"></i> 16</li>
                                        <li><i class="fas fa-bed"></i> 2</li>
                                        <li><i class="fas fa-camera"></i> 10</li>
                                        <li><i class="fas fa-paw"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
    </body>
</html>