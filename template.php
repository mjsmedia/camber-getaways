<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = '';            //Page Title
$pageSlug = '';             //Page Slug

$seoActive = false;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = '';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <!--<div class="container-fluid hero scrollReveal" style="background: linear-gradient(rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)),url(/images/placeholder/ph4.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center">
                        <h1 class="mb-2">Something big</h1>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="#" class="button">Button Label</a>
                    </div>
                </div>
            </div>
        </div>-->
        <!--<div id="carousel" class="carousel slide scrollReveal" data-ride="carousel" data-pause=false data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item hero active" style="background: linear-gradient(rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)),url(/images/placeholder/ph4.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 align-self-center text-center">
                                <h1 class="mb-2">Something big</h1>
                                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <a href="#" class="button">Button Label</a>
                                <a href="#" class="button-text mt-4">More info <span class="lnr lnr-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item hero" style="background: linear-gradient(rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)),url(/images/placeholder/ph1.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 align-self-center text-center">
                                <h1 class="mb-2">Something else</h1>
                                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <a href="#" class="button">Button Label</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item hero" style="background: linear-gradient(rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)),url(/images/placeholder/ph2.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 align-self-center text-center">
                                <h1 class="mb-2">More things</h1>
                                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <a href="#" class="button">Button Label</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="lnr lnr-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="lnr lnr-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
-->
        <div class="half-half scrollReveal background-blue">
            <div class="image-half" style="background-image: url(/images/dog-friendly0.jpg)"></div>
            <div class="text-half">
                <div class="container">
                    Some words
                    <img src="/images/dog-friendly0.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue scrollReveal">
            <div class="container">
                <h1 class="text-blue">Heading 1</h1>
                <h2 class="text-yellow">Heading 2</h2>
                <h3 class="text-white">Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
                <h6>Heading 6</h6>
                <p><a href="#">Lorem ipsum dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="#" class="button">Button Label</a>
                
                <div class="new-checkbox">
                    <label class="new-checkbox-container">One swrgwrg wrgawrgawrg rwg wRGwg gwGAERG RGAERWGAERG
                        <input type="checkbox" checked>
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="new-checkbox">
                    <label class="new-checkbox-container">One
                        <input type="checkbox" checked>
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="new-checkbox">
                    <label class="new-checkbox-container">One
                        <input type="checkbox" checked>
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="new-checkbox">
                    <label class="new-checkbox-container">One
                        <input type="checkbox" checked>
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="new-checkbox">
                    <label class="new-checkbox-container">One
                        <input type="checkbox" checked>
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
        
        <div class="container-fluid page-section background-secondary background-stitch-white scrollReveal">
            <div class="container">
                <h1>Heading 1</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
                <h6>Heading 6</h6>
                <p><a href="#">Lorem ipsum dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="#" class="button">Button Label</a>
            </div>
        </div>
        
        <div class="container-fluid page-section background-default background-stitch-yellow scrollReveal">
            <div class="container">
                <h1>Heading 1</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
                <h6>Heading 6</h6>
                <p><a href="#">Lorem ipsum dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="#" class="button">Button Label</a>
            </div>
        </div>
        
        <div class="container-fluid page-section background-blue scrollReveal">
            <div class="container">
                <h1>Heading 1</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
                <h6>Heading 6</h6>
                <p><a href="#">Lorem ipsum dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="#" class="button">Button Label</a>
            </div>
        </div>
        <div class="container-fluid page-section background-yellow scrollReveal">
            <div class="container">
                <h1>Heading 1</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
                <h6>Heading 6</h6>
                <p><a href="#">Lorem ipsum dolor sit amet</a>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="#" class="button">Button Label</a>
            </div>
        </div>
        
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
    </body>
</html>