<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Landlords';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Contact us and arrange a meeting with one of our dedicated team to discuss renting out your caravan, lodge, or chalet.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
		<div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(/images/holiday-homes.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Owners</h1>
						<a href="https://secure.supercontrol.co.uk/managers/index.asp?se=55492151B1EF1F60B1AF03B2456F4DE1C8D081258588A52A5207FDCC7362F0FB" target="_blank" class="button m-0">Owners Login</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
						<h2>Ready</h2>
                        <p class="lead">Contact us and arrange a meeting with one of our dedicated team to discuss renting out your caravan, lodge, or chalet. They will be able to advise you how to best equip and furnish your property to maximise your bookings. You are under no obligation to sign up at this point, but we will provide you with all the information you require, including possible rates. You are also welcome to ask any questions which you are unsure of.</p>
                    </div>
                </div>
            </div>
        </div>
		<div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
						<h2>Set</h2>
                        <p class="lead">Once you have decided you would like to rent with us, we will send you an online form to complete which helps us to create your advert on our website and other channels. Part of the set up will include photography, carried out by our professional photographer. We will also at this point run all our checks including safety certificate dates, insurance, cleaning, and maintenance.</p>
                    </div>
                </div>
            </div>
        </div>
		<div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
						<h2>Let</h2>
                        <p class="lead">Once all checks are carried out and we both are happy to proceed, we will publish your property online and begin the journey of receiving bookings and letting your property.</p>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>

        <script src="/js/searchForm.js"></script>
    </body>
</html>
