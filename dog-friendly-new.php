<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Dog Friendly';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'At Camber Getaways we understand that pets are family, and luckily we have caravans that are dog friendly so your furry friends can join you on your holiday!';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(/images/dogs0.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Dog Friendly</h1>
                        <h3 class="mb-0">We love dogs!</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('XDST02ZQ2FT');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-secondary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('KZGAEUQ5RWI');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="half-half background-blue background-stitch-white">
            <div class="image-half" style="background-image: url(/images/dogs1.jpg)"></div>
            <div class="text-half">
                <div class="container">
                    <?php graphite_pageSection('C5FX58IDQQP');?>
                    <a href="/search/?large-pets=1" class="button m-0">Search Dog Friendly Properties</a>
                    <img src="/images/dogs1.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('O6ROAE14RU7');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 text-center">
                        <h2>Where can we take our furry friends?</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 col-md-6">
                        <img style="width:100%;height:auto;border-radius:10px;" src="/images/dog-friendly/dog.jpg">
                    </div>
                    <div class="col-12 col-md-6 my-4">
                        <h3>Camber Sands Beach</h3>
                        <p>Of course, the endless stretch of beach is one of the best dog walking places! Your fury friends can enjoy acres of sand dunes and sea all year round! During 1st May to 30th September there is an area that doggies aren’t allowed.</p>
                        <a href="http://www.rother.gov.uk/CHttpHandler.ashx?id=7114&p=0" target="_blank" class="button">View the Map</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 col-md-6">
                        <img style="width:100%;height:auto;border-radius:10px;" src="/images/dog-friendly/rye-harbour.png">
                    </div>
                    <div class="col-12 col-md-6 my-4">
                        <h3>Rye Harbour Nature Reserve</h3>
                        <h6>Harbour Road, Rye TN31 7TU (12 Min Drive)</h6>
                        <p>A coastal nature reserve with miles and miles of flat footpaths to explore. Habitats include saltmarsh, shingle, saline lagoons, grazing marsh and reedbed. A fantastic place for a peaceful walk away from the hustle and bustle of normal life. Lots of parking, dog friendly cafes and pubs too - for your whole family to enjoy.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 col-md-6">
                        <img style="width:100%;height:auto;border-radius:10px;" src="/images/dog-friendly/hythe-canal.jpg">
                    </div>
                    <div class="col-12 col-md-6 my-4">
                        <h3>Canal Hythe</h3>
                        <h6>Port Lympne, CT21 4NB (36 Min Drive)</h6>
                        <p>The carpark is free and it's also very near to the microbrewery which may be worth a try! Starting at the car park in West Hythe follow the Royal Military Canal path west, after about 1km (you will be running parallel with Port Lympne zoo), there is a footpath on the right that takes you up through the park. Or you can continue to walk parallel to the zoo, amazing view of the animals! (Sometimes you can see more than when you visit the zoo) There are picnic areas along the way and off the lead areas – perfect for a family day out with the dog.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 col-md-6">
                        <img style="width:100%;height:auto;border-radius:10px;" src="/images/dog-friendly/dering-woods.png">
                    </div>
                    <div class="col-12 col-md-6 my-4">
                        <h3>Dering Woods</h3>
                        <h6>Smarden Bell Rd, Smarden, Ashford TN27 0SY (33 Min Drive)</h6>
                        <p>Free parking! This wood is part of the woodland trust, so the paths are kept clear all year round. Beautiful, easy going walk. It can get quite muddy during the winter months so be sure to bring your wellies! A huge area to explore… lots of local people use the woods as their local walking spot so lots of dogs will be there.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-12 text-center">
                        <h2 class="text-blue">Places to visit where humans are invited too!</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <a target="_blank" href="https://www.facebook.com/kitkatcafecamber">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dog-friendly/kit-kat-cafe.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Kit Kat Café</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Camber Sands, Rye TN31 7RH</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a target="_blank" href="http://theowlcamber.co.uk/">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dog-friendly/owl-pub.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Owl Pub</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>11 Old Lydd Rd, Camber, TN31 7RE</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a target="_blank" href="https://www.thestandardinnrye.co.uk/">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dog-friendly/the-standard.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>The Standard Inn</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Rye, TN31 7EN</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a target="_blank" href="https://www.ramblinns.com/the-globe-inn-rye">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dog-friendly/globe-inn.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>The Globe Inn</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>10 Military Road, Rye, TN31 7NX</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a target="_blank" href="https://www.williamtheconqueror.co.uk/">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dog-friendly/william-conqueror.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>William the Conqueror</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Rye Harbour, TN31 7TU</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a target="_blank" href="http://www.yprescastleinn.co.uk/">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dog-friendly/ypres-castle.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Ypres Castle Inn</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Gungarden, TN31 7HH</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
    </body>
</html>