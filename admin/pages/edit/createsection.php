<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkContentActive('pages');

graphite_checkLock($_SESSION['userid']);

$contentid = graphite_randomString();
$pageid = $_POST['pageid'];
$name = $_POST['name'];

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO pages_content (pageid,contentid,name)
    VALUES ('$pageid','$contentid','$name');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Section created.');
    header('Location: /admin/pages/edit/editsection.php?id='.$contentid);
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/pages/edit/?id='.$pageid);
}

mysqli_close($conn);
?>