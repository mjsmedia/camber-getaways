<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];
$pageid = graphite_database_lookupValue('pages_content', 'contentid', $id, 'pageid');

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM pages_content WHERE contentid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'Section has been deleted.');
    header("Location: /admin/pages/edit?id=".$pageid);
} else {
    graphite_notification('error', 'Could not delete section.');
    header("Location: /admin/pages/edit?id=".$pageid);
}

mysqli_close($conn);
?>