<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('pages');

$id = $_GET['id'];

graphite_item_exists('pages', 'pageid', $id , '/admin/pages');

$pageTitle = 'Edit Page Content';            //Page Title
$pageSlug = 'pages';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-file-alt"></i> Pages <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('pages', 'pageid', $id, 'name');?></small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('pages', 'pageid', $id, 'name');?></h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/pages"><i class="fas fa-angle-left"></i> All Pages</a>
                    <a class="btn btn-secondary" href="<?php echo graphite_database_lookupValue('pages', 'pageid', $id, 'location');?>">View Page <i class="fa fa-eye"></i></a>
                    <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){echo'
                    <a class="btn btn-secondary" href="#" data-toggle="modal" data-target=".pageSettings">Page Settings <i class="fa fa-cog"></i></a>
                    ';}?>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <?php graphite_pages_sections($id);?>
                    <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') > 0){
    echo'
                        <a href="#" data-toggle="modal" data-target=".newSection">
                            <div class="card card-inverse" style="margin-bottom:10px; background-color: #333; border-color: #333;">
                                <div class="card-block text-center">
                                    <i class="fas fa-plus-square fa-3x"></i>
                                    <h4 class="card-title">New Section</h4>
                                </div>
                            </div>
                        </a>
                    ';}?>
                </div>
            </div>
        </div>

        <div class="modal fade pageSettings" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving Changes')">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label>Title <small class="text-danger">Required</small></label>
                                        <input type="text" name="name" class="form-control" required placeholder="Contact" value="<?php echo graphite_database_lookupValue('pages', 'pageid', $id, 'name');?>">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label>Location <small class="text-danger">Required</small></label>
                                        <input type="text" name="location" class="form-control" required placeholder="/contact" value="<?php echo graphite_database_lookupValue('pages', 'pageid', $id, 'location');?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="pageid" value="<?php echo $id;?>">
                            <a href="delete.php?id=<?php echo $id;?>" class="btn btn-danger">Delete Page</a>
                            <button id="save" type="submit" class="btn btn-primary">Save Changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade newSection" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="createsection.php" method="post" onsubmit="buttonFeedback('save', 'Creating Section')">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Name <small class="text-muted">Required</small></label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="pageid" value="<?php echo $id;?>">
                            <button id="save" type="submit" class="btn btn-primary">Create Section</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>