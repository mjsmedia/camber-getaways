<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkContentActive('pages');

graphite_checkLock($_SESSION['userid']);

$pageid = $_POST['pageid'];
$name = $_POST['name'];
$location = $_POST['location'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE pages
SET name= '$name', location= '$location'
WHERE pageid = '$pageid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/pages/edit/?id='.$pageid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/pages/edit/?id='.$pageid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>