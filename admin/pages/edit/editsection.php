<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('pages');

graphite_checkLock($_SESSION['userid']);

$sectionid = $_GET['id'];
$pageid = graphite_database_lookupValue('pages_content', 'contentid', $sectionid, 'pageid');

graphite_item_exists('pages_content', 'contentid', $sectionid , '/admin/pages');

$pageTitle = 'Edit Page Content';            //Page Title
$pageSlug = 'pages';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-file-alt"></i> Pages <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('pages', 'pageid', $pageid, 'name');?></small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('pages', 'pageid', $pageid, 'name');?></h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/pages/edit/?id=<?php echo $pageid;?>"><i class="fas fa-angle-left"></i> All Sections</a>
                    <a class="btn btn-secondary" href="<?php echo graphite_database_lookupValue('pages', 'pageid', $pageid, 'location');?>">View Page <i class="fa fa-eye"></i></a>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <form action="savesection.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
                        <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){echo'
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" required value="'.graphite_database_lookupValue('pages_content', 'contentid', $sectionid, 'name').'">
                                </div>
                                <p><code>&lt;?php graphite_pageSection(&apos;'.$sectionid.'&apos;);?&gt;</code></p>
                            </div>
                        </div>
                        ';}else{echo'
                        <input type="hidden" name="name" value="'.graphite_database_lookupValue('pages_content', 'contentid', $sectionid, 'name').'">
                        <h2>'.graphite_database_lookupValue('pages_content', 'contentid', $sectionid, 'name').'</h2>
                        ';}?>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <script src="/admin/js/tinymce/tinymce.min.js"></script>
                                    <script>
                                        tinymce.init({
                                            selector: '#contentEditor',
                                            height: 400,
                                            menubar: false,
                                            plugins: [
                                                'advlist autolink lists link image charmap print preview anchor',
                                                'searchreplace visualblocks code fullscreen',
                                                'insertdatetime media table contextmenu paste code'
                                            ],
                                            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
                                            // enable title field in the Image dialog
                                            image_title: true, 
                                            // enable automatic uploads of images represented by blob or data URIs
                                            automatic_uploads: true,
                                            // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
                                            images_upload_url: 'postAcceptor.php',
                                            // here we add custom filepicker only to Image dialog
                                            file_picker_types: 'image', 
                                            // and here's our custom image picker
                                            file_picker_callback: function(cb, value, meta) {
                                                var input = document.createElement('input');
                                                input.setAttribute('type', 'file');
                                                input.setAttribute('accept', 'image/*');

                                                // Note: In modern browsers input[type="file"] is functional without 
                                                // even adding it to the DOM, but that might not be the case in some older
                                                // or quirky browsers like IE, so you might want to add it to the DOM
                                                // just in case, and visually hide it. And do not forget do remove it
                                                // once you do not need it anymore.

                                                input.onchange = function() {
                                                    var file = this.files[0];

                                                    // Note: Now we need to register the blob in TinyMCEs image blob
                                                    // registry. In the next release this part hopefully won't be
                                                    // necessary, as we are looking to handle it internally.
                                                    var id = 'blobid' + (new Date()).getTime();
                                                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                                    var blobInfo = blobCache.create(id, file);
                                                    blobCache.add(blobInfo);

                                                    // call the callback and populate the Title field with the file name
                                                    cb(blobInfo.blobUri(), { title: file.name });
                                                };

                                                input.click();
                                            }
                                        });
                                    </script>
                                    <textarea id="contentEditor" class="form-control" name="content"><?php echo graphite_database_lookupValue('pages_content', 'contentid', $sectionid, 'content');?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 text-right">
                                <input type="hidden" name="contentid" value="<?php echo $sectionid;?>">
                                <button id="submit" class="btn btn-primary" type="submit">Save Changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>