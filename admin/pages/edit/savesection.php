<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('pages');

graphite_checkLock($_SESSION['userid']);

$contentid = $_POST['contentid'];
$pageid = graphite_database_lookupValue('pages_content', 'contentid', $contentid, 'pageid');
$name = $_POST['name'];
$content = $_POST['content'];
$content = str_replace("'","&#39;",$content);

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE pages_content
SET name= '$name', content= '$content'
WHERE contentid = '$contentid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/pages/edit/editsection.php?id='.$contentid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/pages/edit/editsection.php?id='.$contentid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>