<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkContentActive('pages');

graphite_checkLock($_SESSION['userid']);

$pageid = graphite_randomString();
$name = $_POST['name'];
$location = $_POST['location'];

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO pages (pageid,name,location)
    VALUES ('$pageid','$name','$location');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Page created.');
    header('Location: /admin/pages/edit/?id='.$pageid);
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/pages/');
}

mysqli_close($conn);
?>