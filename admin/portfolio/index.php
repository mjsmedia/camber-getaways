<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('portfolio');

$pageTitle = 'Portfolio';            //Page Title
$pageSlug = 'portfolio';             //Page Slug

if(empty($_GET['page'])){
    $page = 1;
}else{
    $page = $_GET['page'];
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-folder-open"></i> Portfolio <small class="text-muted hidden-lg-up">All Posts</small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 col-md-9 col-lg-6 hidden-md-down">
                    <h1>All Posts</h1>
                </div>
                <div class="col-12 col-md-3 col-lg-2">
                    <a style="width:100%" class="btn btn-primary" href="new/">New Post <i class="fas fa-plus-square"></i></a>
                </div>
                <div class="col-lg-4 hidden-md-down">
                    <form method="get">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <?php graphite_portfolio_listPosts($_GET['search'],$page);?>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <script>
        function deletePost(postid){
            swal({
  title: "Are you sure?",
  text: "The post will be perminantly deleted.",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    window.location.replace("/admin/portfolio/delete.php?id="+postid);
  }
});
        }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>