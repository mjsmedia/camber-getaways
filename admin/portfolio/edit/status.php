<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('portfolio');

graphite_checkLock($_SESSION['userid']);

$id = $_GET['id'];
if(graphite_database_lookupValue('portfolio', 'portfolioid', $id, 'status') == 1){
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "UPDATE portfolio
SET status= '0'
WHERE portfolioid = '$id';";

    if ($conn->query($sql) === TRUE) {
        graphite_notification('warning', 'Post Unpublished.');
        header('Location: /admin/portfolio/edit/?id='.$id);
    } else {
        graphite_notification('error', 'An error occured while saving changes.');
        header('Location: /admin/portfolio/edit/?id='.$id)
            or die("FATAL ERROR");
    }

    mysqli_close($conn);
}else{
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "UPDATE portfolio
SET status= '1'
WHERE portfolioid = '$id';";

    if ($conn->query($sql) === TRUE) {
        graphite_SWnotification('success', 'Published', 'Your post has been published to the world!');
        header('Location: /admin/portfolio/edit/?id='.$id);
    } else {
        graphite_notification('error', 'An error occured while saving changes.');
        header('Location: /admin/portfolio/edit/?id='.$id)
            or die("FATAL ERROR");
    }

    mysqli_close($conn);
}
?>