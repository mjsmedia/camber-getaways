<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('portfolio');

graphite_checkLock($_SESSION['userid']);

$portfolioid = $_POST['portfolioid'];
$slug = graphite_prepareSlug($_POST['slug'],'portfolio','slug');

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE portfolio
SET slug= '$slug'
WHERE portfolioid = '$portfolioid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/portfolio/edit/?id='.$portfolioid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/portfolio/edit/slug.php?id='.$portfolioid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>