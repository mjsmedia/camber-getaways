<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$userid = $_POST['userid'];

if(empty($_POST['password'])){
    graphite_notification('error', 'Could not change password.');
    header('Location: /admin/account')
        or die("FATAL ERROR");
}else{
    $newPassword = $_POST['password'];
    $newPasswordRe = $_POST['passwordRe'];
    if($newPassword === $newPasswordRe){
        $password = password_hash($newPassword, PASSWORD_DEFAULT);
        $_SESSION['userid'] = '';
    }else{
        graphite_notification('error', 'Passwords do not match.');
        header('Location: /admin/account/')
            or die("FATAL ERROR");
    }
}

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE users
SET password= '$password'
WHERE userid = '$userid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/account');
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/account')
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>