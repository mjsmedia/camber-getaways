<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$userid = $_POST['userid'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE users
SET firstname= '$firstname', lastname= '$lastname'
WHERE userid = '$userid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/account/welcome/password.php');
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/account/welcome')
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>