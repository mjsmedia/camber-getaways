<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Welcome';            //Page Title
$pageSlug = 'account';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-xs-12 col-md-12 text-center">
                    <h1><strong>Almost There!</strong></h1>
                </div>
                <div class="col-xs-12 col-md-12 text-center mt-4 mb-4">
                    <h5>To make sure you can log back in easily, <br>you should set your password to something you'll remember.</h5>
                </div>
            </div>
            <form action="step2.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" autofocus>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Re-enter Password</label>
                        <input type="password" name="passwordRe" class="form-control">
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="form-group">
                        <a class="btn btn-secondary" href="complete.php">Skip</a>
                        <input type="hidden" name="userid" value="<?php echo $_SESSION['userid'];?>">
                        <button id="submit" class="btn btn-primary" type="submit">Next</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>