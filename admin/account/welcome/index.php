<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Welcome';            //Page Title
$pageSlug = 'account';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-xs-12 col-md-12 text-center">
                    <h3>Hello there,</h3>
                    <h1><strong>Welcome to Graphite!</strong></h1>
                    <p>A Content Management System, tailored just for you.</p>
                </div>
                <div class="col-xs-12 col-md-12 text-center mt-4 mb-4">
                    <h5>Lets get to know each other... Whats your name?</h5>
                </div>
            </div>
            <form action="step1.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="firstname" class="form-control" value="<?php echo graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname');?>" autofocus>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="lastname" class="form-control" value="<?php echo graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'lastname');?>">
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="form-group">
                        <a class="btn btn-secondary" href="password.php">Skip</a>
                        <input type="hidden" name="userid" value="<?php echo $_SESSION['userid'];?>">
                        <button id="submit" class="btn btn-primary" type="submit">Next</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>