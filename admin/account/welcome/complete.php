<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Welcome';            //Page Title
$pageSlug = 'account';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-xs-12 col-md-12 text-center">
                    <i class="fas fa-check-circle fa-5x text-success"></i>
                    <h1 class="mt-4"><strong>You're all set!</strong></h1>
                </div>
                <div class="col-xs-12 col-md-12 text-center mt-4 mb-4">
                    <h5 class="mb-4">If you need to change anything in the future, <br>just click on the account dropdown in the menu bar and go to account settings.</h5>
                    <a class="btn btn-primary" href="/admin">Go to Dashboard</a>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>