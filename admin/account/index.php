<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Account Settings';            //Page Title
$pageSlug = 'account';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fa fas fa-user"></i> Account Settings</h4>
                </div>
            </div>

        </div>
        <hr>
        <div class="container">
            <div class="card">
                <div class="card-block">
                    <h5 class="card-title"><i class="fas fa-user-circle"></i> Personal Details</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Your name and email address.</h6>
                    <a href="#" class="card-link" data-toggle="modal" data-target="#personalSettings">Edit <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-block">
                    <h5 class="card-title"><i class="fas fa-lock"></i> Security Settings</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Your password and other security settings.</h6>
                    <p class="card-text"></p>
                    <a href="#" class="card-link" data-toggle="modal" data-target="#securitySettings">Edit <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>

        <div class="modal fade" id="personalSettings" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Personal Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="savepersonal.php" method="post" onsubmit="buttonFeedback('submitPersonal', 'Saving')">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Email Address <span class="badge badge-default">Required</span></label>
                                <input type="email" class="form-control" name="email" required value="<?php echo graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'email');?>">
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="firstname" value="<?php echo graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname');?>">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="lastname" value="<?php echo graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'lastname');?>">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="userid" value="<?php echo $_SESSION['userid'];?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button id="submitPersonal" type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="securitySettings" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Security Settings</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="savesecurity.php" method="post" onsubmit="buttonFeedback('submitSecurity', 'Saving')">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>New Password <span class="badge badge-default">Required</span></label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label>Re-enter New Password <span class="badge badge-default">Required</span></label>
                                <input type="password" class="form-control" name="passwordRe" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="userid" value="<?php echo $_SESSION['userid'];?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button id="submitSecurity" type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>