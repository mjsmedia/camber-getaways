<?php
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$count = 0;
$output = '';
foreach ($_POST['item'] as $value){
    $output .= 'Test '.$value.': '.$count.' | ';
    graphite_caravans_updatePhotoPosition($value,$count);
    $count ++;
}
echo $output;
?>