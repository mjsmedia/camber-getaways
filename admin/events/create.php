<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

$eventid = graphite_randomString();
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];

$name = str_replace("'","&#39;",$_POST['name']);

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO events (eventid,name,startdate,enddate)
    VALUES ('$eventid','$name','$startdate','$enddate');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Event created.');
    header('Location: /admin/events/edit/?id='.$eventid);
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/events/');
}

mysqli_close($conn);
?>