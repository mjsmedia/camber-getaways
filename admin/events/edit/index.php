<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$id = $_GET['id'];

$pageTitle = 'Events';            //Page Title
$pageSlug = 'events';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-calendar-alt"></i> Events <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('events', 'eventid', $id, 'name');?></small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('events', 'eventid', $id, 'name');?></h1>
                </div>
            </div>
        </div>
        <hr>
        <div class="container mb-4">
            <div class="row">
                <div class="col-12 col-md-6">
                    <form action="save.php" method="post">
                        <div class="form-group">
                            <label>Title</label>
                            <input name="name" type="text" class="form-control" value="<?php echo graphite_database_lookupValue('events', 'eventid', $id, 'name');?>">
                        </div>
                        <div class="form-group">
                            <label>Start Date</label>
                            <input id="startDate" name="startdate" type="date" class="form-control" value="<?php echo graphite_database_lookupValue('events', 'eventid', $id, 'startdate');?>" onchange="setDates()">
                        </div>
                        <div class="form-group">
                            <label>End Date</label>
                            <input id="endDate" name="enddate" type="date" class="form-control" value="<?php echo graphite_database_lookupValue('events', 'eventid', $id, 'enddate');?>">
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="eventid" value="<?php echo $id;?>">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                            <a href="/admin/events/delete.php?id=<?php echo $id;?>" class="btn btn-danger">Delete</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <script>
            function setDates(){
                document.getElementById('endDate').min = document.getElementById('startDate').value;
                document.getElementById('startDate').max = document.getElementById('endDate').value;
            }
            setDates();
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>