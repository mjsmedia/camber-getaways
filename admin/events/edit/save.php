<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

$eventid = $_POST['eventid'];
$name = str_replace("'","&#39;",$_POST['name']);
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE events
SET name= '$name',startdate= '$startdate',enddate= '$enddate'
WHERE eventid = '$eventid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/events/edit/?id='.$eventid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/events/edit/?id='.$eventid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>