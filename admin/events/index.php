<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Events';            //Page Title
$pageSlug = 'events';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-calendar-alt"></i> Events <small class="text-muted hidden-lg-up">All Events</small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 col-md-9 col-lg-10 hidden-md-down">
                    <h1>All Events</h1>
                </div>
                <div class="col-12 col-md-3 col-lg-2">
                    <a style="width:100%" class="btn btn-primary" href="#" data-toggle="modal" data-target=".newPage">New Event <i class="fas fa-plus-square"></i></a>
                </div>
            </div>
            
            <div class="modal fade newPage" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="create.php" method="post" onsubmit="buttonFeedback('create', 'Creating')">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Title <small class="text-danger">Required</small></label>
                                            <input type="text" name="name" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Start Date <small class="text-danger">Required</small></label>
                                            <input id="startDate" type="date" name="startdate" class="form-control" required onchange="setDates()">
                                        </div>
                                        <div class="form-group">
                                            <label>End Date <small class="text-danger">Required</small></label>
                                            <input id="endDate" type="date" name="enddate" class="form-control" required disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="create" type="submit" class="btn btn-primary">Create Event</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php graphite_events_list();?>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        
        <script>
            function setDates(){
                document.getElementById('endDate').disabled = false;
                document.getElementById('endDate').min = document.getElementById('startDate').value;
                document.getElementById('startDate').max = document.getElementById('endDate').value;
            }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>