<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Featured Property';            //Page Title
$pageSlug = 'featured';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-bookmark"></i> Homepage Advert</h4>
                </div>
            </div>

        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="save.php" method="post" enctype="multipart/form-data" onsubmit="buttonFeedback('submit', 'Saving')">
						<div class="form-group">
							<label>Banner Artwork</label>
							<?php if(!empty($GENERAL_SETTINGS->get('homepageadvert', 'imageUrl'))){?>
								<img src="<?php echo $GENERAL_SETTINGS->get('homepageadvert', 'imageUrl');?>" alt="">
							<?php } ?>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#addPhoto">Add/Replace Image</button>
							<?php if(!empty($GENERAL_SETTINGS->get('homepageadvert', 'imageUrl'))){?>
							<a href="removeImage.php" class="btn btn-text">Remove Image</a>
						<?php }?>
						</div>
						<div class="form-group">
							<label>Banner Link</label>
							<input type="url" class="form-control" name="bannerLink" value="<?php echo $GENERAL_SETTINGS->get('homepageadvert', 'linkUrl');?>" placeholder="E.g. https://example.com">
						</div>
                        <button id="submit" type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>

		<div class="modal fade" id="addPhoto" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Image</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="addImage.php" method="post"  enctype="multipart/form-data" onsubmit="buttonFeedback('uploadButton', 'Uploading')">
                        <div class="modal-body">
                            <div class="form-group">
								<div class="slim" data-min-size="1110,200" data-force-type="jpg">
									<input type="file" name="slim[]" accept="image/jpeg, image/png">
								</div>
                                <small class="form-text text-muted">File must be .jpg or .png</small>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button id="uploadButton" type="submit" class="btn btn-primary">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>
