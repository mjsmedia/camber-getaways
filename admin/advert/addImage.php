<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

require_once $ROOTLOCATION.'php/Slim/slim.php';
$images = Slim::getImages();

if($images == false){

    $GENERAL_SETTINGS->set('homepageadvert', 'imageUrl', '');

}else{
    foreach ($images as $image) {
        $imageid = graphite_createImage('Advert');
        if($imageid == false){
            die("Image Processing Error");
        }else{
            $imageName = $imageid.'-full.jpg';

            $data = $image['output']['data'];
            Slim::saveFile($data, $imageName, $GLOBALS[ROOTLOCATION].'../images/uploads/', false);

            graphite_optimiseImage($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageName, 1200, 'auto', 50, 'auto', $GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-resized.jpg');

            if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-full.jpg', '/uploads/'.$imageid.'-full.jpg');
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-resized.jpg', '/uploads/'.$imageid.'-resized.jpg');
            }
        }

    }
	$GENERAL_SETTINGS->set('homepageadvert', 'imageUrl', 'https://s3.amazonaws.com/cdn.mjsmedia.co.uk/camber-getaways/uploads/'.$imageid.'-resized.jpg');
}

$GENERAL_SETTINGS->save();
graphite_notification('success', 'Advert Saved.');
header('Location: /admin/advert');


?>
