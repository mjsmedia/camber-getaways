<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

//---
$GRAPHITE_SETTINGS->set('custom', 'featuredPropertyID', $_POST['propertycode']);
$GRAPHITE_SETTINGS->set('custom', 'featuredPropertyTag', $_POST['tag']);
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/featured");
?>