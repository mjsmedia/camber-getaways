<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Featured Property';            //Page Title
$pageSlug = 'featured';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fa fas fa-star"></i> Featured Property</h4>
                </div>
            </div>

        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <form action="save.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
                        <div class="form-group">
                            <label>Property Code <span class="badge badge-default">Required</span></label>
                            <input type="text" class="form-control" name="propertycode" required value="<?php echo $GRAPHITE_SETTINGS->get('custom', 'featuredPropertyID');?>">
                        </div>
                        <div class="form-group">
                            <label>Featured Tag</label>
                            <input type="text" class="form-control" name="tag" value="<?php echo $GRAPHITE_SETTINGS->get('custom', 'featuredPropertyTag');?>">
                        </div>
                        <button id="submit" type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>