<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

//---
graphite_logActivity(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username'), 'Logged Out');

setcookie("userid", '', time()-604800, "/");
session_destroy();
session_start();
graphite_notification('info', 'You have been logged out.');

if($_GET['return'] == 'front'){
    header('Location: /');
}else{
    header('Location: /admin/login/');
}
?>