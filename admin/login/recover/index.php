<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = '';            //Page Title
$pageSlug = '';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <style>
            body{
                background-color: #242424;
            }
        </style>
        <div class="container">
            <div class="row justify-content-center" style="height:100vh">
                <div class="col-xs-12 col-md-4 align-self-center">
                    <div class="card">
                        <div class="card-block">
                            <div class="text-center" style="margin-bottom:20px;">
                                <p>Enter your Email Address below. We will reset your password and send it to you.</p>
                            </div>
                            <form action="resetPassword.php" method="post" onsubmit="buttonFeedback('submit', 'Please Wait')">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email" required autofocus>
                                </div>
                                <div class="text-center">
                                    <button id="submit" class="btn btn-primary" type="submit">Continue</button>
                                    <a class="btn btn-secondary" href="/admin/login">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>