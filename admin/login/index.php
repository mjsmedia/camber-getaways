<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Login';            //Page Title
$pageSlug = '';             //Page Slug

$BgNo = $_GET['bg'];
if(empty($BgNo)){
    $BgNo = rand(1,5);
}
switch($BgNo){
    case 1:
        $bg = 'blue_panels.jpg';
        break;
    case 2:
        $bg = 'marble_texture.jpg';
        break;
    case 3:
        $bg = 'ocean_water.jpg';
        break;
    case 4:
        $bg = 'sand_texture.jpg';
        break;
    case 5:
        $bg = 'shutter_swirls.jpg';
        break;
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <style>
            body{
                background-image: url(/admin/assets/loginbg/<?php echo $bg;?>);
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
                background-attachment: fixed;
            }
        </style>
        <div class="container">
            <div class="row justify-content-center" style="height:100vh">
                <div class="col-12 col-md-6 col-lg-4 align-self-center text-center">
                    <img src="/admin/assets/loginlogo.png" style="width:200px;">
                    <div class="card text-left">
                        <div class="card-block">
                            <form action="login.php" method="post" onsubmit="buttonFeedback('submit', 'Logging In')">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="form-control" type="email" name="email" autofocus value="<?php echo $_GET['e'];?>">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="password" value="<?php echo $_GET['p'];?>">
                                </div>
                                <div class="form-check text-center">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="remember" value="true">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Remember me</span>
                                    </label>
                                </div>
                                <div class="form-group" style="margin:0;">
                                    <button id="submit" class="btn btn-primary" type="submit" style="width:100%">Log In</button>
                                </div>
                                <p style="margin:0;margin-top:10px" class="text-center"><small><a href="recover/">Recover Account</a> &#8226; <a href="https://mjsmedia.freshdesk.com" target="_blank">Support</a></small></p>
                                <p style="margin:0;margin-top:5px; font-size:7pt;" class="text-center text-muted">Copyright &copy; MJS Media <?php echo date("Y");?></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>