<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

//---
$valid = 0;

if(graphite_database_resultCount('users', 'email', $_POST['email']) != 0){
    if(password_verify($_POST['password'], graphite_database_lookupValue('users', 'email', $_POST['email'], 'password'))){
        $valid = 1;
    }else{
        $valid = 0;
    }
}else{
    $valid = 0;
}

if($valid == 1){
    graphite_logActivity($_POST['username'], 'Logged In');
    $_SESSION['userid'] = graphite_database_lookupValue('users', 'email', $_POST['email'], 'userid');
    if($_POST['remember'] == 'true'){
        graphite_notification('info', 'You will be remembered for 4 weeks.');
        setcookie("userid", $_SESSION['userid'], time()+2419200, "/");
    }
    if(empty($_SESSION['requestUrl'])){
        header('Location: /admin');
    }else{
        header('Location: '.$_SESSION['requestUrl']);
    }
}else{
    graphite_notification('error', 'Username or Password is incorrect.');
    header('Location: /admin/login/');
}
?>