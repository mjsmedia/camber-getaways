<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkContentActive('gallery');

graphite_checkLock($_SESSION['userid']);

$albumid = graphite_randomString();
$name = $_POST['name'];

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO albums (albumid,name)
    VALUES ('$albumid','$name');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Album created.');
    header('Location: /admin/gallery/album/?id='.$albumid);
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/gallery/');
}

mysqli_close($conn);
?>