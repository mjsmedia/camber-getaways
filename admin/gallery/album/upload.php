<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('gallery');

graphite_checkLock($_SESSION['userid']);

$albumid = $_POST['albumid'];

if($GRAPHITE_SETTINGS->get('gallery', 'imageType') == 'exact'){
    $imageMode = 'crop';
}else{
    $imageMode = 'auto';
}

$fileCount = count($_FILES['file']['name']);

for($i=0; $i<$fileCount; $i++){
    $imageid = graphite_randomString();
    if(empty($_FILES['file']['name'][$i])){}else{
        $target_dir = $ROOTLOCATION."../images/gallery/";
        $target_file = $target_dir . basename($_FILES["file"]["name"][$i]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["file"]["tmp_name"][$i]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
                graphite_notification('error', 'File invalid.');
                header('Location: /admin/gallery/album/?id='.$albumid)
                    or die('ERROR 3');
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $uploadOk = 0;
            graphite_notification('error', 'Temporary file already exists.');
            header('Location: /admin/gallery/album/?id='.$albumid)
                or die('ERROR 4');
        }
        // Check file size
        if ($_FILES["file"]["size"][$i] > 50000000) {
            $uploadOk = 0;
            graphite_notification('error', 'File too large.');
            header('Location: /admin/gallery/album/?id='.$albumid)
                or die('ERROR 5');
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png" && $imageFileType != "JPG" && $imageFileType != "JPEG" && $imageFileType != "PNG" ) {
            $uploadOk = 0;
            graphite_notification('error', 'Invalid filetype. Image must be .jpg or .png.');
            header('Location: /admin/gallery/album/?id='.$albumid)
                or die('ERROR 6');
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $uploadOk = 0;
            graphite_notification('error', 'An unknown error occured.');
            header('Location: /admin/gallery/album/?id='.$albumid)
                or die('ERROR 7');
        } else {
            if (move_uploaded_file($_FILES["file"]["tmp_name"][$i], $target_file)) {
                $fileToRename = basename( $_FILES["file"]["name"][$i]);

                graphite_gallery_processImages($fileToRename, $imageid);
                
            } else {
                $uploadOk = 0;
                graphite_notification('error', 'Image optimisation failed.');
                header('Location: /admin/gallery/album/?id='.$albumid)
                    or die('ERROR 8');
            }
        }
    }
    
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO albums_images (imageid,albumid)
        VALUES ('$imageid','$albumid');";

    if ($conn->query($sql) === TRUE) {
        graphite_notification('success', 'Image Uploaded');
        header('Location: /admin/gallery/album/?id='.$albumid);
    } else {
        graphite_notification('error', 'An error occured.');
        header('Location: /admin/gallery/album/?id='.$albumid);
    }

    mysqli_close($conn);
}
//--
/*

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO albums_images (imageid,albumid)
    VALUES ('$imageid','$albumid');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Image Uploaded');
    header('Location: /admin/gallery/album/?id='.$albumid);
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/gallery/album/?id='.$albumid);
}

mysqli_close($conn);*/
?>