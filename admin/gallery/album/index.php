<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('gallery');

$id = $_GET['id'];

graphite_item_exists('albums', 'albumid', $id , '/admin/gallery');

$pageTitle = 'Album';            //Page Title
$pageSlug = 'gallery';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-images"></i> Gallery <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('pages', 'pageid', $id, 'name');?></small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('albums', 'albumid', $id, 'name');?></h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/gallery"><i class="fas fa-angle-left"></i> All Albums</a>
                    <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){echo'
                    <a class="btn btn-secondary" href="#" data-toggle="modal" data-target=".pageSettings">Album Settings <i class="fa fa-cog"></i></a>
                    ';}?>
                </div>
            </div>
            <style>
                .albumImage{
                    position: relative;
                    width: 100%;

                }
                .albumImage p{
                    position: absolute;
                    bottom: 0px;
                    width: 100%;
                    background-color: rgba(0, 0, 0, 0.4);
                    color: white;
                    border-bottom-left-radius: .25rem;
                    border-bottom-right-radius: .25rem;
                    padding: 10px;
                    margin-bottom: 0;
                }
            </style>
            <div class="row" style="margin-top:10px">
                <div class="col-6 col-md-3 text-center" style="margin-bottom:30px;">
                    <a data-toggle="modal" data-target="#newPhoto">
                        <div class="albumImage">
                            <img src="/admin/assets/Add-Photo.png" class="rounded" alt="..." style="width:100%">
                            <p><strong>Add Photos</strong></p>
                        </div>
                    </a>
                </div>
                <div class="modal fade" id="newPhoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Upload New Photos</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="upload.php" method="post" enctype="multipart/form-data" onsubmit="buttonFeedback('upload', 'Uploading')">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" name="file[]" required multiple>
                                        <small class="form-text text-muted">Files must be .jpg or .png</small>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="albumid" value="<?php echo $id;?>">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button id="upload" type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php graphite_albums_images($id);?>
            </div>
        </div>
        
        <div class="modal fade pageSettings" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving Changes')">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Name <small class="text-danger">Required</small></label>
                                        <input type="text" class="form-control" name="name" required value="<?php echo graphite_database_lookupValue('albums', 'albumid', $id, 'name');?>">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <p class="mb-0"><code>&lt;?php graphite_gallery('<?php echo $id;?>');?&gt;</code></p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="albumid" value="<?php echo $id;?>">
                            <a href="delete.php?id=<?php echo $id;?>" class="btn btn-danger">Delete Album</a>
                            <button id="save" type="submit" class="btn btn-primary">Save Changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>