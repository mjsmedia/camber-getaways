<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkContentActive('gallery');

graphite_checkLock($_SESSION['userid']);

$albumid = $_POST['albumid'];
$name = $_POST['name'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE albums
SET name= '$name'
WHERE albumid = '$albumid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/gallery/album/?id='.$albumid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/gallery/album/?id='.$albumid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>