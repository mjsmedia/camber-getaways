<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$imageid = $_GET['id'];
$albumid = graphite_database_lookupValue('albums_images', 'imageid', $imageid, 'albumid');

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM albums_images WHERE imageid='$imageid'";

if ($conn->query($sql) === TRUE) {
    unlink($ROOTLOCATION.'../images/gallery/'.$imageid.'.jpg');
    unlink($ROOTLOCATION.'../images/gallery/'.$imageid.'-thumb.jpg');
    unlink($ROOTLOCATION.'../images/gallery/'.$imageid.'-graphitethumb.jpg');
    graphite_notification('warning', 'Image has been deleted.');
    header("Location: /admin/gallery/album/?id=".$albumid);
} else {
    graphite_notification('error', 'Could not delete image.');
    header("Location: /admin/gallery/album/?id=".$albumid);
}

mysqli_close($conn);
?>