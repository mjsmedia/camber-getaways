<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('gallery');

$pageTitle = 'Gallery';            //Page Title
$pageSlug = 'gallery';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-images"></i> Gallery <small class="text-muted hidden-lg-up">All Albums</small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 col-md-9 col-lg-10 hidden-md-down">
                    <h1>All Albums</h1>
                </div>
                <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){echo'
                <div class="col-12 col-md-3 col-lg-2">
                    <a style="width:100%" class="btn btn-primary" href="#" data-toggle="modal" data-target=".newAlbum">New Album <i class="fas fa-plus-square"></i></a>
                </div>
                ';}?>
            </div>
            <style>
                .albumImage{
                    position: relative;
                    width: 100%;

                }
                .albumImage p{
                    position: absolute;
                    bottom: 0px;
                    width: 100%;
                    background-color: rgba(0, 0, 0, 0.4);
                    color: white;
                    border-bottom-left-radius: .25rem;
                    border-bottom-right-radius: .25rem;
                    padding: 10px;
                    margin-bottom: 0;
                }
            </style>
            <div class="row">
                <?php graphite_albums_list();?>
            </div>
        </div>

        <div class="modal fade newAlbum" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="create.php" method="post" onsubmit="buttonFeedback('create', 'Creating')">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Title <small class="text-danger">Required</small></label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="create" type="submit" class="btn btn-primary">Create Album</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>