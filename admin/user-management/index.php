<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(1);

$pageTitle = 'User Management';            //Page Title
$pageSlug = 'user-management';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fa fa-users"></i> User Management <small class="text-muted hidden-lg-up">All Users</small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 col-md-9 col-lg-10 hidden-md-down">
                    <h1>All Users</h1>
                </div>
                <div class="col-12 col-md-3 col-lg-2">
                    <a style="width:100%" class="btn btn-primary" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">New User <i class="fa fa-user-plus"></i></a>
                </div>
            </div>
            
            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form action="create.php" method="post" onsubmit="buttonFeedback('create', 'Creating')">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label>Email <small class="text-danger">Required</small></label>
                                            <input type="email" name="email" class="form-control" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" name="firstname" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" name="lastname" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p>Upon creation, the user will recieve an email with an auto-generated temporary password and instructions about how to log in.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="create" type="submit" class="btn btn-primary">Create User</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php graphite_userManagement_listUsers();?>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <script>
        function deleteUser(userid){
            swal({
  title: "Are you sure?",
  text: "The user will be perminantly deleted.",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    window.location.replace("/admin/user-management/delete.php?id="+userid);
  }
});
        }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>