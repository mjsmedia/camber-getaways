<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(1);

graphite_checkLock($_SESSION['userid']);

//---

$userid = graphite_randomString();
$email = $_POST['email'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];

$password = graphite_randomString();
$passwordHash = password_hash($password, PASSWORD_DEFAULT);

$permissions = 0;

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO users (userid,firstname,lastname,email,password,permissions) VALUES ('$userid','$firstname','$lastname','$email','$passwordHash','$permissions');";

if ($conn->query($sql) === TRUE) {
    graphite_logActivity(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username'), 'Created user: '.$username);
    graphite_notification('success', 'User has been created.');
    header('Location: sendPassword.php?u='.$userid.'&p='.$password);
} else {
    graphite_notification('error', 'User could not be created.');
    header('Location: /admin/user-management');
}

mysqli_close($conn);
?>