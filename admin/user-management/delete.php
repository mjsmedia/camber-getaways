<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(1);

graphite_checkLock($_SESSION['userid']);

//---

$userid = $_GET['id'];

if(graphite_database_lookupValue('users', 'userid', $userid, 'permissions') >= 1){
    graphite_notification('error', 'You are not permitted to delete this user.');
    header("Location: /admin/settings/users") or
    die('You are not permitted to delete this user.');
}

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM users WHERE userid='$userid'";

if ($conn->query($sql) === TRUE) {
    graphite_SWnotification('success', 'Deleted', 'The user has been deleted.');
    header("Location: /admin/user-management/");
} else {
    graphite_SWnotification('error', 'Whoops', 'The user could not be deleted.');
    header("Location: /admin/user-management/");
}

mysqli_close($conn);
?>