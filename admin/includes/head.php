<?php

if(empty($pageTitle)){$pageTitle = '';}else{$pageTitle = $pageTitle.' - ';}

$metaSeoImage = $seoMetaURL.$seoMetaImageURL;
if($seoActive === true){
    $metaTags = '
    <meta name="description" content="'.$seoMetaDescription.'"/>
    <meta name="robots" content="noodp"/>
    <link rel="canonical" href="'.$seoMetaURL.'"/>
    <meta property="og:locale" content="en_GB"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="'.$pageTitle.$siteTitle.'"/>
    <meta property="og:description" content="'.$seoMetaDescription.'"/>
    <meta property="og:url" content="'.$seoMetaURL.'"/>
    <meta property="og:image" content="'.$metaSeoImage.'"/>
    ';
}else{
    $metaTags = '';
}
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $pageTitle.'Graphite - '.$siteTitle;?></title>
    <link rel="stylesheet" href="/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/admin/css/stylesheet.css">
    <link rel="stylesheet" href="/admin/css/toastr.min.css">
    <link rel="stylesheet" href="/admin/css/introjs.css">
    <link rel="stylesheet" href="/admin/css/slim.min.css">
    <link rel="icon" type="image/png" href="/admin/assets/logo-dark.png">
    <?php echo $metaTags;?>
</head>