<nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/admin">
        <img src="/admin/assets/logo-light-wide.png" height="30" class="d-inline-block align-top" alt="GraphiteLogo">
    </a>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if($pageSlug == 'dashboard'){echo'active';}?>"><a class="nav-link" href="/admin">Dashboard</a></li>
            <?php if($GRAPHITE_SETTINGS->get('blog', 'enabled') == 'true'){
            if($pageSlug == 'blog'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/blog/">Blog</a></li>
            ';}?>
            <?php if($GRAPHITE_SETTINGS->get('testimonials', 'enabled') == 'true'){
            if($pageSlug == 'testimonials'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/testimonials/">Testimonials</a></li>
            ';}?>
            <?php if($GRAPHITE_SETTINGS->get('portfolio', 'enabled') == 'true'){
            if($pageSlug == 'portfolio'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/portfolio/">Portfolio</a></li>
            ';}?>
            <?php if($GRAPHITE_SETTINGS->get('gallery', 'enabled') == 'true'){
            if($pageSlug == 'gallery'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/gallery/">Gallery</a></li>
            ';}?>
            <?php if($GRAPHITE_SETTINGS->get('pages', 'enabled') == 'true'){
            if($pageSlug == 'pages'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/pages/">Pages</a></li>
            ';}?>
            <?php if($GRAPHITE_SETTINGS->get('variables', 'enabled') == 'true'){
            if($pageSlug == 'variables'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/variables/">Variables</a></li>
            ';}?>
            <li class="nav-item <?php if($pageSlug == 'caravans'){echo'active';}?>"><a class="nav-link" href="/admin/caravans/">Caravans</a></li>
            <li class="nav-item <?php if($pageSlug == 'events'){echo'active';}?>"><a class="nav-link" href="/admin/events/">Events</a></li>
            <li class="nav-item <?php if($pageSlug == 'featured'){echo'active';}?>"><a class="nav-link" href="/admin/featured/">Featured</a></li>
            <li class="nav-item <?php if($pageSlug == 'date-selection'){echo'active';}?>"><a class="nav-link" href="/admin/date-selection/">Dates</a></li>
            <li class="nav-item <?php if($pageSlug == 'banners'){echo'active';}?>"><a class="nav-link" href="/admin/banners/">Banners</a></li>
            <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){
            if($pageSlug == 'settings'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/settings/">Settings</a></li>
            ';}?>
            <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') == 1){
            if($pageSlug == 'user-management'){$active = 'active';}else{$active = '';}
            echo'
                <li class="nav-item '.$active.'"><a class="nav-link" href="/admin/user-management/">User Management</a></li>
            ';}?>
        </ul>
        <ul class="navbar-nav float-md-right">
            <div class="dropdown-divider"></div>
            <li class="nav-item"><a class="nav-link" href="/"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return to <?php echo $siteTitle;?></a></li>
            <li class="nav-item dropdown">
                <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') < 1){$userBadge = 'fa-shield';}else{$userBadge = 'fa-user';}?>
                <?php
                switch(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions')){
                    case 3:
                        $userBadge = '<i class="fas fa-exclamation-circle"></i><i class="fas fa-shield-alt"></i>';
                        break;
                    case 2:
                        $userBadge = '<i class="fas fa-shield-alt"></i>';
                        break;
                    case 1:
                        $userBadge = '<i class="fa fa-key"></i>';
                        break;
                    case 0:
                        $userBadge = '<i class="fa fa-user"></i>';
                        break;
                }
                ?>
                <?php
                if(empty(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname'))){
                    $userDisplayName = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'email');
                }else{
                    $userDisplayName = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname').' '.graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'lastname');
                }
                ?>
                <a class="nav-link dropdown-toggle <?php if($pageSlug == 'account'){echo'active';}?>" href="" data-toggle="dropdown"><?php echo $userBadge;?> <?php echo $userDisplayName;?></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="/admin/account">Account Settings</a>
                    <a class="dropdown-item" href="/admin/login/logout.php">Log Out</a>
                </div>
            </li>
        </ul>
        <!--
        <div class="dropdown hidden-md-down">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username');?>
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="/admin/settings/account-settings.php">Account Settings</a>
                <a class="dropdown-item" href="/admin/login/logout.php">Log Out</a>
            </div>
        </div>
        <div class="dropdown hidden-md-up">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username');?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="/admin/settings/account-settings.php">Account Settings</a>
                <a class="dropdown-item" href="/admin/login/logout.php">Log Out</a>
            </div>
        </div>
        -->
    </div>
</nav>