<div style="height:80px;width:100%"> </div>
<nav class="navbar fixed-bottom" style="background:rgba(0, 0, 0, 0.1);">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-12 col-lg-2">
                <button id="formBarButton" class="btn btn-primary" type="button" style="width:100%" onclick="formBarSubmit()">Save Changes</button>
            </div>
        </div>
    </div>
</nav>

<script>
function formBarSubmit(){
    document.getElementById('formBarButton').innerHTML = 'Saving <i class="fa fa-pulse fa-spinner"></i>';
    document.getElementById('formBarButton').classList.remove('btn-warning');
    document.getElementById('form').submit();
}
</script>