<?php
if(empty($_SESSION['notificationType'])){}else{
    echo'<script>toastr["'.$_SESSION['notificationType'].'"]("'.$_SESSION['notificationMessage'].'");</script>';
}
?>
<?php
if(empty($_SESSION['SWnotificationType'])){}else{
    echo'<script>swal("'.$_SESSION['SWnotificationTitle'].'", "'.$_SESSION['SWnotificationMessage'].'", "'.$_SESSION['SWnotificationType'].'");</script>';
}
?>
<script>
    function buttonFeedback(id, message){
        document.getElementById(id).innerHTML = message + ' <i class="fa fa-pulse fa-spinner"></i>';
        toastr["info"](message);
        document.getElementById('formBarButton').innerHTML = 'Saving <i class="fa fa-pulse fa-spinner"></i>';
    }
</script>
<script>
    function unsavedChanges(){
        toastr["warning"]('Make sure to save before continuing!','Unsaved Changes');
        document.getElementById('formBarButton').classList.add('btn-warning');
        document.getElementById('formBarButton').classList.remove('btn-primary');
        document.getElementById('formBarButton').innerHTML = 'Save Changes <i class="fa fa-exclamation-circle"></i>';
    }
</script>