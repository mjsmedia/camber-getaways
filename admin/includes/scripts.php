<script src="/admin/js/jquery-3.2.0.min.js"></script>
<script src="/admin/js/tether.min.js"></script>
<script src="/admin/js/bootstrap.min.js"></script>
<script src="/admin/js/toastr.min.js"></script>
<script src="/admin/js/intro.js"></script>
<script src="/admin/js/slim.kickstart.js"></script>
<script src="/admin/js/sweetalert.min.js"></script>
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-bottom-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
<script>
    $(window).scroll(function() {
        sessionStorage.scrollTop = $(this).scrollTop();
    });

    $(document).ready(function() {
        if (sessionStorage.scrollTop != "undefined") {
            $(window).scrollTop(sessionStorage.scrollTop);
        }
    });
</script>