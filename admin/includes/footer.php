<hr class="hidden-lg-up">
<div class="container">
    <div class="row text-center">
        <div class="col-xs-12 col-md-12">
            <p class="text-muted m-0"><small style="font-size:8pt;">Graphite <?php echo $versionGraphite;?> &copy; <a href="https://mjsmedia.co.uk" target="_blank">MJS Media</a> <?php echo date("Y");?>. <br class="hidden-lg-up"> All rights reserved. <a target="_blank" href="https://mjsmedia.freshdesk.com/support/solutions/28000007511">Support <i class="fa fa-life-ring"></i></a></small></p>
        </div>
    </div>
</div>