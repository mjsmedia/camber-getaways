<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Date Selection';            //Page Title
$pageSlug = 'date-selection';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fa fas fa-calendar-check"></i> Date Selection</h4>
                </div>
            </div>

        </div>
        <hr>
        <div class="container">

            <div class="card mb-4">
                <div class="card-block">
                    <h4 class="card-title">Default Days</h4>
                    <form action="save.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <h6 class="card-subtitle mb-2 text-muted">Arrival</h6>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="arrivalMonday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalMonday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Monday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="arrivalTuesday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalTuesday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Tuesday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="arrivalWednesday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalWednesday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Wednesday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="arrivalThursday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalThursday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Thursday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="arrivalFriday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalFriday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Friday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="arrivalSaturday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalSaturday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Saturday</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="arrivalSunday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'arrivalSunday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Sunday</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <h6 class="card-subtitle mb-2 text-muted">Departure</h6>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="departureMonday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'departureMonday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Monday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="departureTuesday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'departureTuesday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Tuesday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="departureWednesday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'departureWednesday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Wednesday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="departureThursday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'departureThursday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Thursday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="departureFriday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'departureFriday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Friday</span>
                                    </label>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="departureSaturday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'departureSaturday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Saturday</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="departureSunday" <?php if($GRAPHITE_SETTINGS->get('dateselection', 'departureSunday') == 'true'){echo'checked';}?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Sunday</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <button id="submit" type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <a href="#" data-toggle="modal" data-target=".newPage">
                <div class="card card-inverse" style="margin-bottom:10px; background-color: #333; border-color: #333;">
                    <div class="card-block text-center">
                        <i class="fas fa-plus-square fa-3x"></i>
                        <h4 class="card-title">New Date Range</h4>
                    </div>
                </div>
            </a>
            <div class="modal fade newPage" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="create.php" method="post" onsubmit="buttonFeedback('create', 'Creating')">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Note</label>
                                            <input type="text" name="note" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Start Date <small class="text-danger">Required</small></label>
                                            <input id="startDate" type="date" name="startdate" class="form-control" required onchange="setDates()">
                                        </div>
                                        <div class="form-group">
                                            <label>End Date <small class="text-danger">Required</small></label>
                                            <input id="endDate" type="date" name="enddate" class="form-control" required disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Arrival Days <small class="text-danger">Required</small></label>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="arrivalMonday" checked>
                                                    Monday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="arrivalTuesday" checked>
                                                    Tuesday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="arrivalWednesday" checked>
                                                    Wednesday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="arrivalThursday" checked>
                                                    Thursday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="arrivalFriday" checked>
                                                    Friday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="arrivalSaturday" checked>
                                                    Saturday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="arrivalSunday" checked>
                                                    Sunday
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Departure Days <small class="text-danger">Required</small></label>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="departureMonday" checked>
                                                    Monday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="departureTuesday" checked>
                                                    Tuesday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="departureWednesday" checked>
                                                    Wednesday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="departureThursday" checked>
                                                    Thursday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="departureFriday" checked>
                                                    Friday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="departureSaturday" checked>
                                                    Saturday
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="departureSunday" checked>
                                                    Sunday
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="create" type="submit" class="btn btn-primary">Create Date Range</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <?php graphite_dateselection_list();?>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>

        <script>
            function setDates(){
                document.getElementById('endDate').disabled = false;
                document.getElementById('endDate').min = document.getElementById('startDate').value;
                document.getElementById('startDate').max = document.getElementById('endDate').value;
            }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>