<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

//---

if($_POST['arrivalMonday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalMonday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalMonday', "");
}
if($_POST['arrivalTuesday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalTuesday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalTuesday', "");
}
if($_POST['arrivalWednesday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalWednesday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalWednesday', "");
}
if($_POST['arrivalThursday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalThursday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalThursday', "");
}
if($_POST['arrivalFriday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalFriday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalFriday', "");
}
if($_POST['arrivalSaturday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalSaturday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalSaturday', "");
}
if($_POST['arrivalSunday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalSunday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'arrivalSunday', "");
}
if($_POST['departureMonday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'departureMonday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'departureMonday', "");
}
if($_POST['departureTuesday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'departureTuesday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'departureTuesday', "");
}
if($_POST['departureWednesday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'departureWednesday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'departureWednesday', "");
}
if($_POST['departureThursday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'departureThursday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'departureThursday', "");
}
if($_POST['departureFriday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'departureFriday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'departureFriday', "");
}
if($_POST['departureSaturday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'departureSaturday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'departureSaturday', "");
}
if($_POST['departureSunday'] == true){
    $GRAPHITE_SETTINGS->set('dateselection', 'departureSunday', "true");
}else{
    $GRAPHITE_SETTINGS->set('dateselection', 'departureSunday', "");
}
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/date-selection");
?>