<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$id = $_GET['id'];

$pageTitle = 'Date Selection';            //Page Title
$pageSlug = 'date-selection';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fa fas fa-calendar-check"></i> Date Selection <small class="text-muted hidden-lg-up">Date Range</small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1>Date Range</h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/date-selection/"><i class="fas fa-angle-left"></i> Go Back</a>
                </div>
            </div>
        </div>
        <hr>
        <div class="container mb-4">
            <div class="row">
                <div class="col-12 col-md-6">
                    <form action="save.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
                        <div class="form-group">
                            <label>Note</label>
                            <input name="note" type="text" class="form-control" value="<?php echo graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'note');?>">
                        </div>
                        <div class="form-group">
                            <label>Start Date</label>
                            <input id="startDate" name="startdate" type="date" class="form-control" value="<?php echo graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'startdate');?>" onchange="setDates()">
                        </div>
                        <div class="form-group">
                            <label>End Date</label>
                            <input id="endDate" name="enddate" type="date" class="form-control" value="<?php echo graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'enddate');?>">
                        </div>
                        <div class="form-group">
                            <label>Arrival Days <small class="text-danger">Required</small></label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="arrivalMonday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'arrivalMonday') == 1){echo'checked';}?>>
                                    Monday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="arrivalTuesday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'arrivalTuesday') == 1){echo'checked';}?>>
                                    Tuesday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="arrivalWednesday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'arrivalWednesday') == 1){echo'checked';}?>>
                                    Wednesday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="arrivalThursday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'arrivalThursday') == 1){echo'checked';}?>>
                                    Thursday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="arrivalFriday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'arrivalFriday') == 1){echo'checked';}?>>
                                    Friday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="arrivalSaturday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'arrivalSaturday') == 1){echo'checked';}?>>
                                    Saturday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="arrivalSunday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'arrivalSunday') == 1){echo'checked';}?>>
                                    Sunday
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Departure Days <small class="text-danger">Required</small></label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="departureMonday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'departureMonday') == 1){echo'checked';}?>>
                                    Monday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="departureTuesday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'departureTuesday') == 1){echo'checked';}?>>
                                    Tuesday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="departureWednesday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'departureWednesday') == 1){echo'checked';}?>>
                                    Wednesday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="departureThursday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'departureThursday') == 1){echo'checked';}?>>
                                    Thursday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="departureFriday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'departureFriday') == 1){echo'checked';}?>>
                                    Friday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="departureSaturday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'departureSaturday') == 1){echo'checked';}?>>
                                    Saturday
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="departureSunday" <?php if(graphite_database_lookupValue('dateselector_ranges', 'id', $id, 'departureSunday') == 1){echo'checked';}?>>
                                    Sunday
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                            <a href="/admin/date-selection/delete.php?id=<?php echo $id;?>" class="btn btn-danger">Delete</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <script>
            function setDates(){
                document.getElementById('endDate').min = document.getElementById('startDate').value;
                document.getElementById('startDate').max = document.getElementById('endDate').value;
            }
            setDates();
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>