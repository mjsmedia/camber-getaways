<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

$id = $_POST['id'];
$note = $_POST['note'];
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];

if($_POST['arrivalMonday'] == true){
    $arrivalMonday = 1;
}else{
    $arrivalMonday = 0;
}
if($_POST['arrivalTuesday'] == true){
    $arrivalTuesday = 1;
}else{
    $arrivalTuesday = 0;
}
if($_POST['arrivalWednesday'] == true){
    $arrivalWednesday = 1;
}else{
    $arrivalWednesday = 0;
}
if($_POST['arrivalThursday'] == true){
    $arrivalThursday = 1;
}else{
    $arrivalThursday = 0;
}
if($_POST['arrivalFriday'] == true){
    $arrivalFriday = 1;
}else{
    $arrivalFriday = 0;
}
if($_POST['arrivalSaturday'] == true){
    $arrivalSaturday = 1;
}else{
    $arrivalSaturday = 0;
}
if($_POST['arrivalSunday'] == true){
    $arrivalSunday = 1;
}else{
    $arrivalSunday = 0;
}

if($_POST['departureMonday'] == true){
    $departureMonday = 1;
}else{
    $departureMonday = 0;
}
if($_POST['departureTuesday'] == true){
    $departureTuesday = 1;
}else{
    $departureTuesday = 0;
}
if($_POST['departureWednesday'] == true){
    $departureWednesday = 1;
}else{
    $departureWednesday = 0;
}
if($_POST['departureThursday'] == true){
    $departureThursday = 1;
}else{
    $departureThursday = 0;
}
if($_POST['departureFriday'] == true){
    $departureFriday = 1;
}else{
    $departureFriday = 0;
}
if($_POST['departureSaturday'] == true){
    $departureSaturday = 1;
}else{
    $departureSaturday = 0;
}
if($_POST['departureSunday'] == true){
    $departureSunday = 1;
}else{
    $departureSunday = 0;
}

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE dateselector_ranges
SET note= '$note',startdate= '$startdate',enddate= '$enddate',arrivalMonday='$arrivalMonday',arrivalTuesday='$arrivalTuesday',arrivalWednesday='$arrivalWednesday',arrivalThursday='$arrivalThursday',arrivalFriday='$arrivalFriday',arrivalSaturday='$arrivalSaturday',arrivalSunday='$arrivalSunday',departureMonday='$departureMonday',departureTuesday='$departureTuesday',departureWednesday='$departureWednesday',departureThursday='$departureThursday',departureFriday='$departureFriday',departureSaturday='$departureSaturday',departureSunday='$departureSunday'
WHERE id = '$id';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/date-selection/edit/?id='.$id);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/date-selection/edit/?id='.$id)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>