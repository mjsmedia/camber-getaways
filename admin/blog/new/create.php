<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('blog');

graphite_checkLock($_SESSION['userid']);

$blogid = graphite_randomString();
$title = $_POST['title'];
$slug = graphite_prepareSlug($title,'blog','slug');
$publishDate = $_POST['publishDate'];
$author = $_POST['author'];
$category = $_POST['category'];
$metaDescription = $_POST['metaDescription'];
$metaImageUrl = $_POST['metaImageUrl'];
$custom1 = $_POST['custom1'];
$custom2 = $_POST['custom2'];
$content = str_replace("'","&#39;",$_POST['content']);
$summary = str_replace("'","&#39;",$_POST['summary']);


require_once $ROOTLOCATION.'php/Slim/slim.php';
$images = Slim::getImages();

if($images == false){

    $imageid = '';

}else{
    foreach ($images as $image) {
        $imageid = graphite_createImage('blog');
        if($imageid == false){
            die("Image Processing Error");
        }else{
            $imageName = $imageid.'-full.jpg';
            
            $data = $image['output']['data'];
            Slim::saveFile($data, $imageName, $GLOBALS[ROOTLOCATION].'../images/uploads/', false);
            
            graphite_optimiseImage($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageName, 500, 500, 50, 'crop', $GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-thumbnail.jpg');
            graphite_optimiseImage($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageName, 1200, 630, 50, 'crop', $GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-seo.jpg');
            
            if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-full.jpg', '/uploads/'.$imageid.'-full.jpg');
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-thumbnail.jpg', '/uploads/'.$imageid.'-thumbnail.jpg');
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-seo.jpg', '/uploads/'.$imageid.'-seo.jpg');
            }
        }
        
    }
}




// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO blog (blogid,title,slug,publishDate,author,metaDescription,metaImageUrl,customField1,customField2,content,summary,category,imageid)
    VALUES ('$blogid','$title','$slug','$publishDate','$author','$metaDescription','$metaImageUrl','$custom1','$custom2','$content','$summary','$category','$imageid');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Post Created.');
    header('Location: /admin/blog/edit/?id='.$blogid);
} else {
    graphite_notification('error', 'An error occured. '.$conn->error);
    header('Location: /admin/blog/');
}

mysqli_close($conn);
?>