<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('blog');

$pageTitle = 'Blog';            //Page Title
$pageSlug = 'blog';             //Page Slug

$firstname = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname');
$lastname = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'lastname');
if(empty($firstname)){
    $author = '';
}else{
    if(empty($lastname)){
        $author = $firstname;
    }else{
        $author = $firstname.' '.$lastname;
    }
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-newspaper"></i> Blog <small class="text-muted hidden-lg-up">New Post</small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1>New Post</h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/blog"><i class="fas fa-angle-left"></i> All Posts</a>
                </div>
            </div>
            <div class="alert alert-warning alert-dismissible fade show hidden-md-up" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Heads Up!</strong> You are using a basic version of the post editor, for all features use a desktop computer.
            </div>
        </div>
        <hr>
        <form action="create.php" method="post" enctype="multipart/form-data" onsubmit="buttonFeedback('submit', 'Creating Post')">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-9">
                        <div class="form-group">
                            <input type="text" class="form-control graphiteTitleField" name="title" placeholder="<?php echo graphite_randomPlaceholderTitle();?>" required autofocus>
                        </div>
                        <div class="form-group">
                            <script src="/admin/js/tinymce/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: '#contentEditor',
                                    themes: "modern",
                                    height: 400,
                                    menubar: false,
                                    plugins: [
                                        'advlist autolink lists link image charmap print preview anchor',
                                        'searchreplace visualblocks code fullscreen',
                                        'insertdatetime media table contextmenu paste code'
                                    ],
                                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
                                    // enable title field in the Image dialog
                                    image_title: true, 
                                    // enable automatic uploads of images represented by blob or data URIs
                                    automatic_uploads: true,
                                    // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
                                    images_upload_url: 'postAcceptor.php',
                                    // here we add custom filepicker only to Image dialog
                                    file_picker_types: 'image', 
                                    // and here's our custom image picker
                                    file_picker_callback: function(cb, value, meta) {
                                        var input = document.createElement('input');
                                        input.setAttribute('type', 'file');
                                        input.setAttribute('accept', 'image/*');

                                        // Note: In modern browsers input[type="file"] is functional without 
                                        // even adding it to the DOM, but that might not be the case in some older
                                        // or quirky browsers like IE, so you might want to add it to the DOM
                                        // just in case, and visually hide it. And do not forget do remove it
                                        // once you do not need it anymore.

                                        input.onchange = function() {
                                            var file = this.files[0];

                                            // Note: Now we need to register the blob in TinyMCEs image blob
                                            // registry. In the next release this part hopefully won't be
                                            // necessary, as we are looking to handle it internally.
                                            var id = 'blobid' + (new Date()).getTime();
                                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                            var blobInfo = blobCache.create(id, file);
                                            blobCache.add(blobInfo);

                                            // call the callback and populate the Title field with the file name
                                            cb(blobInfo.blobUri(), { title: file.name });
                                        };

                                        input.click();
                                    }
                                });
                            </script>
                            <textarea id="contentEditor" class="form-control" name="content"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Post Summary</label>
                            <textarea class="form-control" name="summary" rows="5"></textarea>
                        </div>
                        <?php if($GRAPHITE_SETTINGS->get('blog', 'metaDetails') == 'true'){echo'
                        <div class="card card-block mb-4 hidden-sm-down">
                            <h6 class="card-title">Meta Details</h6>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <textarea id="field" class="form-control" name="metaDescription" onkeyup="countChar(this)" max-length="160"></textarea>
                                        <small id="charNum" class="form-text text-muted">160</small>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Overwrite Meta Image URL</label>
                                        <input type="text" class="form-control" name="metaImageUrl" placeholder="E.g. https://i.imgur.com/....jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';}?>
                    </div>
                    <div class="col-12 col-md-12 col-lg-3">
                        <div class="card mb-4">
                            <div class="card-block">
                                <div class="row">
                                    <?php
                                    if($GRAPHITE_SETTINGS->get('blog', 'author') == 'true'){echo'
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>Author</label>
                                            <input type="text" class="form-control" name="author" value="'.$author.'" placeholder="Joe Bloggs" required>
                                        </div>
                                    </div>
                                    ';}?>
                                    <?php
                                    if($GRAPHITE_SETTINGS->get('blog', 'featuredImage') == 'true'){echo'
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>Featured Image</label>
                                            <div class="slim" data-force-size="1920,1080" data-force-type="jpg">
                                                    <input type="file" name="slim[]" accept="image/jpeg, image/png">
                                                </div>
                                            <small class="form-text text-muted">File must be .jpg or .png</small>
                                        </div>
                                    </div>
                                    ';}?>
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>Publish Date</label>
                                            <input type="date" class="form-control" name="publishDate" value="<?php echo date("Y-m-d");?>" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" name="category">
                                                <option value="">Uncategorised</option>
                                                <?php graphite_blog_categoryDropdown('');?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button id="submit" type="submit" class="btn btn-primary" style="width:100%">Create Post</button>
                            </div>
                        </div>
                        <?php if($GRAPHITE_SETTINGS->get('blog', 'customFields') == 'true'){echo'
                        <div class="card mb-4 hidden-sm-down">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>'.$GRAPHITE_SETTINGS->get('portfolio', 'customField1Name').'</label>
                                            <input type="text" class="form-control" name="custom1">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>'.$GRAPHITE_SETTINGS->get('portfolio', 'customField2Name').'</label>
                                            <input type="text" class="form-control" name="custom2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';}?>
                    </div>
                </div>
            </div>
        </form>
        
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <script>
            function countChar(val) {
                var len = val.value.length;
                if (len >= 160) {
                    val.value = val.value.substring(0, 160);
                } else {
                    $('#charNum').text(160 - len);
                }
            };
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>