<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('blog');

graphite_checkLock($_SESSION['userid']);

$blogid = $_POST['blogid'];
$slug = graphite_prepareSlug($_POST['slug'],'blog','slug');

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE blog
SET slug= '$slug'
WHERE blogid = '$blogid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/blog/edit/?id='.$blogid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/blog/edit/slug.php?id='.$blogid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>