<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('blog');

$id = graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'id');

graphite_item_exists('blog', 'blogid', $_GET['id'] , '/admin/blog');

$pageTitle = 'Blog';            //Page Title
$pageSlug = 'blog';             //Page Slug

$firstname = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname');
$lastname = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'lastname');
if(empty($firstname)){
    $author = '';
}else{
    if(empty($lastname)){
        $author = $firstname;
    }else{
        $author = $firstname.' '.$lastname;
    }
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-newspaper"></i> Blog <small class="text-muted hidden-lg-up">Edit Post</small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1>Edit Post</h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/blog"><i class="fas fa-angle-left"></i> All Posts</a>
                    <a class="btn btn-secondary" href="/admin/blog/new/">New Post <i class="fas fa-plus-square"></i></a>
                </div>
            </div>
            <div class="alert alert-warning alert-dismissible fade show hidden-md-up" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Heads Up!</strong> You are using a basic version of the post editor, for all features use a desktop computer.
            </div>
        </div>
        <hr>
        <form action="save.php" method="post" enctype="multipart/form-data" onsubmit="buttonFeedback('submit', 'Saving Changes')">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-9">
                        <?php if(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'status') != 1){echo'
                        <div class="alert alert-warning" role="alert">
                            <strong>Heads up!</strong> This post is unpublished and wont be visible to visitors. <a href="status.php?id='.$_GET['id'].'" class="alert-link">Publish this Post</a>.
                        </div>
                        ';}else if(strtotime(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'publishDate')) > strtotime('now')){echo'
                        <div class="alert alert-info" role="alert">
                            <strong>Heads up!</strong> This post is scheduled to be published on '.date("d M Y", strtotime(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'publishDate'))).' and will not be visible to visitors until then.
                        </div>
                        ';}?>
                        <?php if(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'status') != 1){
                            $statusBadge = '<span class="badge badge-warning">Unpublished</span>';
                        }else if(strtotime(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'publishDate')) > strtotime('now')){
                            $statusBadge = '<span class="badge badge-info">Scheduled</span>';
                        }else{
                            $statusBadge = '<span class="badge badge-success">Published</span>';
                        }?>
                        <?php $postUrl = $currentUrl.$GRAPHITE_SETTINGS->get('blog', 'archiveLocation').graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'slug');?>
                        <p class="hidden-sm-down"><?php echo $statusBadge;?> Post URL: <em><a target="_blank" href="<?php echo $GRAPHITE_SETTINGS->get('blog', 'archiveLocation').graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'slug');?>"><?php echo $postUrl;?></a></em> <a class="btn btn-secondary btn-sm" href="#" data-toggle="modal" data-target="#editSlug">Edit</a></p>
                        <div class="form-group">
                            <input type="text" class="form-control graphiteTitleField" name="title" placeholder="<?php echo graphite_randomPlaceholderTitle();?>" required value="<?php echo graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'title');?>">
                        </div>
                        <div class="form-group">
                            <script src="/admin/js/tinymce/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: '#contentEditor',
                                    themes: "modern",
                                    height: 400,
                                    menubar: false,
                                    plugins: [
                                        'advlist autolink lists link image charmap print preview anchor',
                                        'searchreplace visualblocks code fullscreen',
                                        'insertdatetime media table contextmenu paste code'
                                    ],
                                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
                                    // enable title field in the Image dialog
                                    image_title: true,
                                    // enable automatic uploads of images represented by blob or data URIs
                                    automatic_uploads: true,
                                    // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
                                    images_upload_url: 'postAcceptor.php',
                                    // here we add custom filepicker only to Image dialog
                                    file_picker_types: 'image',
                                    // and here's our custom image picker
                                    file_picker_callback: function(cb, value, meta) {
                                        var input = document.createElement('input');
                                        input.setAttribute('type', 'file');
                                        input.setAttribute('accept', 'image/*');

                                        // Note: In modern browsers input[type="file"] is functional without
                                        // even adding it to the DOM, but that might not be the case in some older
                                        // or quirky browsers like IE, so you might want to add it to the DOM
                                        // just in case, and visually hide it. And do not forget do remove it
                                        // once you do not need it anymore.

                                        input.onchange = function() {
                                            var file = this.files[0];

                                            // Note: Now we need to register the blob in TinyMCEs image blob
                                            // registry. In the next release this part hopefully won't be
                                            // necessary, as we are looking to handle it internally.
                                            var id = 'blobid' + (new Date()).getTime();
                                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                            var blobInfo = blobCache.create(id, file);
                                            blobCache.add(blobInfo);

                                            // call the callback and populate the Title field with the file name
                                            cb(blobInfo.blobUri(), { title: file.name });
                                        };

                                        input.click();
                                    }
                                });
                            </script>
                            <textarea id="contentEditor" class="form-control" name="content"><?php echo graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'content');?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Post Summary</label>
                            <textarea class="form-control" name="summary" rows="5"><?php echo graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'summary');?></textarea>
                        </div>
                        <?php if($GRAPHITE_SETTINGS->get('blog', 'metaDetails') == 'true'){echo'
                        <div class="card card-block mb-4 hidden-sm-down">
                            <h6 class="card-title">Meta Details</h6>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <textarea id="field" class="form-control" name="metaDescription" onkeyup="countChar(this)" max-length="160">'.graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'metaDescription').'</textarea>
                                        <small id="charNum" class="form-text text-muted">160</small>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Overwrite Meta Image URL</label>
                                        <input type="text" class="form-control" name="metaImageUrl" placeholder="E.g. https://i.imgur.com/....jpg" value="'.graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'metaImageUrl').'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';}?>
                    </div>
                    <div class="col-12 col-md-12 col-lg-3">
                        <div class="card mb-4">
                            <div class="card-block">
                                <div class="row">
                                    <?php
                                    if($GRAPHITE_SETTINGS->get('blog', 'author') == 'true'){echo'
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>Author</label>
                                            <input type="text" class="form-control" name="author" value="'.graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'author').'" placeholder="Joe Bloggs" required>
                                        </div>
                                    </div>
                                    ';}?>

                                    <?php
                                    if($GRAPHITE_SETTINGS->get('blog', 'featuredImage') == 'true'){
                                        if(!empty(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'imageid'))){
                                            $existingImage='<img src="https://s3.amazonaws.com/cdn.mjsmedia.co.uk/camber-getaways/uploads/'.graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'imageid').'-full.jpg'.'" alt=""/>';
                                        }else{
                                            $existingImage = '';
                                        }
                                        echo'
                                        <div class="col-12 col-md-4 col-lg-12">
                                            <div class="form-group">
                                                <label>Featured Image</label>
                                                <div class="slim" data-force-size="1920,1080" data-force-type="jpg">
                                                    '.$existingImage.'
                                                    <input type="file" name="slim[]" accept="image/jpeg, image/png">
                                                </div>
                                                <small class="form-text text-muted">File must be .jpg or .png</small>
                                            </div>
                                        </div>
                                    ';}?>
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>Publish Date</label>
                                            <input type="date" class="form-control" name="publishDate" value="<?php echo date("Y-m-d", strtotime(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'publishDate')));?>" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" name="category">
                                                <option value="">Uncategorised</option>
                                                <?php graphite_blog_categoryDropdown(graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'category'));?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="blogid" value="<?php echo $_GET['id'];?>">
                                <button id="submit" type="submit" class="btn btn-primary" style="width:100%">Save Changes</button>
                                <?php if(graphite_database_lookupValue('blog', 'id', $id, 'status') != 1){echo'
                                <a class="btn btn-success mt-2" href="status.php?id='.$_GET['id'].'" style="width:100%">Publish Post</a>
                                ';}else{echo'
                                <a class="btn btn-warning mt-2" href="status.php?id='.$_GET['id'].'" style="width:100%">Unpublish Post</a>
                                ';}?>
                                <a href="#" class="btn btn-danger mt-2" style="width:100%" onclick="deletePost('<?php echo $_GET['id'];?>')">Delete Post</a>
                            </div>
                        </div>
                        <?php if($GRAPHITE_SETTINGS->get('blog', 'customFields') == 'true'){echo'
                        <div class="card mb-4 hidden-sm-down">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>'.$GRAPHITE_SETTINGS->get('portfolio', 'customField1Name').'</label>
                                            <input type="text" class="form-control" name="custom1" value="'.graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'customField1').'">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-12">
                                        <div class="form-group">
                                            <label>'.$GRAPHITE_SETTINGS->get('portfolio', 'customField2Name').'</label>
                                            <input type="text" class="form-control" name="custom2" value="'.graphite_database_lookupValue('blog', 'blogid', $_GET['id'], 'customField2').'">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';}?>
                    </div>
                </div>
            </div>
        </form>
        <div class="modal fade" id="editSlug" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Post URL</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="saveslug.php" method="post" onsubmit="buttonFeedback('saveslug', 'Saving Changes')">
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <strong>Heads up!</strong> The post slug is part of the URL for this specific post, this means it must be unique. If a post already exists with the chosen slug, an itterative number will be ammended to the end.
                            </div>
                            <?php
                            $buttonType = 'primary';
                            if(graphite_database_lookupValue('blog', 'id', $id, 'status') == 1){echo'
                            <div class="alert alert-danger" role="alert">
                                <strong>Warning!</strong> This post is already published so it is not recomended to change the slug. This can massively affect SEO scores and damage existing links to the post.
                            </div>
                            ';
                            $buttonType = 'danger';}?>
                            <?php $postUrl = $currentUrl.$GRAPHITE_SETTINGS->get('blog', 'archiveLocation');?>

                            <div class="form-group">
                                <label><?php echo $postUrl;?>...</label>
                                <input type="text" class="form-control" name="slug" value="<?php echo graphite_database_lookupValue('blog', 'id', $id, 'slug');?>" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="blogid" value="<?php echo $_GET['id'];?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button id="saveslug" type="submit" class="btn btn-<?php echo $buttonType;?>">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <script>
            function countChar(val) {
                var len = val.value.length;
                if (len >= 160) {
                    val.value = val.value.substring(0, 160);
                } else {
                    $('#charNum').text(160 - len);
                }
            };
        </script>
        <script>
            function deletePost(postid){
                swal({
                    title: "Are you sure?",
                    text: "The post will be perminantly deleted.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location.replace('/admin/blog/delete.php?id='+postid);
                    }
                });
            }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>
