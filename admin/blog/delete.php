<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM blog WHERE blogid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_logActivity(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username'), 'Deleted Blog post: '.$id);
    graphite_SWnotification('success', 'Deleted', 'The post has been deleted.');
    header("Location: /admin/blog");
} else {
    graphite_SWnotification('error', 'Whoops!', 'The post could not be deleted.');
    header("Location: /admin/blog");
}

mysqli_close($conn);
?>