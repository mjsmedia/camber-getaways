<?php
$ROOTLOCATION = '../../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$userid = $_POST['userid'];
$email = $_POST['email'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$permissions = $_POST['permissions'];

if(graphite_database_lookupValue('users', 'userid', $userid, 'permissions') >= 3){
    graphite_notification('error', 'You are not permitted to edit this user.');
    header("Location: /admin/settings/users") or
    die('This user cannot be deleted.');
}

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE users
SET email= '$email', firstname= '$firstname', lastname= '$lastname', permissions= '$permissions'
WHERE userid = '$userid';";

if ($conn->query($sql) === TRUE) {
    graphite_logActivity(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username'), 'Edited user: '.$username);
    graphite_notification('success', 'Changes saved.');
    header("Location: /admin/settings/users/edit?id=".$userid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header("Location: /admin/settings/users/edit?id=".$userid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>