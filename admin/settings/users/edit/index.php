<?php //PAGE CONFIG
$ROOTLOCATION = '../../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$pageTitle = 'Settings';            //Page Title
$pageSlug = 'settings';             //Page Slug
$settingsSlug = 'users';
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12 col-md-3">
                    <?php include_once $ROOTLOCATION.'includes/settingsnav.php';?>
                </div>
                <div class="col-xs-12 col-md-9">
                    <h2><i class="fa fa-users"></i> Users</h2>
                    <a class="btn btn-secondary" href="/admin/settings/users/"><i class="fas fa-angle-left"></i> All Users</a>
                    <hr>
                    <form action="edit.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>Email <small class="text-muted">Required</small></label>
                                    <input type="email" name="email" class="form-control" required autofocus value="<?php echo graphite_database_lookupValue('users', 'userid', $_GET['id'], 'email');?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>Account Type</label>
                                    <select class="form-control" name="permissions">
                                        <option <?php if(graphite_database_lookupValue('users', 'userid', $_GET['id'], 'permissions') == 0){echo'selected';}?> value="0">User</option>
                                        <option <?php if(graphite_database_lookupValue('users', 'userid', $_GET['id'], 'permissions') == 1){echo'selected';}?> value="1">Manager</option>
                                        <option <?php if(graphite_database_lookupValue('users', 'userid', $_GET['id'], 'permissions') == 2){echo'selected';}?> value="2">Administrator</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="firstname" class="form-control" value="<?php echo graphite_database_lookupValue('users', 'userid', $_GET['id'], 'firstname');?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="lastname" class="form-control" value="<?php echo graphite_database_lookupValue('users', 'userid', $_GET['id'], 'lastname');?>">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 text-right">
                                <div class="form-group">
                                    <input type="hidden" name="userid" value="<?php echo $_GET['id'];?>">
                                    <a class="btn btn-warning" href="resetPassword.php?id=<?php echo $_GET['id'];?>">Reset Password</a>
                                    <?php if(graphite_database_lookupValue('users', 'userid', $_GET['id'], 'permissions') <= 2){echo '<a class="btn btn-danger" href="delete.php?id='.$_GET['id'].'">Delete</a>';}?>
                                    <button id="submit" class="btn btn-primary" type="submit">Save Changes</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>