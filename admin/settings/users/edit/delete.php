<?php
$ROOTLOCATION = '../../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$userid = $_GET['id'];

if(graphite_database_lookupValue('users', 'userid', $userid, 'permissions') >= 3){
    graphite_notification('error', 'You are not permitted to delete this user.');
    header("Location: /admin/settings/users") or
    die('This user cannot be deleted.');
}

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM users WHERE userid='$userid'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'User has been deleted.');
    header("Location: /admin/settings/users");
} else {
    graphite_notification('error', 'Could not delete user.');
    header("Location: /admin/settings/users");
}

mysqli_close($conn);
?>