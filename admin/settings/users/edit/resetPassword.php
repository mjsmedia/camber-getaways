<?php
$ROOTLOCATION = '../../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';
require_once $ROOTLOCATION.'php/PHPMailer/PHPMailerAutoload.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$userid = $_GET['id'];
$email = graphite_database_lookupValue('users', 'userid', $userid, 'email');

if(graphite_database_lookupValue('users', 'userid', $userid, 'permissions') >= 3){
    graphite_notification('error', 'You are not permitted to edit this user.');
    header("Location: /admin/settings/users") or
    die('This user cannot be deleted.');
}

$password = graphite_randomString();
$passwordHash = password_hash($password, PASSWORD_DEFAULT);

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE users
SET password= '$passwordHash', setup= 0
WHERE userid = '$userid';";

if ($conn->query($sql) === TRUE) {

    $mail = new PHPMailer;

    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();
    $mail->Host = $GENERAL_SETTINGS->get('smtp', 'host');
    $mail->SMTPAuth = $GENERAL_SETTINGS->get('smtp', 'SMTPAuth');
    $mail->Username = $GENERAL_SETTINGS->get('smtp', 'username');
    $mail->Password = $GENERAL_SETTINGS->get('smtp', 'password');
    $mail->SMTPSecure = $GENERAL_SETTINGS->get('smtp', 'SMTPSecure');
    $mail->Port = $GENERAL_SETTINGS->get('smtp', 'port');

    $mail->setFrom('graphite@mjsmedia.co.uk', 'MJS Media Graphite');
    $mail->addAddress($email);          // Name is optional

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Your Graphite Account';
    $mail->Body    = '
    <h1>Heads Up!</h1> <h3>Your password has been reset</h3>
    You requested to have your password reset on Graphite. If this was not you, please contact MJS Media Support immediately (support@mjsmedia.co.uk).
    <br>To log in, go to <a href="http://'.$currentUrl.'/admin"><em>'.$currentUrl.'/admin</em></a>.
    <br>Your Password is: '.$password.'<br><strong>Remember to change this in your Account Settings once you have logged in.</strong>
    <hr>
    <small>You are receiving this email because your password has been reset on Graphite.</small>
    ';

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        graphite_notification('success', 'Password Reset.');
        header("Location: /admin/settings/users/edit?id=".$userid);
    }
} else {
    graphite_notification('error', 'An error occured while reseting password.');
    header("Location: /admin/settings/users/edit?id=".$userid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>