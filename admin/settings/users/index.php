<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$pageTitle = 'Settings';            //Page Title
$pageSlug = 'settings';             //Page Slug
$settingsSlug = 'users';
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12 col-md-3">
                    <?php include_once $ROOTLOCATION.'includes/settingsnav.php';?>
                </div>
                <div class="col-12 col-md-9">
                    <h2><i class="fa fa-users"></i> Users</h2>
                    <a class="btn btn-primary mb-2" href="#" data-toggle="modal" data-target="#addusers">Add Users <i class="fas fa-plus-square"></i></a>

                    <?php graphite_settings_listUsers();?>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addusers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Users</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="create.php" method="post" onsubmit="buttonFeedback('submit', 'Creating')">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Email Address <small class="text-danger">Required</small></label>
                                        <input type="email" name="email" class="form-control" required autofocus>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Account Type</label>
                                        <select class="form-control" name="permissions">
                                            <option value="0">User</option>
                                            <option value="1">Manager</option>
                                            <option value="2">Administrator</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button id="submit" type="submit" class="btn btn-primary">Add Users</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>