<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$userid = graphite_randomString();
$email = $_POST['email'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];

$password = graphite_randomString();
$passwordHash = password_hash($password, PASSWORD_DEFAULT);

$permissions = $_POST['permissions'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO users (userid,firstname,lastname,email,password,permissions) VALUES ('$userid','$firstname','$lastname','$email','$passwordHash','$permissions');";

if ($conn->query($sql) === TRUE) {
    graphite_logActivity(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username'), 'Created user: '.$username);
    graphite_notification('success', 'User has been created.');
    header('Location: sendPassword.php?u='.$userid.'&p='.$password);
} else {
    graphite_notification('error', 'User could not be created.');
    header('Location: /admin/settings/users');
}

mysqli_close($conn);
?>