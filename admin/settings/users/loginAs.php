<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';
require_once $ROOTLOCATION.'php/PHPMailer/PHPMailerAutoload.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$newuserid = $_GET['id'];

setcookie("userid", '', time()-604800, "/");
$_SESSION['userid'] = $newuserid;

graphite_SWnotification('info', 'User Changed', 'You have been logged in as a different user.');

header('Location: /admin');

?>