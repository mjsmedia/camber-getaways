<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';
require_once $ROOTLOCATION.'php/PHPMailer/PHPMailerAutoload.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$userid = $_GET['u'];
$password = $_GET['p'];
$email = graphite_database_lookupValue('users', 'userid', $userid, 'email');

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();
$mail->Host = $GENERAL_SETTINGS->get('smtp', 'host');
$mail->SMTPAuth = $GENERAL_SETTINGS->get('smtp', 'SMTPAuth');
$mail->Username = $GENERAL_SETTINGS->get('smtp', 'username');
$mail->Password = $GENERAL_SETTINGS->get('smtp', 'password');
$mail->SMTPSecure = $GENERAL_SETTINGS->get('smtp', 'SMTPSecure');
$mail->Port = $GENERAL_SETTINGS->get('smtp', 'port');

$mail->setFrom('graphite@mjsmedia.co.uk', 'MJS Media Graphite');
$mail->addAddress($email);          // Name is optional

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Welcome to Graphite';
$mail->Body    = '
<h1>Welcome to Graphite</h1> <h3>A content management system, tailored just for you!</h3>
An account has been created for you on Graphite with your email address ('.$email.').
<br>To log in, go to <a href="http://'.$currentUrl.'/admin"><em>'.$currentUrl.'/admin</em></a>.
<br>Your Password is: '.$password.'<br><strong>Remember to change this in your Account Settings once you have logged in.</strong>
<hr>
<small>You are receiving this email because Graphite has been set up on your website and an account has been created for you by <a href="https://mjsmedia.co.uk">MJS Media</a>.</small>
';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    header('Location: /admin/settings/users');
}

?>