<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$GRAPHITE_SETTINGS->set('blog', 'enabled', $_POST['blogEnabled']);
$GRAPHITE_SETTINGS->set('blog', 'archiveLocation', $_POST['archiveLocation']);
$GRAPHITE_SETTINGS->set('blog', 'featuredImage', $_POST['featuredImage']);
$GRAPHITE_SETTINGS->set('blog', 'author', $_POST['author']);
$GRAPHITE_SETTINGS->set('blog', 'metaDetails', $_POST['metaDetails']);
$GRAPHITE_SETTINGS->set('blog', 'customFields', $_POST['customFields']);
$GRAPHITE_SETTINGS->set('blog', 'customField1Name', $_POST['customField1Name']);
$GRAPHITE_SETTINGS->set('blog', 'customField2Name', $_POST['customField2Name']);
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/settings/blog");
?>