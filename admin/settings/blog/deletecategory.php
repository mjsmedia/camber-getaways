<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$id = $_GET['id'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM blog_categories WHERE categoryid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'Category has been deleted.');
    header("Location: /admin/settings/blog");
} else {
    graphite_notification('error', 'Could not delete category.');
    header("Location: /admin/settings/blog");
}

mysqli_close($conn);
?>