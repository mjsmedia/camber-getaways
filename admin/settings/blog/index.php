<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$pageTitle = 'Settings';            //Page Title
$pageSlug = 'settings';             //Page Slug
$settingsSlug = 'blog'
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12 col-md-3">
                    <?php include_once $ROOTLOCATION.'includes/settingsnav.php';?>
                </div>
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <h2><i class="fas fa-newspaper"></i> Blog</h2>
                            <p class="mt-0">Archive of text and multimedia based posts. Suitable for Blogging, News etc...</p>
                        </div>
                    </div>
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving')" id="form">
                        <div class="row">
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>General</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="blogEnabled" value="true" <?php if($GRAPHITE_SETTINGS->get('blog', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enabled</span>
                                            </label>
                                            <p class="form-text text-muted">If active, the blog posts content type will become available.</p>
                                        </div>
                                        <a href="#" data-toggle="modal" data-target=".code" class="btn btn-secondary">Code <i class="fa fa-code"></i></a>
                                        <a href="#" data-toggle="modal" data-target=".categories" class="btn btn-secondary">Categories <i class="fa fa-folder"></i></a>
                                        <div class="form-group mt-2">
                                            <label>Archive Location</label>
                                            <input type="text" class="form-control col-12 col-md-4" name="archiveLocation" value="<?php echo $GRAPHITE_SETTINGS->get('blog', 'archiveLocation');?>" onchange="unsavedChanges()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Featured Image</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="featuredImage" value="true" <?php if($GRAPHITE_SETTINGS->get('blog', 'featuredImage') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enabled</span>
                                            </label>
                                            <p class="form-text text-muted">If active, a Featured Image uploader will become available in the post editor. This will set a featured image, thumbnail image and SEO friendly image for the post.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Features</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="author" value="true" <?php if($GRAPHITE_SETTINGS->get('blog', 'author') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable Author</span>
                                            </label>
                                            <p class="form-text text-muted">Makes the Author variable available to edit on an individual post basis.</p>
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="metaDetails" value="true" <?php if($GRAPHITE_SETTINGS->get('blog', 'metaDetails') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable SEO Meta Tags</span>
                                            </label>
                                            <p class="form-text text-muted">Allows entry of meta descriptions and custom sharing images.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Custom Fields</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="customFields" value="true" <?php if($GRAPHITE_SETTINGS->get('blog', 'customFields') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enabled</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>Custom Field 1 Name</label>
                                            <input type="text" class="form-control col-12 col-md-4" name="customField1Name" value="<?php echo $GRAPHITE_SETTINGS->get('blog', 'customField1Name');?>" onchange="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Custom Field 2 Name</label>
                                            <input type="text" class="form-control col-12 col-md-4" name="customField2Name" value="<?php echo $GRAPHITE_SETTINGS->get('blog', 'customField2Name');?>" onchange="unsavedChanges()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal fade code" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="list-group mb-2">
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">Title</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET['postid'],'title');?&gt;</code></p>
                            </li>
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">Publish Date (Formatted)</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET['postid'],'publishDate');?&gt;</code></p>
                            </li>
                            <?php
                            if($GRAPHITE_SETTINGS->get('blog', 'featuredImage') == 'true'){echo'
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">Featured Image</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET[&apos;postid&apos;],&apos;featuredImage&apos;);?&gt;</code></p>
                            </li>
                            ';}?>
                            <?php
                            if($GRAPHITE_SETTINGS->get('blog', 'author') == 'true'){echo'
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">Author</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET[&apos;postid&apos;],&apos;author&apos;);?&gt;</code></p>
                            </li>
                            ';}?>
                            <?php
                            if($GRAPHITE_SETTINGS->get('blog', 'metaDetails') == 'true'){echo'
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">Meta Description</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET[&apos;postid&apos;],&apos;metaDescription&apos;);?&gt;</code></p>
                            </li>
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">Meta Image URL</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET[&apos;postid&apos;],&apos;metaImageURL&apos;);?&gt;</code></p>
                            </li>
                            ';}?>
                            <?php
                            if($GRAPHITE_SETTINGS->get('blog', 'customFields') == 'true'){echo'
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">'.$GRAPHITE_SETTINGS->get('blog', 'customField1Name').'</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET[&apos;postid&apos;],&apos;customField1&apos;);?&gt;</code></p>
                            </li>
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">'.$GRAPHITE_SETTINGS->get('blog', 'customField2Name').'</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET[&apos;postid&apos;],&apos;customField2&apos;);?&gt;</code></p>
                            </li>
                            ';}?>

                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">Content</h5>
                                <p class="mb-0"><code>&lt;?php echo graphite_blogPostValue($_GET['postid'],'content');?&gt;</code></p>
                            </li>
                        </div>
                        <div class="list-group mb-2">
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">All Posts</h5>
                                <p class="mb-0"><code>&lt;?php graphite_blogPosts();?&gt;</code></p>
                            </li>
                        </div>
                        <div class="list-group">
                            <?php graphite_blog_listCode();?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade categories" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row" style="margin-bottom:10px;">
                            <div class="col-xs-12 col-md-12">
                                <form action="createcategory.php" method="post" onsubmit="buttonFeedback('submit', 'Creating Category')">
                                    <div class="form-group">
                                        <label>Category Name</label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                    <div class="form-group text-right">
                                        <button id="submit" class="btn btn-primary" type="submit">Create Category</button>
                                    </div>
                                </form>
                                <hr>
                            </div>
                        </div>
                        <?php graphite_blog_listCategories();?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include_once $ROOTLOCATION.'includes/formBar.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>