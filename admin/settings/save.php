<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$GENERAL_SETTINGS->set('general', 'siteTitle', $_POST['siteTitle']);
$GENERAL_SETTINGS->set('general', 'enableHTTPS', $_POST['https']);
$GENERAL_SETTINGS->set('general', 'dateFormat', $_POST['dateFormat']);

$GENERAL_SETTINGS->set('analytics', 'enabled', $_POST['analyticsEnabled']);
$GENERAL_SETTINGS->set('analytics', 'trackingID', $_POST['analyticsTrackingID']);

$GENERAL_SETTINGS->set('aws', 'enabled', $_POST['awsEnabled']);
$GENERAL_SETTINGS->set('aws', 'graphiteSiteID', $_POST['awsGraphiteSiteID']);
$GENERAL_SETTINGS->set('aws', 'requestUrl', $_POST['awsRequestUrl']);
$GENERAL_SETTINGS->set('aws', 'accessKey', $_POST['awsAccessKey']);
$GENERAL_SETTINGS->set('aws', 'secretKey', $_POST['awsSecretKey']);
$GENERAL_SETTINGS->save();

$GRAPHITE_SETTINGS->set('graphite', 'lock', $_POST['lock']);
$GRAPHITE_SETTINGS->set('graphite', 'lockReason', $_POST['lockReason']);

$GRAPHITE_SETTINGS->set('blog', 'enabled', $_POST['enableBlog']);
$GRAPHITE_SETTINGS->set('testimonials', 'enabled', $_POST['enableTestimonials']);
$GRAPHITE_SETTINGS->set('portfolio', 'enabled', $_POST['enablePortfolio']);
$GRAPHITE_SETTINGS->set('gallery', 'enabled', $_POST['enableGallery']);
$GRAPHITE_SETTINGS->set('pages', 'enabled', $_POST['enablePages']);
$GRAPHITE_SETTINGS->set('variables', 'enabled', $_POST['enableVariables']);
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/settings/");
?>