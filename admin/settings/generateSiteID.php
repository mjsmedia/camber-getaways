<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---
$id = graphite_randomString(22);

$graphiteSiteID = graphite_prepareSlugString($siteTitle).'-'.$id;

$GENERAL_SETTINGS->set('aws', 'graphiteSiteID', $graphiteSiteID);
$GENERAL_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/settings/");

?>