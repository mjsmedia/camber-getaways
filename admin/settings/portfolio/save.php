<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$GRAPHITE_SETTINGS->set('portfolio', 'enabled', $_POST['portfolioEnabled']);
$GRAPHITE_SETTINGS->set('portfolio', 'archiveLocation', $_POST['archiveLocation']);
$GRAPHITE_SETTINGS->set('portfolio', 'featuredImage', $_POST['featuredImage']);
$GRAPHITE_SETTINGS->set('portfolio', 'author', $_POST['author']);
$GRAPHITE_SETTINGS->set('portfolio', 'metaDetails', $_POST['metaDetails']);
$GRAPHITE_SETTINGS->set('portfolio', 'customFields', $_POST['customFields']);
$GRAPHITE_SETTINGS->set('portfolio', 'customField1Name', $_POST['customField1Name']);
$GRAPHITE_SETTINGS->set('portfolio', 'customField2Name', $_POST['customField2Name']);
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/settings/portfolio");
?>