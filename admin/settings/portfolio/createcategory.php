<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkContentActive('portfolio');

$categoryid = graphite_randomString();
$name = $_POST['name'];

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO portfolio_categories (categoryid,name)
    VALUES ('$categoryid','$name');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Category Created.');
    header('Location: /admin/settings/portfolio');
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/settings/portfolio');
}

mysqli_close($conn);
?>