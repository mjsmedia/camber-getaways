<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$GRAPHITE_SETTINGS->set('variables', 'enabled', $_POST['variablesEnabled']);
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/settings/variables");
?>