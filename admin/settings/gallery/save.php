<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$GRAPHITE_SETTINGS->set('gallery', 'enabled', $_POST['galleryEnabled']);
$GRAPHITE_SETTINGS->set('gallery', 'imageType', $_POST['imageType']);
$GRAPHITE_SETTINGS->set('gallery', 'imageWidth', $_POST['imageWidth']);
$GRAPHITE_SETTINGS->set('gallery', 'imageHeight', $_POST['imageHeight']);
$GRAPHITE_SETTINGS->set('gallery', 'imageQuality', $_POST['imageQuality']);
$GRAPHITE_SETTINGS->set('gallery', 'thumbnailType', $_POST['thumbnailType']);
$GRAPHITE_SETTINGS->set('gallery', 'thumbnailWidth', $_POST['thumbnailWidth']);
$GRAPHITE_SETTINGS->set('gallery', 'thumbnailHeight', $_POST['thumbnailHeight']);
$GRAPHITE_SETTINGS->set('gallery', 'thumbnailQuality', $_POST['thumbnailQuality']);
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/settings/gallery");
?>