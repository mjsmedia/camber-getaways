<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$pageTitle = 'Settings';            //Page Title
$pageSlug = 'settings';             //Page Slug
$settingsSlug = 'gallery'
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12 col-md-3">
                    <?php include_once $ROOTLOCATION.'includes/settingsnav.php';?>
                </div>
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <h2><i class="fas fa-images"></i> Gallery</h2>
                            <p class="m-0">Simple image based entries.</p>
                        </div>
                    </div>
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving')" id="form">
                        <div class="row">
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>General</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="galleryEnabled" value="true" <?php if($GRAPHITE_SETTINGS->get('gallery', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enabled</span>
                                            </label>
                                            <p class="form-text text-muted">If active, the gallery content type will become available.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Image</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-radio">
                                                <input name="imageType" type="radio" class="custom-control-input" <?php if($GRAPHITE_SETTINGS->get('gallery', 'imageType') == 'exact'){echo'checked';}?> value="exact" onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Exact</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input name="imageType" type="radio" class="custom-control-input" <?php if($GRAPHITE_SETTINGS->get('gallery', 'imageType') == 'maximum'){echo'checked';}?> value="maximum" onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Maximum</span>
                                            </label>
                                            <p class="form-text text-muted">Exact crops the image to exactly the dimensions below. Maximum will keep the aspect ratio and set the maximum width or height.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Width</label>
                                            <input type="number" class="form-control col-12 col-md-4" name="imageWidth" value="<?php echo $GRAPHITE_SETTINGS->get('gallery', 'imageWidth');?>" onchange="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Height</label>
                                            <input type="number" class="form-control col-12 col-md-4" name="imageHeight" value="<?php echo $GRAPHITE_SETTINGS->get('gallery', 'imageHeight');?>" onchange="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Quality</label>
                                            <input type="number" class="form-control col-12 col-md-4" name="imageQuality" value="<?php echo $GRAPHITE_SETTINGS->get('gallery', 'imageQuality');?>" onchange="unsavedChanges()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Thumbnail</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-radio">
                                                <input name="thumbnailType" type="radio" class="custom-control-input" <?php if($GRAPHITE_SETTINGS->get('gallery', 'thumbnailType') == 'exact'){echo'checked';}?> value="exact" onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Exact</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input name="thumbnailType" type="radio" class="custom-control-input" <?php if($GRAPHITE_SETTINGS->get('gallery', 'thumbnailType') == 'maximum'){echo'checked';}?> value="maximum" onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Maximum</span>
                                            </label>
                                            <p class="form-text text-muted">Exact crops the image to exactly the dimensions below. Maximum will keep the aspect ratio and set the maximum width or height.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Width</label>
                                            <input type="number" class="form-control col-12 col-md-4" name="thumbnailWidth" value="<?php echo $GRAPHITE_SETTINGS->get('gallery', 'thumbnailWidth');?>" onchange="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Height</label>
                                            <input type="number" class="form-control col-12 col-md-4" name="thumbnailHeight" value="<?php echo $GRAPHITE_SETTINGS->get('gallery', 'thumbnailHeight');?>" onchange="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Quality</label>
                                            <input type="number" class="form-control col-12 col-md-4" name="thumbnailQuality" value="<?php echo $GRAPHITE_SETTINGS->get('gallery', 'thumbnailQuality');?>" onchange="unsavedChanges()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/formBar.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>