<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$pageTitle = 'Settings';            //Page Title
$pageSlug = 'settings';             //Page Slug
$settingsSlug = 'testimonials'
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12 col-md-3">
                    <?php include_once $ROOTLOCATION.'includes/settingsnav.php';?>
                </div>
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <h2><i class="fas fa-comments"></i> Testimonials</h2>
                            <p class="mt-0">Simple text based entries.</p>
                            
                        </div>
                    </div>
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving')" id="form">
                        
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>General</strong></p>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="testimonialsEnabled" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Enabled</span>
                                        </label>
                                        <p class="form-text text-muted">If active, the testimonials posts content type will become available.</p>
                                        <a href="#" data-toggle="modal" data-target=".code" class="btn btn-secondary">Code <i class="fa fa-code"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 mt-4">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-12">
                                                <p><strong>Features</strong></p>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="firstName" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'firstName') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">First Name</span>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="lastName" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'lastName') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Last Name</span>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="company" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'company') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Company</span>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="website" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'website') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Website</span>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="date" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'date') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Date</span>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="customField" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'customField') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Custom Field</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 mt-4">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Custom Field</strong></p>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="customFieldRequired" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'customFieldRequired') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Required</span>
                                        </label>
                                        <div class="form-group">
                                            <label>Label</label>
                                            <input type="text" class="form-control col-12 col-md-4" name="customFieldName" value="<?php echo $GRAPHITE_SETTINGS->get('testimonials', 'customFieldName');?>" onchange="unsavedChanges()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade code" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="list-group">
                            <li class="list-group-item flex-column align-items-start">
                                <h5 class="mb-1">All Testimonials</h5>
                                <p class="mb-0"><code>&lt;?php graphite_testimonials();?&gt;</code></p>
                            </li>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/formBar.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>