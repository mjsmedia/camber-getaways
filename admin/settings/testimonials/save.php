<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

//---

$GRAPHITE_SETTINGS->set('testimonials', 'enabled', $_POST['testimonialsEnabled']);
$GRAPHITE_SETTINGS->set('testimonials', 'firstName', $_POST['firstName']);
$GRAPHITE_SETTINGS->set('testimonials', 'lastName', $_POST['lastName']);
$GRAPHITE_SETTINGS->set('testimonials', 'customField', $_POST['customField']);
$GRAPHITE_SETTINGS->set('testimonials', 'customFieldName', $_POST['customFieldName']);
$GRAPHITE_SETTINGS->set('testimonials', 'customFieldRequired', $_POST['customFieldRequired']);
$GRAPHITE_SETTINGS->set('testimonials', 'company', $_POST['company']);
$GRAPHITE_SETTINGS->set('testimonials', 'website', $_POST['website']);
$GRAPHITE_SETTINGS->set('testimonials', 'date', $_POST['date']);
$GRAPHITE_SETTINGS->save();

graphite_notification('success', 'Changes saved.');
header("Location: /admin/settings/testimonials");
?>