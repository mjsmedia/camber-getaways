<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$pageTitle = 'Settings';            //Page Title
$pageSlug = 'settings';             //Page Slug
$settingsSlug = 'general'
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12 col-md-3">
                    <?php include_once $ROOTLOCATION.'includes/settingsnav.php';?>
                </div>
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <img src="/admin/assets/logo-dark-wide.png" height="40">
                            <p class="m-0"><strong><?php echo $GRAPHITE_SETTINGS->get('graphite', 'versionName');?></strong> <?php echo $versionGraphite;?></p>
                        </div>
                    </div>
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving')" id="form">
                        <div class="row">
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Website Settings</strong></p>
                                       <div class="form-group">
                                            <label>Site Title</label>
                                            <input type="text" class="form-control col-12 col-md-4" name="siteTitle" value="<?php echo $siteTitle;?>" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Date Format</label>
                                            <input type="text" class="form-control col-12 col-md-4" name="dateFormat" value="<?php echo $graphiteDateFormat;?>" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="https" value="true" <?php if($enableHTTPS == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Use HTTPS</span>
                                            </label>
                                            <p class="form-text text-muted">If active, all connections to the frontend website and Graphite will be directed to use the HTTPS protocol. If a certificate is not available, this may cause connection problems.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Google Analytics</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="analyticsEnabled" value="true" <?php if($GENERAL_SETTINGS->get('analytics', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>Tracking ID</label>
                                            <input type="text" class="form-control col-12 col-md-4" name="analyticsTrackingID" value="<?php echo $GENERAL_SETTINGS->get('analytics', 'trackingID');?>" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()" placeholder="E.g. UA-12345678-9">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Amazon Web Services S3</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="awsEnabled" value="true" <?php if($GENERAL_SETTINGS->get('aws', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>Graphite Site ID <a href="generateSiteID.php" class="btn btn-secondary btn-sm">Generate</a></label>
                                            <input type="text" class="form-control col-12 col-md-6" name="awsGraphiteSiteID" value="<?php echo $GENERAL_SETTINGS->get('aws', 'graphiteSiteID');?>" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Request Location</label>
                                            <input type="text" class="form-control col-12 col-md-6" name="awsRequestUrl" value="<?php echo $GENERAL_SETTINGS->get('aws', 'requestUrl');?>" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Access Key</label>
                                            <input type="text" class="form-control col-12 col-md-6" name="awsAccessKey" value="<?php echo $GENERAL_SETTINGS->get('aws', 'accessKey');?>" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()">
                                        </div>
                                        <div class="form-group">
                                            <label>Secret Key</label>
                                            <input type="text" class="form-control col-12 col-md-6" name="awsSecretKey" value="<?php echo $GENERAL_SETTINGS->get('aws', 'secretKey');?>" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>Content Types</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="enableBlog" value="true" <?php if($GRAPHITE_SETTINGS->get('blog', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable Blog</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="enableTestimonials" value="true" <?php if($GRAPHITE_SETTINGS->get('testimonials', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable Testimonials</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="enablePortfolio" value="true" <?php if($GRAPHITE_SETTINGS->get('portfolio', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable Portfolio</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="enableGallery" value="true" <?php if($GRAPHITE_SETTINGS->get('gallery', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable Gallery</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="enablePages" value="true" <?php if($GRAPHITE_SETTINGS->get('pages', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable Pages</span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="enableVariables" value="true" <?php if($GRAPHITE_SETTINGS->get('variables', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enable Variables</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <div class="card card-outline-danger">
                                    <div class="card-block">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="lock" value="true" <?php if($GRAPHITE_SETTINGS->get('graphite', 'lock') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Lock Graphite</span>
                                        </label>
                                        <p class="form-text text-muted">If active, accounts with less than administrator permissions will not be able to alter any content via Graphite.</p>
                                        <label>Lock Reason</label>
                                        <textarea class="form-control" name="lockReason" rows="2" onchange="unsavedChanges()" onkeypress="unsavedChanges()" onpaste="unsavedChanges()"><?php echo $GRAPHITE_SETTINGS->get('graphite', 'lockReason');?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/formBar.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>