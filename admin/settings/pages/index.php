<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

$pageTitle = 'Settings';            //Page Title
$pageSlug = 'settings';             //Page Slug
$settingsSlug = 'pages'
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12 col-md-3">
                    <?php include_once $ROOTLOCATION.'includes/settingsnav.php';?>
                </div>
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <h2><i class="fas fa-file-alt"></i> Pages</h2>
                            <p class="m-0">Unarchived text and multimedia based entries. To be used for specific static-page content.</p>
                        </div>
                    </div>
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving')" id="form">
                        <div class="row">
                            <div class="col-12 my-2">
                                <div class="card card-outline">
                                    <div class="card-block">
                                        <p><strong>General</strong></p>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="pagesEnabled" value="true" <?php if($GRAPHITE_SETTINGS->get('pages', 'enabled') == 'true'){echo'checked';}?> onchange="unsavedChanges()">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Enabled</span>
                                            </label>
                                            <p class="form-text text-muted">If active, the testimonials posts content type will become available.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/formBar.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>