<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('testimonials');

graphite_checkLock($_SESSION['userid']);

$testimonialid = $_POST['testimonialid'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$customfield = $_POST['customfield'];
$company = $_POST['company'];
$website = $_POST['website'];
$testimonialDate = $_POST['testimonialDate'];
$content = $_POST['content'];
$headline = $_POST['headline'];
$platform = $_POST['platform'];

$content = graphite_replaceEntities($content);
$headline = graphite_replaceEntities($headline);

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE testimonials
SET firstname= '$firstname', lastname= '$lastname', customfield= '$customfield', company= '$company', website= '$website', testimonialDate= '$testimonialDate', content= '$content', headline='$headline', platform='$platform'
WHERE testimonialid = '$testimonialid';";

if ($conn->query($sql) === TRUE) {
    graphite_logActivity(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'username'), 'Edited Testimonial: '.$testimonialid);
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/testimonials/edit/?id='.$testimonialid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/testimonials/edit/?id='.$testimonialid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>