<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('testimonials');

$testimonialid = $_GET['id'];

graphite_item_exists('testimonials', 'testimonialid', $testimonialid , '/admin/testimonials');

$pageTitle = 'Edit Testimonial';            //Page Title
$pageSlug = 'testimonials';             //Page Slug

$firstname = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname');
$lastname = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'lastname');
if(empty($firstname)){
    $author = '';
}else{
    if(empty($lastname)){
        $author = $firstname;
    }else{
        $author = $firstname.' '.$lastname;
    }
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-comments"></i> Testimonials <small class="text-muted hidden-lg-up">Edit</small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1>Edit Testimonial</h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/testimonials"><i class="fas fa-angle-left"></i> All Testimonials</a>
                </div>
            </div>
            <hr>
            <?php if($GRAPHITE_SETTINGS->get('testimonials', 'website') == 'true'){
            if(strpos(graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'website'), 'http') === FALSE){echo'
            <div class="alert alert-warning" role="alert">
                <strong>Warning!</strong> Your website link looks weird! Make sure it starts with either <strong>https://</strong> or <strong>http://</strong>. For example: <em>https://www.google.co.uk</em>
            </div>';}}?>
            <form action="save.php" method="post" onsubmit="buttonFeedback('submit', 'Saving')">
            <div class="row">
                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'firstName') == 'true'){echo'
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>First Name <small class="text-muted">Required</small></label>
                        <input type="text" class="form-control" name="firstname" placeholder="Joe" required value="'.graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'firstname').'">
                    </div>
                </div>
                ';}?>
                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'lastName') == 'true'){echo'
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" name="lastname" placeholder="Bloggs" value="'.graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'lastname').'">
                    </div>
                </div>
                ';}?>
                <?php
                if($GRAPHITE_SETTINGS->get('testimonials', 'customFieldRequired') == 'true'){$req = 'required'; $reqLabel = '<small class="text-danger">Required</small>';}else{$req = ''; $reqLabel = '';}
                if($GRAPHITE_SETTINGS->get('testimonials', 'customField') == 'true'){echo'
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>'.$GRAPHITE_SETTINGS->get('testimonials', 'customFieldName').' '.$reqLabel.'</label>
                        <input type="text" class="form-control" name="customfield" value="'.graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'customfield').'" '.$req.'>
                    </div>
                </div>
                ';}?>
                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'company') == 'true'){echo'
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Company</label>
                        <input type="text" class="form-control" name="company" placeholder="Acme Corporation" value="'.graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'company').'">
                    </div>
                </div>
                ';}?>
                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'website') == 'true'){echo'
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Website</label>
                        <input type="text" class="form-control" name="website" value="'.graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'website').'">
                    </div>
                </div>
                ';}?>
                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'date') == 'true'){echo'
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Date</label>
                        <input type="date" class="form-control" name="testimonialDate" placeholder="https://google.com" value="'.graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'testimonialDate').'">
                    </div>
                </div>
                ';}?>
            </div>
                <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="form-group">
                        <label>Headline <small class="text-danger">Required</small></label>
                        <input type="text" class="form-control" name="headline" required value="<?php echo graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'headline');?>">
                    </div>
                </div>
            </div>
                <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="form-group">
                        <label>Content <small class="text-muted">Required</small></label>
                        <textarea class="form-control" rows="5" name="content" required><?php echo graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'content');?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="form-group">
                        <label>Platform <small class="text-danger">Required</small></label>
                        <select name="platform" class="form-control" required>
                            <option <?php if(graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'platform') == 'tripadvisor'){echo'selected';}?> value="tripadvisor">Trip Advisor</option>
                            <option <?php if(graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'platform') == 'homeaway'){echo'selected';}?> value="homeaway">Home Away</option>
                            <option <?php if(graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'platform') == 'airbnb'){echo'selected';}?> value="airbnb">AirBNB</option>
                            <option <?php if(graphite_database_lookupValue('testimonials', 'testimonialid', $testimonialid, 'platform') == 'other'){echo'selected';}?> value="other">Other</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <input type="hidden" name="testimonialid" value="<?php echo $testimonialid;?>">
                    <button id="submit" class="btn btn-primary" type="submit">Save Changes</button>
                </div>
            </div>
            </form>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>