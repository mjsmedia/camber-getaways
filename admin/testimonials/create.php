<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('testimonials');

graphite_checkLock($_SESSION['userid']);

$testimonialid = graphite_randomString();
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$customfield = $_POST['customfield'];
$company = $_POST['company'];
$website = $_POST['website'];
$testimonialDate = $_POST['testimonialDate'];
$content = $_POST['content'];
$headline = $_POST['headline'];
$platform = $_POST['platform'];

$content = graphite_replaceEntities($content);
$headline = graphite_replaceEntities($headline);

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO testimonials (testimonialid,firstname,lastname,customfield,company,website,testimonialDate,content,headline,platform)
    VALUES ('$testimonialid','$firstname','$lastname','$customfield','$company','$website','$testimonialDate','$content','$headline','$platform');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Post Created.');
    header('Location: /admin/testimonials/');
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/testimonials/');
}

mysqli_close($conn);
?>