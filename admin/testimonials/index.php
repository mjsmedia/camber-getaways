<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('testimonials');

$pageTitle = 'Testimonials';            //Page Title
$pageSlug = 'testimonials';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-comments"></i> Testimonials <small class="text-muted hidden-lg-up">All Testimonials</small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 col-md-9 col-lg-9 hidden-md-down">
                    <h1>All Testimonials</h1>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                    <a style="width:100%" class="btn btn-primary" href="#" data-toggle="modal" data-target=".newTestimonial">New Testimonial <i class="fas fa-plus-square"></i></a>
                </div>
            </div>
            <?php graphite_testimonials_list();?>
        </div>

        <div class="modal fade newTestimonial" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="create.php" method="post" onsubmit="buttonFeedback('create', 'Creating')">
                        <div class="modal-body">
                            <div class="row">
                                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'firstName') == 'true'){echo'
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label>First Name <small class="text-danger">Required</small></label>
                                        <input type="text" class="form-control" name="firstname" placeholder="Joe" required>
                                    </div>
                                </div>
                                ';}?>
                                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'lastName') == 'true'){echo'
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label>Last Name <small class="text-danger">Required</small></label>
                                        <input type="text" class="form-control" name="lastname" placeholder="Bloggs" required>
                                    </div>
                                </div>
                                ';}?>
                                <?php
                                if($GRAPHITE_SETTINGS->get('testimonials', 'customFieldRequired') == 'true'){$req = 'required'; $reqLabel = '<small class="text-danger">Required</small>';}else{$req = ''; $reqLabel = '';}
                                if($GRAPHITE_SETTINGS->get('testimonials', 'customField') == 'true'){echo'
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label>'.$GRAPHITE_SETTINGS->get('testimonials', 'customFieldName').' '.$reqLabel.'</label>
                                        <input type="text" class="form-control" name="customfield" '.$req.'>
                                    </div>
                                </div>
                                ';}?>
                                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'company') == 'true'){echo'
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <input type="text" class="form-control" name="company" placeholder="Acme Corporation">
                                    </div>
                                </div>
                                ';}?>
                                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'website') == 'true'){echo'
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label>Website</label>
                                        <input type="text" class="form-control" name="website" placeholder="https://google.com">
                                    </div>
                                </div>
                                ';}?>
                                <?php if($GRAPHITE_SETTINGS->get('testimonials', 'date') == 'true'){echo'
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label>Date <small class="text-danger">Required</small></label>
                                        <input type="date" class="form-control" name="testimonialDate" required>
                                    </div>
                                </div>
                                ';}?>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Headline <small class="text-danger">Required</small></label>
                                        <input type="text" class="form-control" name="headline" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Content <small class="text-danger">Required</small></label>
                                        <textarea class="form-control" rows="5" name="content" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Platform <small class="text-danger">Required</small></label>
                                        <select name="platform" class="form-control" required>
                                            <option value="tripadvisor">Trip Advisor</option>
                                            <option value="homeaway">Home Away</option>
                                            <option value="airbnb">AirBNB</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="create" type="submit" class="btn btn-primary">Create Testimonial</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>