<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM testimonials WHERE testimonialid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'Testimonial has been deleted.');
    header("Location: /admin/testimonials");
} else {
    graphite_notification('error', 'Could not delete testimonial.');
    header("Location: /admin/testimonials");
}

mysqli_close($conn);
?>