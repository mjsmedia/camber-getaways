<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();

$pageTitle = 'Dashboard';            //Page Title
$pageSlug = 'dashboard';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-tachometer-alt"></i> Dashboard <br><small class="text-muted hidden-lg-up"><?php echo graphite_timeGreeting();?></small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 hidden-md-down">
                    <h1><?php echo graphite_timeGreeting();?></h1>
                </div>
            </div>
            <style>
                .dashboardButton{
                    -webkit-transition-duration: 0.2s; /* Safari */
                    transition-duration: 0.2s;
                }
                .dashboardButton:hover{
                    background-color: #3D6FDD;
                }
                .dashboardButton h1{
                    margin: 0;
                    text-decoration: none;
                }
                .dashboardButton p{
                    margin: 0;
                    text-decoration: none;
                }
                .dashboardButtonLink{
                    text-decoration: none !important;
                    color: #292b2c;
                }
                .dashboardButtonLink:hover{
                    color: white;
                }
            </style>
            <div class="row" style="margin-top:20px">

                <?php
                if($GRAPHITE_SETTINGS->get('blog', 'enabled') == 'true'){echo'
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/blog/new">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px">
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-newspaper"></i></h1>
                                <p><i class="fas fa-newspaper hidden-lg-up"></i> New Blog Post</p>
                            </div>
                        </div>
                    </a>
                </div>
                ';}
                ?>
                <?php
                if($GRAPHITE_SETTINGS->get('testimonials', 'enabled') == 'true'){echo'
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/testimonials">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-comments"></i></h1>
                                <p><i class="fas fa-comments hidden-lg-up"></i>Reviews</p>
                            </div>
                        </div>
                    </a>
                </div>
                ';}
                ?>
                <?php
                if($GRAPHITE_SETTINGS->get('portfolio', 'enabled') == 'true'){echo'
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/portfolio/new">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-folder-open"></i></h1>
                                <p><i class="fas fa-folder-open hidden-lg-up"></i> New Portfolio Entry</p>
                            </div>
                        </div>
                    </a>
                </div>
                ';}
                ?>
                <?php
                if($GRAPHITE_SETTINGS->get('gallery', 'enabled') == 'true'){echo'
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/gallery/">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-images"></i></h1>
                                <p><i class="fas fa-images hidden-lg-up"></i> Gallery</p>
                            </div>
                        </div>
                    </a>
                </div>
                ';}
                ?>
                <?php
                if($GRAPHITE_SETTINGS->get('pages', 'enabled') == 'true'){echo'
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/pages">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-file-alt"></i></h1>
                                <p><i class="fas fa-file-alt hidden-lg-up"></i> Edit Pages</p>
                            </div>
                        </div>
                    </a>
                </div>
                ';}
                ?>
                <?php
                if($GRAPHITE_SETTINGS->get('variables', 'enabled') == 'true'){echo'
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/variables">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-exchange-alt"></i></h1>
                                <p><i class="fas fa-exchange-alt hidden-lg-up"></i> Variables</p>
                            </div>
                        </div>
                    </a>
                </div>
                ';}
                ?>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/caravans">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-home"></i></h1>
                                <p><i class="fas fa-home hidden-lg-up"></i> Caravans</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/events">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-calendar-alt"></i></h1>
                                <p><i class="fas fa-calendar-alt hidden-lg-up"></i> Events</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/featured">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-star"></i></h1>
                                <p><i class="fas fa-star hidden-lg-up"></i> Featured Property</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/date-selection/">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-calendar-check"></i></h1>
                                <p><i class="fas fa-calendar-check hidden-lg-up"></i> Date Selection</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/banners/">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-bullhorn"></i></h1>
                                <p><i class="fas fa-bullhorn hidden-lg-up"></i> Info Banners</p>
                            </div>
                        </div>
                    </a>
                </div>
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/advert/">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fas fa-bookmark"></i></h1>
                                <p><i class="fas fa-bookmark hidden-lg-up"></i> Homepage Advert</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                    <a class="dashboardButtonLink" href="/admin/account">
                        <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                            <div class="card-block">
                                <h1 class="hidden-md-down"><i class="fa fas fa-user"></i></h1>
                                <p><i class="fa fas fa-user hidden-lg-up"></i> Account</p>
                            </div>
                        </div>
                    </a>
                </div>
                <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){
                echo'
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                        <a class="dashboardButtonLink" href="/admin/settings">
                            <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                                <div class="card-block">
                                    <h1 class="hidden-md-down"><i class="fa fa-cogs"></i></h1>
                                    <p><i class="fa fa-cogs hidden-lg-up"></i> Settings</p>
                                </div>
                            </div>
                        </a>
                    </div>
                ';}?>
                <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') == 1){
                echo'
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center" style="margin-bottom:30px">
                        <a class="dashboardButtonLink" href="/admin/user-management">
                            <div class="card dashboardButton" style="padding-top:20px;padding-bottom:20px" >
                                <div class="card-block">
                                    <h1 class="hidden-md-down"><i class="fa fa-users"></i></h1>
                                    <p><i class="fa fa-users hidden-lg-up"></i> User Management</p>
                                </div>
                            </div>
                        </a>
                    </div>
                ';}?>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <?php if($GRAPHITE_SETTINGS->get('graphite', 'lock') == 'true'){echo'

                <script>swal("Lock Mode", "Graphite has been locked and you will be unable to make any changes.", "info");</script>
                ';}?>
    </body>
</html>
<?php graphite_clearNotification();?>
