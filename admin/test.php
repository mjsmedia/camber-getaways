<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();

$pageTitle = 'Dashboard';            //Page Title
$pageSlug = 'dashboard';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-tachometer-alt"></i> Dashboard <br><small class="text-muted hidden-lg-up"><?php echo graphite_timeGreeting();?></small></h4>
                </div>
            </div>
            <style>
                .draggable li{
                    display: inline-block;
                    height: 100px;
                    width: 100px;
                    border: 1px solid #ba0f0f;
                    margin: 10px;
                }
                .draggable li:hover{
                    cursor: move!important;
                }
                .sortable-placeholder{
                    height: 100px;
                    width: 100px;
                    border: 1px solid #ff0000;
                    margin: 10px;
                }
            </style>
            <div class="row">
                <div class="col-12">
                    <ul class="draggable">
                        <?php graphite_caravans_photosList();?>
                    </ul>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <?php if($GRAPHITE_SETTINGS->get('graphite', 'lock') == 'true'){echo'
        <script>swal("Lock Mode", "Graphite has been locked and you will be unable to make any changes.", "info");</script>
        ';}?>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script>
            $( ".draggable" ).sortable({
                placeholder: "col-6",
                // Cancel the drag when selecting contenteditable items, buttons, or input boxes
                cancel: ":input,button,[contenteditable]",
                // Triggered when the user has finished moving a row
                update: function (event, ui) {
                    // sortable() - Creates an array of the elements based on the element's id. 
                    // The element id must be a word separated by a hyphen, underscore, or equal sign. For example, <tr id='item-1'>
                    var data = $(this).sortable('serialize');
                    
                    //alert(data); // Uncomment this to see what data will be sent to the server

                    // AJAX POST to server
                    $.ajax({
                        data: data,
                        type: 'POST',
                        url: 'refresh_order.php',
                        success: function(response) {
                             alert(response); // Uncomment this to see the server's response
                        }
                    });
                    
                }
            });
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>