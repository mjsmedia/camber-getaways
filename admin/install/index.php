<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/executelite.php';
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <div class="container" style="margin-top:50px;">
            <div class="row">
                <div class="col-12">
                    <img src="/admin/assets/logo-dark-wide.png" height="40">
                </div>
            </div>
            <hr>
            <form action="install.php" method="post" onsubmit="buttonFeedback('installButton', 'Installing')">
                <div class="row">
                    <div class="col-12">
                        <h3>Database Installation <small class="text-muted">Step 1 of 2</small></h3>
                        <p class="text-muted">The following details are required to set up the database for Graphite. Entering these details incorrectly could cause critical errors.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label>Host</label>
                            <input type="text" class="form-control" name="sqlHOST" value="<?php echo $sqlHOST;?>"  required>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" name="sqlUSER" value="<?php echo $sqlUSER;?>"  required>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" class="form-control" name="sqlPASS" value="<?php echo $sqlPASS;?>"  required>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label>Database</label>
                            <input type="text" class="form-control" name="sqlDATA" value="<?php echo $sqlDATA;?>"  required>
                        </div>
                    </div>
                </div>
                <!--
                <div class="row mt-4">
                    <div class="col-12">
                        <h3>Administrator</h3>
                        <p class="text-muted">You will need to create a main administrator account. This account will have the highest permissions and will not be removable. Enter the email address and password for this account below.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="email" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" required>
                        </div>
                    </div>
                </div>
-->
                <div class="row mt-4 text-right">
                    <div class="col-12">
                        <button id="installButton" type="submit" class="btn btn-primary">Install</button>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>