<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/executelite.php';
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <div class="container" style="margin-top:50px;">
            <div class="row">
                <div class="col-12">
                    <img src="/admin/assets/logo-dark-wide.png" height="40">
                </div>
            </div>
            <hr>
            <form action="create.php" method="post" onsubmit="buttonFeedback('continueButton', 'Creating Account')">
                
                <div class="row mt-4">
                    <div class="col-12">
                        <h3>Super User Account <small class="text-muted">Step 2 of 2</small></h3>
                        <p class="text-muted">You will need to create a main super user account. This account will have the highest permissions and will not be removable. Enter the email address and password for this account below.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="email" required value="info@mjsmedia.co.uk">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" class="form-control" name="password" required value="<?php echo graphite_randomString();?>">
                        </div>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">I have stored this password in a safe place.</span>
                        </label>
                    </div>
                </div>
                <div class="row mt-4 text-right">
                    <div class="col-12">
                        <button id="continueButton" type="submit" class="btn btn-primary">Continue</button>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>