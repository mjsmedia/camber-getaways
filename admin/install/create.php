<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/executelite.php';

$userid = graphite_randomString();
$email = $_POST['email'];
$firstname = '';
$lastname = '';

$password = $_POST['password'];
$passwordHash = password_hash($password, PASSWORD_DEFAULT);

$permissions = 3;

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO users (userid,firstname,lastname,email,password,permissions) VALUES ('$userid','$firstname','$lastname','$email','$passwordHash','$permissions');";

if ($conn->query($sql) === TRUE) {
    header('Location: /admin/install/complete');
} else {
    graphite_notification('error', 'User could not be created.');
    header('Location: /admin/install/account');
}

mysqli_close($conn);
?>