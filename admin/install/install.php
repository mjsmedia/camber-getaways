<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/executelite.php';

$GENERAL_SETTINGS->set('database', 'host', $_POST['sqlHOST']);
$GENERAL_SETTINGS->set('database', 'user', $_POST['sqlUSER']);
$GENERAL_SETTINGS->set('database', 'pass', $_POST['sqlPASS']);
$GENERAL_SETTINGS->set('database', 'data', $_POST['sqlDATA']);
$GENERAL_SETTINGS->save();

// Create connection
$conn = new mysqli($_POST['sqlHOST'], $_POST['sqlUSER'], $_POST['sqlPASS'], $_POST['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "
CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `albumid` varchar(11) NOT NULL,
  `name` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `albums_images` (
  `id` int(11) NOT NULL,
  `imageid` varchar(11) NOT NULL,
  `albumid` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `blogid` varchar(11) NOT NULL,
  `title` tinytext NOT NULL,
  `slug` tinytext NOT NULL,
  `publishDate` date NOT NULL,
  `author` tinytext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `metaDescription` tinytext NOT NULL,
  `metaImageUrl` tinytext NOT NULL,
  `customField1` tinytext NOT NULL,
  `customField2` tinytext NOT NULL,
  `content` blob NOT NULL,
  `category` varchar(11) NOT NULL,
  `imageid` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `blog_categories` (
  `id` int(11) NOT NULL,
  `categoryid` varchar(11) NOT NULL,
  `name` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `imageid` varchar(11) NOT NULL,
  `createdBy` tinytext NOT NULL,
  `createdTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `pageid` varchar(11) NOT NULL,
  `name` tinytext NOT NULL,
  `location` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `pages_content` (
  `id` int(11) NOT NULL,
  `pageid` varchar(11) NOT NULL,
  `contentid` varchar(11) NOT NULL,
  `name` tinytext NOT NULL,
  `content` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `portfolioid` varchar(11) NOT NULL,
  `title` tinytext NOT NULL,
  `slug` tinytext NOT NULL,
  `publishDate` date NOT NULL,
  `author` tinytext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `metaDescription` tinytext NOT NULL,
  `metaImageUrl` tinytext NOT NULL,
  `customField1` tinytext NOT NULL,
  `customField2` tinytext NOT NULL,
  `content` blob NOT NULL,
  `category` varchar(11) NOT NULL,
  `imageid` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `portfolio_categories` (
  `id` int(11) NOT NULL,
  `categoryid` varchar(11) NOT NULL,
  `name` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `testimonialid` varchar(11) NOT NULL,
  `firstname` tinytext NOT NULL,
  `lastname` tinytext NOT NULL,
  `customfield` tinytext NOT NULL,
  `company` tinytext NOT NULL,
  `website` tinytext NOT NULL,
  `testimonialDate` date NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(11) NOT NULL,
  `firstname` tinytext NOT NULL,
  `lastname` tinytext NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` tinytext NOT NULL,
  `permissions` int(1) NOT NULL,
  `setup` int(1) NOT NULL DEFAULT '0',
  `lastLogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `variables` (
  `id` int(11) NOT NULL,
  `variableid` varchar(11) NOT NULL,
  `name` tinytext NOT NULL,
  `value` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `albumid` (`albumid`);

ALTER TABLE `albums_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `albumid` (`albumid`);

ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `imageid` (`imageid`);

ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pageid` (`pageid`);

ALTER TABLE `pages_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pageid` (`pageid`);

ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `portfolio_categories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userid` (`userid`),
  ADD UNIQUE KEY `email` (`email`);

ALTER TABLE `variables`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `albums_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pages_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `portfolio_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `variables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `albums_images`
  ADD CONSTRAINT `albums_images_ibfk_1` FOREIGN KEY (`albumid`) REFERENCES `albums` (`albumid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pages_content`
  ADD CONSTRAINT `pages_content_ibfk_1` FOREIGN KEY (`pageid`) REFERENCES `pages` (`pageid`) ON DELETE CASCADE ON UPDATE CASCADE;
";

if ($conn->multi_query($sql) === TRUE) {
    header('Location: /admin/install/account');
} else {
    die("Installation Error 1");
}

mysqli_close($conn);
?>