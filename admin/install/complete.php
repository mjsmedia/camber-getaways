<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/executelite.php';
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <div class="container" style="margin-top:50px;">
            <div class="row">
                <div class="col-12">
                    <img src="/admin/assets/logo-dark-wide.png" height="40">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12">
                    <h3>Installation Complete</h3>
                    <p>Graphite has been successfully installed! The installation screen will now be unavailable.</p>
                    <a class="btn btn-primary" href="/admin">Go to Dashboard</a>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <?php
        $GRAPHITE_SETTINGS->set('graphite', 'installed', 'true');
        $GRAPHITE_SETTINGS->save();
        ?>
    </body>
</html>