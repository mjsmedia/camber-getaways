<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM banners WHERE bannerid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'Banner Deleted');
    header('Location: /admin/banners');
} else {
    graphite_notification('error', 'Could not delete Banner.');
    header('Location: /admin/banners/edit/?id='.$id);
}

mysqli_close($conn);
?>