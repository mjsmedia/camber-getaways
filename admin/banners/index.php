<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$pageTitle = 'Banners';            //Page Title
$pageSlug = 'banners';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-bullhorn"></i> Info Banners <small class="text-muted hidden-lg-up">All Info Banners</small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 col-md-9 col-lg-10 hidden-md-down">
                    <h1>All Banners</h1>
                </div>
                <div class="col-12 col-md-3 col-lg-2">
                    <a style="width:100%" class="btn btn-primary" href="#" data-toggle="modal" data-target=".newPage">New Banner <i class="fas fa-plus-square"></i></a>
                </div>
            </div>

            <div class="modal fade newPage" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="create.php" method="post" onsubmit="buttonFeedback('create', 'Creating')">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Start Date (DD/MM/YYYY) <small class="text-danger">Required</small></label>
                                            <input id="startDate" type="date" name="startdate" class="form-control" required onchange="setDates()">
                                        </div>
                                        <div class="form-group">
                                            <label>End Date (DD/MM/YYYY) <small class="text-danger">Required</small></label>
                                            <input id="endDate" type="date" name="enddate" class="form-control" required disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Placement <small class="text-danger">Required</small></label>
                                            <select class="form-control" name="type" required>
                                                <option value="0">Homepage</option>
                                                <option value="1">Search Results</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Style <small class="text-danger">Required</small></label>
                                            <select class="form-control" name="style" required>
                                                <option value="0">Default</option>
                                                <option value="1">Information</option>
                                                <option value="2">Warning</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Title <small class="text-danger">Required</small></label>
                                            <input type="text" name="title" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Content <small class="text-danger">Required</small></label>
                                            <textarea name="content" class="form-control" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Button Label <small>Leave blank for no button</small></label>
                                            <input type="text" name="buttonLabel" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Button URL</label>
                                            <input type="text" name="buttonUrl" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Text Align <small class="text-danger">Required</small></label>
                                            <select class="form-control" name="textAlign" required>
                                                <option value="0">Left</option>
                                                <option value="1">Center</option>
                                                <option value="2">Right</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="create" type="submit" class="btn btn-primary">Create Event</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php graphite_banners_list();?>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>

        <script>
            function setDates(){
                document.getElementById('endDate').disabled = false;
                document.getElementById('endDate').min = document.getElementById('startDate').value;
                document.getElementById('startDate').max = document.getElementById('endDate').value;
            }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>
