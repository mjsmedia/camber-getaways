<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

$bannerid = $_POST['bannerid'];
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];
$type = $_POST['type'];
$style = $_POST['style'];
$title = str_replace("'","&#39;",$_POST['title']);
$content = str_replace("'","&#39;",$_POST['content']);
$buttonLabel = str_replace("'","&#39;",$_POST['buttonLabel']);
$buttonUrl = $_POST['buttonUrl'];
$textAlign = $_POST['textAlign'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE banners
SET title= '$title',startdate= '$startdate',enddate= '$enddate',type='$type',style='$style',content='$content',buttonLabel='$buttonLabel',buttonUrl='$buttonUrl',textAlign='$textAlign'
WHERE bannerid = '$bannerid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/banners/edit/?id='.$bannerid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/banners/edit/?id='.$bannerid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>