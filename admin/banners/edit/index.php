<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$id = $_GET['id'];

$pageTitle = 'Banners';            //Page Title
$pageSlug = 'Banners';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-bullhorn"></i> Info Banners <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('events', 'eventid', $id, 'name');?></small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1>Edit Banner</h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/banners"><i class="fas fa-angle-left"></i> All Banners</a>
                </div>
            </div>
        </div>
        <hr>
        <div class="container mb-4">
            <div class="row">
                <div class="col-12 col-md-6">
                    <form action="save.php" method="post">
                        <div class="form-group">
                            <label>Start Date (DD/MM/YYYY) <small class="text-danger">Required</small></label>
                            <input id="startDate" name="startdate" type="date" class="form-control" value="<?php echo graphite_database_lookupValue('banners', 'bannerid', $id, 'startdate');?>" onchange="setDates()" required>
                        </div>
                        <div class="form-group">
                            <label>End Date (DD/MM/YYYY) <small class="text-danger">Required</small></label>
                            <input id="endDate" name="enddate" type="date" class="form-control" value="<?php echo graphite_database_lookupValue('banners', 'bannerid', $id, 'enddate');?>" required>
                        </div>
                        <div class="form-group">
                            <label>Placement <small class="text-danger">Required</small></label>
                            <select class="form-control" name="type" required>
                                <option value="0" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'type') == 0){echo'selected';}?>>Homepage</option>
                                <option value="1" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'type') == 1){echo'selected';}?>>Search Results</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Style <small class="text-danger">Required</small></label>
                            <select class="form-control" name="style" required>
                                <option value="0" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'style') == 0){echo'selected';}?>>Default</option>
                                <option value="1" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'style') == 1){echo'selected';}?>>Information</option>
                                <option value="2" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'style') == 2){echo'selected';}?>>Warning</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Title <small class="text-danger">Required</small></label>
                            <input name="title" type="text" class="form-control" value="<?php echo graphite_database_lookupValue('banners', 'bannerid', $id, 'title');?>">
                        </div>
                        <div class="form-group">
                            <label>Content <small class="text-danger">Required</small></label>
                            <textarea name="content" class="form-control"><?php echo graphite_database_lookupValue('banners', 'bannerid', $id, 'content');?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Button Label <small>Leave blank for no button</small></label>
                            <input name="buttonLabel" type="text" class="form-control" value="<?php echo graphite_database_lookupValue('banners', 'bannerid', $id, 'buttonLabel');?>">
                        </div>
                        <div class="form-group">
                            <label>Button URL</label>
                            <input name="buttonUrl" type="text" class="form-control" value="<?php echo graphite_database_lookupValue('banners', 'bannerid', $id, 'buttonUrl');?>">
                        </div>
                        <div class="form-group">
                            <label>Text Align <small class="text-danger">Required</small></label>
                            <select class="form-control" name="textAlign" required>
                                <option value="0" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'textAlign') == 0){echo'selected';}?>>Left</option>
                                <option value="1" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'textAlign') == 1){echo'selected';}?>>Center</option>
                                <option value="2" <?php if(graphite_database_lookupValue('banners', 'bannerid', $id, 'textAlign') == 2){echo'selected';}?>>Right</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="bannerid" value="<?php echo $id;?>">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                            <a href="/admin/banners/delete.php?id=<?php echo $id;?>" class="btn btn-danger">Delete</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        <script>
            function setDates(){
                document.getElementById('endDate').min = document.getElementById('startDate').value;
                document.getElementById('startDate').max = document.getElementById('endDate').value;
            }
            setDates();
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>
