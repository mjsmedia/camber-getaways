<?php
function graphite_uploadToS3($target, $destination){
    $accessKey = $GLOBALS[GENERAL_SETTINGS]->get('aws', 'accessKey');
    $secretKey = $GLOBALS[GENERAL_SETTINGS]->get('aws', 'secretKey');
    $graphiteSiteID = $GLOBALS[GENERAL_SETTINGS]->get('aws', 'graphiteSiteID');
    
    if(empty($accessKey)){
        die("Fatal Error: AWS S3 is not configured!");
    }else if(empty($secretKey)){
        die("Fatal Error: AWS S3 is not configured!");
    }else if(empty($graphiteSiteID)){
        die("Fatal Error: AWS S3 is not configured!");
    }else{
        if (!defined('awsAccessKey')) define('awsAccessKey', $accessKey);
        if (!defined('awsSecretKey')) define('awsSecretKey', $secretKey);
        
        $s3 = new S3(awsAccessKey, awsSecretKey);
        
        if ($s3->putObjectFile($target, 'cdn.mjsmedia.co.uk', $graphiteSiteID.$destination, S3::ACL_PUBLIC_READ)) {
            return true;
        }else{
            return false;
        }
    }
}

function graphite_optimiseImage($target, $width, $height, $quality, $mode, $destination){
    $resizeObj = new resize($target);
    $resizeObj -> resizeImage($width, $height, $mode);
    $resizeObj -> saveImage($destination, $quality);
}

function graphite_getImagePath(){
    if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
        $graphiteSiteID = $GLOBALS[GENERAL_SETTINGS]->get('aws', 'graphiteSiteID');
        $requestUrl = $GLOBALS[GENERAL_SETTINGS]->get('aws', 'requestUrl');
        return $requestUrl.$graphiteSiteID.'/';
    }else{
        return '/images/';
    }
}

function graphite_createImage($createdBy){
    $imageid = graphite_randomString();
    
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO images (imageid,createdBy)
        VALUES ('$imageid','$createdBy');";

    if ($conn->query($sql) === TRUE) {
        return $imageid;
    } else {
        return false;
    }

    mysqli_close($conn);
    
}
?>