<?php
function graphite_albums_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from albums ORDER BY id DESC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        if(empty(graphite_albums_latestImage($row[albumid]))){
            $thumbnail = '/admin/assets/New-Album.png';
        }else{
            $thumbnail = graphite_getImagePath().'gallery/'.graphite_albums_latestImage($row[albumid]).'-graphitethumb.jpg';
        }
        echo'
        <div class="col-6 col-md-3 text-center" style="margin-bottom:30px;">
            <a href="album/?id='.$row[albumid].'">
                <div class="albumImage">
                    <img src="'.$thumbnail.'" class="rounded" alt="..." style="width:100%">
                    <p>'.$row[name].'</p>
                </div>
            </a>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="col-12">
            <div class="card">
                <div class="card-block text-muted">
                    No albums to display.
                </div>
            </div>
        </div>
        ';
    }
}

function graphite_albums_images($albumid){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from albums_images WHERE albumid = '$albumid' ORDER BY id DESC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
            $imagePath = graphite_getImagePath().'gallery/'.$row[imageid].'.jpg';
        }else{
            $imagePath = $GLOBALS[currentUrl].'/images/gallery/'.$row[imageid].'.jpg';
        }
        $count ++;
        echo'
        <div class="col-6 col-md-3 text-center" style="margin-bottom:30px;">
            <a href="#" data-toggle="modal" data-target=".'.$row[imageid].'">
                <div class="albumImage">
                    <img src="'.graphite_getImagePath().'gallery/'.$row[imageid].'-graphitethumb.jpg" class="rounded" alt="..." style="width:100%">
                </div>
            </a>
        </div>
        
        <div class="modal fade '.$row[imageid].'" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                    <div class="row">
                            <div class="col-12 text-center">
                                <img src="'.graphite_getImagePath().'gallery/'.$row[imageid].'.jpg" class="rounded" alt="..." style="width:100%">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-4">
                                <a target="_blank" href="'.$imagePath.'" class="btn btn-primary" style="width:100%;">View</a>
                            </div>
                            <div class="col-4">
                                <a href="deleteimage.php?id='.$row[imageid].'" class="btn btn-danger" style="width:100%;">Delete</a>
                            </div>
                            <div class="col-4">
                                <a href="#" data-dismiss="modal" class="btn btn-secondary" style="width:100%;">Close</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        ';
    }
}

function graphite_albums_latestImage($albumid){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from albums_images WHERE albumid = '$albumid' ORDER BY id ASC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        $imageid = $row[imageid];
    }
    mysqli_close($conn);
    
    return $imageid;
}

function graphite_gallery_listCode(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from albums";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        echo'
        <li class="list-group-item flex-column align-items-start">
            <h5 class="mb-1">'.$row[name].'</h5>
            <p class="mb-0"><code>&lt;?php graphite_gallery(&#39;'.$row[albumid].'&#39;);?&gt;</code></p>
        </li>
        ';
        
    }
    mysqli_close($conn);
}

function graphite_gallery_processImages($fileToRename, $imageid){
    $imageWidth = $GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'imageWidth');
    $imageHeight = $GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'imageHeight');
    if($GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'imageType') == 'exact'){$imageType = 'crop';}else{$imageType = 'auto';};
    $imageQuality = $GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'imageQuality');
    
    $thumbnailWidth = $GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'thumbnailWidth');
    $thumbnailHeight = $GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'thumbnailHeight');
    if($GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'thumbnailType') == 'exact'){$thumbnailType = 'crop';}else{$thumbnailType = 'auto';};
    $thumbnailQuality = $GLOBALS[GRAPHITE_SETTINGS]->get('gallery', 'thumbnailQuality');
    
    graphite_optimiseImage($GLOBALS[ROOTLOCATION]."../images/gallery/".$fileToRename, 400, 400, 20, 'crop', $GLOBALS[ROOTLOCATION]."../images/gallery/".$imageid.'-graphitethumb.jpg');
            
    graphite_optimiseImage($GLOBALS[ROOTLOCATION]."../images/gallery/".$fileToRename, $imageWidth, $imageHeight, $imageQuality, $imageType, $GLOBALS[ROOTLOCATION]."../images/gallery/".$imageid.'.jpg');
    
    graphite_optimiseImage($GLOBALS[ROOTLOCATION]."../images/gallery/".$fileToRename, $thumbnailWidth, $thumbnailHeight, $thumbnailQuality, $thumbnailType, $GLOBALS[ROOTLOCATION]."../images/gallery/".$imageid.'-thumb.jpg');
    
    if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
        graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/gallery/'.$imageid.'-graphitethumb.jpg', '/gallery/'.$imageid.'-graphitethumb.jpg');
        graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/gallery/'.$imageid.'.jpg', '/gallery/'.$imageid.'.jpg');   
        graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/gallery/'.$imageid.'-thumb.jpg', '/gallery/'.$imageid.'-thumb.jpg');   
    }
    
    unlink($GLOBALS[ROOTLOCATION]."../images/gallery/".$fileToRename);
}
?>