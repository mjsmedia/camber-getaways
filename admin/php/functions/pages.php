<?php
function graphite_pages_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from pages";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[name].'</h4>
                <h6 class="card-subtitle mb-2 text-muted">'.$GLOBALS[currentUrl].$row[location].'</h6>
                <a href="edit/?id='.$row[pageid].'" class="card-link">Edit</a>
                <a target="_blank" href="'.$row[location].'" class="card-link">View</a>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card">
            <div class="card-block text-muted">
                No pages to display.
            </div>
        </div>
        ';
    }
}

function graphite_pages_sections($pageid){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from pages_content WHERE pageid = '$pageid'";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){
            $delete = '<a href="deletesection.php?id='.$row[contentid].'" class="card-link">Delete</a>';
            $code = '<p><code>&lt;?php graphite_pageSection(&apos;'.$row[contentid].'&apos;);?&gt;</code></p>';
        }else{
            $delete = '';
            $code = '';
        }
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[name].'</h4>
                '.$code.'
                <a href="editsection.php?id='.$row[contentid].'" class="card-link">Edit</a>
                '.$delete.'
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        ';
    }
}
?>