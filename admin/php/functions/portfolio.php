<?php
function graphite_portfolio_categoryDropdown($category){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from portfolio_categories";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        if($row[categoryid] == $category){$selected = 'selected';}else{$selected = '';}
        echo'
        <option '.$selected.' value="'.$row[categoryid].'">'.$row[name].'</option>
        ';
    }
    mysqli_close($conn);
}

function graphite_portfolio_listPosts($search,$page){
    
    $archiveLocation = $GLOBALS[GRAPHITE_SETTINGS]->get('portfolio', 'archiveLocation');
    
    if(!empty($search)){
        echo '<p style="font-size:15pt;"><span class="badge badge-default">Searching: <em>'.$search.'</em> <a href="?search=" style="color:white;margin-left:5px;"><i class="fa fa-times-circle"></i></a></span></p>';
        $WHERE = "WHERE title LIKE '%".$search."%'";
    }else{
        $WHERE = "";
    }
    
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from portfolio ".$WHERE;
    $result = $conn->query($query);
    $count = 0;

    while($row = $result->fetch_array()){
        $count ++;
    }
    mysqli_close($conn);
    
    $pages = ceil($count / 10);
    
    $OFFSET = ($page - 1) * 10;
    
    for($a = 1; $a <= $pages; $a++){
        if($page == $a){
            $btnclass = 'primary';
        }else{
            $btnclass = 'secondary';
        }
        $pageButtons .= '<a href="?search='.$search.'&page='.$a.'" class="btn btn-'.$btnclass.'">'.$a.'</a>';
    }
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from portfolio ".$WHERE." ORDER BY publishdate DESC LIMIT 10 OFFSET $OFFSET";
    $result = $conn->query($query);
    $count = 0;
    
    while($row = $result->fetch_array()){
        $count ++;
        if(empty($row[category])){
            $category = '';
        }else{
            $category = '<span class="badge badge-default"><i class="fa fa-folder"></i> '.graphite_database_lookupValue('portfolio_categories', 'categoryid', $row[category], 'name').'</span> ';
        }
        
        if($GLOBALS[GRAPHITE_SETTINGS]->get('portfolio', 'author') == 'true'){
            if(empty($row[author])){
                $author = '';
            }else{
                $author = '<span class="badge badge-default"><i class="fa fa-user"></i> '.$row[author].'</span>';
            }
        }else{
            $author = '';
        }
        
        $publishDate = date("d M Y", strtotime($row[publishDate]));
        
        if($row[status] != 1){
            $status = '<span class="badge badge-warning"><i class="fa fa-times"></i> Unpublished</span>';
        }else if(strtotime($row[publishDate]) > strtotime('now')){
            $status = '<span class="badge badge-info"><i class="fas fa-clock"></i> Scheduled</span>';
        }else{
            $status = '<span class="badge badge-success"><i class="fa fa-check"></i> Published</span>';
        }
        
        if(!empty($row[imageid])){
            echo'
            <div class="card" style="margin-bottom:10px;">
                <div class="card-block">
                    <div class="row">
                        <div class="col-12 col-md-9 col-lg-10 text-center text-md-left mb-2">
                            <h4 class="card-title">'.$row[title].'</h4>
                            <h6 class="card-subtitle mb-2 text-muted">'.$status.' '.$category.$author.' <span class="badge badge-default"><i class="fa fa-calendar"></i> '.$publishDate.'</span></h6>
                            <a href="edit/?id='.$row[portfolioid].'" class="card-link">Edit</a>
                            <a href="#" onclick="deletePost('."'".$row[portfolioid]."'".')" class="card-link">Delete</a>
                            <a href="'.$archiveLocation.$row[slug].'" target="_blank" class="card-link">View</a>
                        </div>
                        <div class="col-12 col-md-3 col-lg-2">
                            <img src="/images/uploads/'.$row[imageid].'-thumbnail.jpg" class="rounded" style="width:100%">
                        </div>
                    </div>
                </div>
            </div>
            ';
        }else{
            echo'
            <div class="card" style="margin-bottom:10px;">
                <div class="card-block">
                    <h4 class="card-title">'.$row[title].'</h4>
                    <h6 class="card-subtitle mb-2 text-muted">'.$status.' '.$category.$author.' <span class="badge badge-default"><i class="fa fa-calendar"></i> '.$publishDate.'</span></h6>
                    <a href="edit/?id='.$row[portfolioid].'" class="card-link">Edit Post</a>
                    <a href="#" onclick="deletePost('."'".$row[portfolioid]."'".')" class="card-link">Delete</a>
                </div>
            </div>
            ';
        }
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card text-center" style="margin-bottom:10px;">
            <div class="card-block text-muted">
                No posts to display.
            </div>
        </div>
        ';
    }
    
    $prevPage = $_GET[page] - 1;
        $nextPage = $_GET[page] + 1;
        
    if($prevPage < 1){
        $prevButton = '<a href="#" class="btn btn-secondary disabled"><i class="fas fa-angle-left"></i></a>';
    }else{
        $prevButton = '<a href="?search='.$search.'&page='.$prevPage.'" class="btn btn-secondary"><i class="fas fa-angle-left"></i></a>';
    }
        
    if($nextPage > $pages){
        $nextButton = '<a href="#" class="btn btn-secondary disabled"><i class="fa fa-long-arrow-right"></i></a>';
    }else{
        $nextButton = '<a href="?search='.$search.'&page='.$nextPage.'" class="btn btn-secondary"><i class="fa fa-long-arrow-right"></i></a>';
    }
    
    if($pages > 1 && $pages <= 6){
        echo'
        <div class="row" style="margin-top:10px;">
            <div class="col-12 text-center">
                <div class="btn-group">
                    '.$prevButton.'
                    '.$pageButtons.'
                    '.$nextButton.'
                </div>
            </div>
        </div>
        ';
    }else if($pages > 6){
        echo'
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-4">
            <form method="get">
                <div class="input-group">
                    <span class="input-group-btn">'.$prevButton.'</span>
                    <input type="hidden" name="search" value="'.$_GET[search].'">
                    <input type="number" class="form-control" name="page" placeholder="Page Number" value="'.$page.'" min="1" max="'.$pages.'">
                    <span class="input-group-btn"><button class="btn btn-secondary" type=submit>Go</button></span>
                    <span class="input-group-btn">'.$nextButton.'</span>
                </div>
                </form>
            </div>
        </div>
        ';
    }
    echo'
    <div class="row">
        <div class="col-12 text-center">
            <small class="text-muted"><em>'.graphite_database_resultCount('portfolio','','').' Posts, '.$pages.' Pages</em></small>
        </div>
    </div>
    ';
}

function graphite_portfolio_listCode(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from portfolio_categories";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        echo'
        <li class="list-group-item flex-column align-items-start">
            <h5 class="mb-1">'.$row[name].'</h5>
            <p class="mb-0"><code>&lt;?php graphite_portfolioPosts(&#39;'.$row[categoryid].'&#39;);?&gt;</code></p>
        </li>
        ';
        
    }
    mysqli_close($conn);
}

function graphite_portfolio_listCategories(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from portfolio_categories";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[name].'</h4>
                <a href="deletecategory.php?id='.$row[categoryid].'" class="card-link">Delete</a>
            </div>
        </div>
        ';
        
    }
    mysqli_close($conn);
}
?>