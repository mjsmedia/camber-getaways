<?php
function graphite_sliders_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from sliders";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[name].'</h4>
                <a href="edit/?id='.$row[sliderid].'" class="card-link">Edit</a>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card">
            <div class="card-block text-muted">
                No sliders to display.
            </div>
        </div>
        ';
    }
}
function graphite_sliders_slides($sliderid){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from sliders_slides WHERE sliderid='$sliderid'";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
            <div class="row">
                <div class="col-12 col-md-10">
                    <h4 class="card-title">'.$row[title].'</h4>
                    <p class="card-text">'.$row[content].'</p>
                    <a href="slide.php?id='.$row[slideid].'" class="card-link">Edit</a>
                </div>
                <div class="col-12 col-md-2">
                    <img class="rounded w-100" src="/images/uploads/'.$row[imageid].'-thumbnail.jpg">
                </div>
            </div>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card mb-2">
            <div class="card-block text-muted">
                No slides to display.
            </div>
        </div>
        ';
    }
}
?>