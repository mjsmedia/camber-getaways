<?php
function graphite_users_authorise(){
    if(empty($_SESSION['userid'])){
        $_SESSION['userid'] = $_COOKIE['userid'];
        if(empty($_SESSION['userid'])){
            $_SESSION['userid'] = '';
            graphite_notification('warning', 'You must log in to access Graphite.');
            header('Location: /admin/login')
                or die('Error: You are not authorised to view this page.');
        }
    }else{
        if(graphite_database_resultCount('users', 'userid', $_SESSION['userid']) != 1){
            $_SESSION['userid'] = '';
            graphite_SWnotification('error','Unauthorised','Your account no longer exists. Please contact support for help.');
            header('Location: /admin/login')
                or die('Error: Your User ID is Invalid. Please contact your system administrator for help.');
        }
    }
    
    if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'setup') == 0){
        // Create connection
        $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "UPDATE users
        SET setup= 1
        WHERE userid = '$_SESSION[userid]';";

        if ($conn->query($sql) === TRUE) {
            header('Location: /admin/account/welcome');
        } else {
            die("FATAL ERROR");
        }

        mysqli_close($conn);
    }
    graphite_users_updateTimestamp($_SESSION['userid']);
}

function graphite_users_permissionsCheck($requiredPermissions){
    if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') < $requiredPermissions){
        graphite_SWnotification('error','Unauthorised','You do not have the required permissions to do this.');
        header("Location: {$_SERVER['HTTP_REFERER']}")
            or header("Location: /admin")
            or die('Error: You do not have the required permissions to do this.');
    }
}

function graphite_users_updateTimestamp($userid){
    // Create connection
        $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "UPDATE users
        SET lastLogin= NOW()
        WHERE userid = '$userid';";

        if ($conn->query($sql) === TRUE) {
            //Success
        } else {
            //Fail
        }

        mysqli_close($conn);
}
?>
