<?php
function graphite_dateselection_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from dateselector_ranges WHERE enddate >= CURDATE() ORDER BY startdate ASC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[note].'</h4>
                <h6 class="card-subtitle mb-2 text-muted">'.$row[startdate].' - '.$row[enddate].'</h6>
                <a href="edit/?id='.$row[id].'" class="card-link">Edit</a>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card">
            <div class="card-block text-muted">
                No date ranges to display.
            </div>
        </div>
        ';
    }
}
?>