<?php
function graphite_userManagement_listUsers(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from users ORDER BY permissions DESC";
    $result = $conn->query($query);
    $count = 0;

    while($row = $result->fetch_array()){
        $count ++;
        if($row[setup] != 1){$setup = '<span class="badge badge-pill badge-warning">Password Unchanged</span>';}else{$setup = '';}

        switch($row[permissions]){
            case 3:
                $role = '<i class="fas fa-exclamation-circle"></i><i class="fas fa-shield-alt"></i> Super User';
                break;
            case 2:
                $role = '<i class="fas fa-shield-alt"></i> Administrator';
                break;
            case 1:
                $role = '<i class="fa fa-key"></i> Manager';
                break;
            case 0:
                $role = '<i class="fa fa-user"></i> User';
                break;
        }
        
        if($row[permissions] >= 1){
            $editLink = '';
            $textClass = 'text-muted';
        }else{
            $editLink = '<a href="resetPassword.php?id='.$row[userid].'" class="card-link">Reset Password</a> <a href="#" class="card-link" onclick="deleteUser('."'".$row[userid]."'".')">Delete</a>';
            $textClass = '';
        }
        
        if($row[userid] == $_SESSION[userid]){
            $youTag = '<span class="badge badge-pill badge-success">You</span>';
            $editLink = '<a href="/admin/account" class="card-link">Account Settings</a>';
        }else{
            $youTag = '';
        }
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h6 class="card-title"><span class="text-muted">'.$role.'</span> '.$setup.' '.$youTag.'</h6>
                <p class="card-text '.$textClass.'"><strong>'.$row[email].'</strong> '.$row[firstname].' '.$row[lastname].'</p>
                '.$editLink.'
            </div>
        </div>
        ';
        
    }
    mysqli_close($conn);
}
?>