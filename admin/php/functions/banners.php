<?php
function graphite_banners_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from banners WHERE enddate >= CURDATE() ORDER BY startdate ASC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        
        switch($row['type']){
            case 0:
                $typeBadge = '<span class="badge badge-default">Homepage</span> ';
                break;
            case 1:
                $typeBadge = '<span class="badge badge-default">Search</span> ';
                break;
        }
        
        switch($row['style']){
            default:
                $styleBadge = '<span class="badge badge-default">Default</span> ';
                break;
            case 1:
                $styleBadge = '<span class="badge badge-info">Info</span> ';
                break;
            case 2:
                $styleBadge = '<span class="badge badge-danger">Warning</span> ';
                break;
        }
        
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$typeBadge.$styleBadge.$row[title].'</h4>
                <h6 class="card-subtitle mb-2 text-muted">'.$row[startdate].' - '.$row[enddate].'</h6>
                <a href="edit/?id='.$row[bannerid].'" class="card-link">Edit</a>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card">
            <div class="card-block text-muted">
                No banners to display.
            </div>
        </div>
        ';
    }
}
?>