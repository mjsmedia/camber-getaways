<?php
function graphite_settings_listUsers(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from users ORDER BY permissions DESC";
    $result = $conn->query($query);
    $count = 0;

    while($row = $result->fetch_array()){
        $count ++;
        if($row[setup] != 1){
            $setup = '<span class="badge badge-pill badge-warning">Password Unchanged</span>';
            $lastLogin = 'Never';
        }else{
            $setup = '';
            $lastLogin = time_elapsed_string($row[lastLogin]);
        }

        switch($row[permissions]){
            case 3:
                $role = '<i class="fas fa-exclamation-circle"></i><i class="fas fa-shield-alt"></i> Super User';
                break;
            case 2:
                $role = '<i class="fas fa-shield-alt"></i> Administrator';
                break;
            case 1:
                $role = '<i class="fa fa-key"></i> Manager';
                break;
            case 0:
                $role = '<i class="fa fa-user"></i> User';
                break;
        }
        
        if($row[permissions] > 2){
            $editLink = '';
            $textClass = 'text-muted';
        }else{
            $editLink = '<a href="edit?id='.$row[userid].'" class="card-link">Edit</a>';
            $textClass = '';
        }
        
        if($row[userid] == $_SESSION[userid]){
            $youTag = '<span class="badge badge-pill badge-success">You</span>';
            $editLink = '<a href="/admin/account" class="card-link">Account Settings</a>';
            $loginAsLink = '';
        }else{
            $youTag = '';
            $loginAsLink = '<a href="loginAs.php?id='.$row[userid].'" class="card-link">Login</a>';
        }
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h6 class="card-title"><span class="text-muted">'.$role.'</span> '.$setup.' '.$youTag.'</h6>
                <p class="card-text m-0 '.$textClass.'"><strong>'.$row[email].'</strong> '.$row[firstname].' '.$row[lastname].'</p>
                <p class="cart-text mb-2"><small>Last Active: '.$lastLogin.'</small></p>
                '.$editLink.$loginAsLink.'
            </div>
        </div>
        ';
        
    }
    mysqli_close($conn);
}

function graphite_settings_listLog(){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from log ORDER BY logTime DESC";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        echo'
        <tr>
            <td>'.$row[username].'</td>
            <td>'.$row[logTime].'</td>
            <td>'.$row[activity].'</td>
        </tr>
        ';
    }
    mysqli_close($conn);
}

function graphite_settings_listCategories($archive){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from $archive";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        echo'
        <li>'.$row[name].' <a href="delete.php?id='.$row[id].'" class="card-link">Delete</a></li>
        ';
        
    }
    mysqli_close($conn);
}

function graphite_settings_listAlbums($archive){
    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from $archive";
    $result = $conn->query($query);

    while($row = $result->fetch_array()){
        echo'
        <div class="card" style="margin-bottom:10px">
            <div class="card-block">
                <p class="card-text"><strong>'.$row[name].'</strong> | '.$row[slug].' | # Items</p>
                <a href="delete.php?id='.$row[id].'" class="card-link">Delete</a>
            </div>
        </div>
        ';
        
    }
    mysqli_close($conn);
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>