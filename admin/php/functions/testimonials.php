<?php
function graphite_testimonials_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from testimonials ORDER BY testimonialDate DESC";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        $summary = $row[headline];
        if(strlen($summary) > 200){
            $summary = substr($summary,0,200).'...';
        }
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[firstname].' '.$row[lastname].' '.$row[company].' '.$row[customfield].'</h4>
                <h6 class="card-subtitle mb-2 text-muted">'.$row[testimonialDate].'</h6>
                <p class="card-text">'.$summary.'</p>
                <a href="edit/?id='.$row[testimonialid].'" class="card-link">Edit Testimonial</a>
                <a href="delete.php?id='.$row[testimonialid].'" class="card-link">Delete Testimonial</a>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card">
            <div class="card-block text-muted">
                No testimonials to display.
            </div>
        </div>
        ';
    }
}
?>