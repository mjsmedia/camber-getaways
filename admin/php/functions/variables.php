<?php
function graphite_variables_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from variables";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        $summary = $row[value];
        if(strlen($summary) > 200){
            $summary = substr($summary,0,200).'...';
        }
        
        if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){
            $delete = '<a href="delete.php?id='.$row[variableid].'" class="card-link">Delete Variable</a>';
            $code = '<p><code>&lt;?php echo graphite_variable(&apos;'.$row[variableid].'&apos;);?&gt;</code></p>';
        }else{
            $delete = '';
            $code = '';
        }
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[name].'</h4>
                <p class="card-text text-muted m-0">'.$summary.'</p>
                '.$code.'
                <a href="edit/?id='.$row[variableid].'" class="card-link">Edit Variable</a>
                '.$delete.'
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card">
            <div class="card-block text-muted">
                No variables to display.
            </div>
        </div>
        ';
    }
}
?>