<?php
function graphite_database_resultCount($table, $column, $parameter){
    
    if(empty($column)){
        $where = "";
    }else{
        $where = " WHERE $column = '$parameter'";
    }

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from $table".$where;
    $result = $conn->query($query);
    $count = 0;

    while($row = $result->fetch_array()){
        $count ++;
    }
    mysqli_close($conn);

    return $count;
}

function graphite_database_lookupValue($table, $column, $row, $lookup){
    if(is_numeric($row)){
    }else{
        $row = "'".$row."'";
    }

    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $query = "SELECT * from $table WHERE $column = $row";
    $result = $conn->query($query);

    if ($result) {
        while($row = $result->fetch_object()) {
            $value = $row->$lookup;
        }
    } else {
        die("Error: " . $query . "<br>" . $conn->error);
    }

    mysqli_close($conn);

    return $value;
}

function graphite_database_insert($table, $column, $value){
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO $table ($column)
                VALUES ('$value');";

    if ($conn->query($sql) === TRUE) {
        return 1;
    } else {
        die("Error: " . $query . "<br>" . $conn->error);
    }

    mysqli_close($conn);
}

function graphite_item_exists($table,$column,$input,$location){
    if(graphite_database_resultCount($table, $column, $input) < 1){
        graphite_notification('error', 'That item does not exist.');
        header('Location: '.$location)
            or die('Error: The item you tried to view does not exist');
    }
}
?>