<?php
function graphite_caravans_list(){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from caravans";
    $result = $conn->query($query);
    $count = 0;
    while($row = $result->fetch_array()){
        $count ++;
        echo'
        <div class="card" style="margin-bottom:10px;">
            <div class="card-block">
                <h4 class="card-title">'.$row[name].'</h4>
                <a href="edit/?id='.$row[caravanid].'" class="card-link">Edit</a>
            </div>
        </div>
        ';
    }
    mysqli_close($conn);
    
    if($count < 1){
        echo'
        <div class="card">
            <div class="card-block text-muted">
                No caravans to display.
            </div>
        </div>
        ';
    }
}

function graphite_caravans_photosList($caravanid){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from caravans_images WHERE caravanid = '$caravanid' ORDER BY position ASC";
    $result = $conn->query($query);
    while($row = $result->fetch_array()){
        $count ++;
        echo'
        <li id="item-'.$row[caravanImageID].'">
            <div class="row">
                <div class="col-8">
                    <p class="imageTitle">Caption:</p>
                    <p class="imageCaption">'.$row[caption].'</p>
                    <a href="deleteImage.php?id='.$row[caravanImageID].'" class="imageDelete"><i class="fas fa-trash-alt"></i><span> Delete</span></a>
                </div>
                <div class="col-4">
                    <img src="/images/uploads/'.$row[imageid].'-thumbnail.jpg">
                </div>
            </div>
        </li>
        ';
    }
    mysqli_close($conn);
}

function graphite_caravans_updatePhotoPosition($id,$position){
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "UPDATE caravans_images
    SET position= $position
    WHERE caravanImageID = '$id';";

    if ($conn->query($sql) === TRUE) {
        return true;
    } else {
        return false;
    }

    mysqli_close($conn);
}

function getCaravanFeaturedImage($caravanid){
    // Create connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $query = "SELECT * from caravans_images WHERE caravanid = '$caravanid' ORDER BY position ASC LIMIT 1";
    $result = $conn->query($query);

    if ($result) {
        while($row = $result->fetch_object()) {
            $value = $row->imageid;
        }
    } else {
        die("Error: " . $query . "<br>" . $conn->error);
    }

    mysqli_close($conn);

    return $value;
}
?>