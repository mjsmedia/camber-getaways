<?php
error_reporting(-1);
ini_set('error_reporting', E_ALL);
require_once 'Config/Lite.php';
require_once 'aws-s3.php';
require_once 'resize-class.php';
$GENERAL_SETTINGS = new Config_Lite($ROOTLOCATION.'../config/general.ini');
$GRAPHITE_SETTINGS = new Config_Lite($ROOTLOCATION.'../config/graphite.ini');

if($_SERVER['HTTP_HOST'] == 'localhost:8888' || $_SERVER['HTTP_HOST'] == '192.168.1.58:8888'){
    $sqlHOST = $GENERAL_SETTINGS->get('databaselocal', 'host');
    $sqlUSER = $GENERAL_SETTINGS->get('databaselocal', 'user');
    $sqlPASS = $GENERAL_SETTINGS->get('databaselocal', 'pass');
    $sqlDATA = $GENERAL_SETTINGS->get('databaselocal', 'data');
}else{
    $sqlHOST = $GENERAL_SETTINGS->get('database', 'host');
    $sqlUSER = $GENERAL_SETTINGS->get('database', 'user');
    $sqlPASS = $GENERAL_SETTINGS->get('database', 'pass');
    $sqlDATA = $GENERAL_SETTINGS->get('database', 'data');
}

$siteTitle = $GENERAL_SETTINGS->get('general', 'siteTitle');
$enableHTTPS = $GENERAL_SETTINGS->get('general', 'enableHTTPS');
$graphiteDateFormat = $GENERAL_SETTINGS->get('general', 'dateFormat');
$versionGraphite = $GRAPHITE_SETTINGS->get('graphite', 'version');

if($GRAPHITE_SETTINGS->get('graphite', 'installed') != 'true'){
    header("Location: /admin/install")
        or die("FATAL ERROR");
}

//--HTTPS Settings
if($enableHTTPS == 'true'){
    if($_SERVER["HTTPS"] != "on")
    {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
    }
}

$targetUrl = $_SERVER[REQUEST_URI];
$currentUrl = $_SERVER['HTTP_HOST'];
session_start();

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

require_once 'functions.php';
?>