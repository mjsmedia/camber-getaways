<?php
//EMAIL SETTINGS
$logoUrl = ''; //Logo Url
$toAddress = ''; //Send email to this address
$toName = ''; // Company Name
$emailSubject = ''; //Subject of the email
$contactPageReturn = ''; //Return address after sending email, including URL Argument (E.g. /contact?success=true).

$name = $_POST['name'];
$email = $_POST['email'];
$message = $_POST['message'];

$htmlMessage = '
<body style="padding:50px">
<img src="'.$logoUrl.'" style="height:50px">
<hr>
<h3 style="font-family:sans-serif;"><strong>Contact Form Message</strong></h3>
<p style="font-family:sans-serif;"><strong>Sender:</strong> '.$name.' ('.$email.')</p>
<hr>
<p style="font-family:sans-serif;">'.$message.'</p>
</body>
';

require 'PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'mjsmedia.co.uk';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'smtp@mjsmedia.co.uk';                 // SMTP username
$mail->Password = 'A-ss#!c!Xfd7';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom($email, $name);
$mail->addAddress($toAddress, $toName);          // Name is optional
$mail->addBCC('info@mjsmedia.co.uk', 'MJS Media'); //REMOVE THIS LINE WHEN TESTING IS COMPLETE

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = $emailSubject;
$mail->Body    = $htmlMessage;
$mail->AltBody = $message;

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    header('Location: '.$contactPageReturn);
}
?>