<?php
require_once 'functions/database.php';
require_once 'functions/users.php';
require_once 'functions/settings.php';
require_once 'functions/blog.php';
require_once 'functions/testimonials.php';
require_once 'functions/pages.php';
require_once 'functions/gallery.php';
require_once 'functions/portfolio.php';
require_once 'functions/variables.php';
require_once 'functions/user-management.php';
require_once 'functions/files.php';
require_once 'functions/sliders.php';
require_once 'functions/caravans.php';
require_once 'functions/events.php';
require_once 'functions/date-selection.php';
require_once 'functions/banners.php';

function graphite_notification($type, $message){
    $_SESSION['notificationType'] = $type;
    $_SESSION['notificationMessage'] = $message;
}
function graphite_SWnotification($type, $title, $message){
    $_SESSION['SWnotificationType'] = $type;
    $_SESSION['SWnotificationTitle'] = $title;
    $_SESSION['SWnotificationMessage'] = $message;
}

function graphite_clearNotification(){
    $_SESSION['notificationType'] = '';
    $_SESSION['notificationMessage'] = '';
    $_SESSION['SWnotificationType'] = '';
    $_SESSION['SWnotificationTitle'] = '';
    $_SESSION['SWnotificationMessage'] = '';
}

function graphite_timeGreeting(){
    $firstname = graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'firstname');
    if(empty($firstname)){
        $name = '!';
    }else{
        $name = ', '.$firstname.'!';
    }
    
    if (date('H') < 12) {
        $greet = 'Good Morning'.$name;
    }else if (date('H') > 17) {
        $greet = 'Good Evening'.$name;
    }else{
        $greet = 'Good Afternoon'.$name;
    }
    return $greet;
}

function graphite_randomString($length = 11) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function graphite_randomPlaceholderTitle(){
    switch(rand(0,4)){
        case 0:
            return 'A hair-raising headline';
            break;
        case 1:
            return 'An enticing article';
            break;
        case 2:
            return 'A titillating topic';
            break;
        case 3:
            return 'A stimulating subject';
            break;
        case 4:
            return 'An eye-popping post';
            break;
    }
}

function graphite_checkSlug($slug,$table,$tableslug){

    //Init connection
    $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

    // check connection
    if ($conn->connect_errno) {
        die("Connect failed: ".$conn->connect_error);
    }

    $query = "SELECT * from $table WHERE $tableslug = '$slug'";
    $result = $conn->query($query);

    $count = 0;

    while($row = $result->fetch_array()){
        $count ++;
    }
    mysqli_close($conn);

    return $count;
}

function graphite_slugItteration($slug,$table,$tableslug){
    if(graphite_checkSlug($slug,$table,$tableslug) >= 1){
        $slugOK = 0;
        $itteration = 1;
        while($slugOK == 0){
            $newslug = $slug.'-'.$itteration;
            if(graphite_checkSlug($newslug,$table,$tableslug) >= 1){
                $itteration ++;
                $slugOK = 0;
            }else{
                $slug = $newslug;
                $slugOK = 1;
            }
        }
    }
    return $slug;
}

function graphite_prepareSlugString($title){
    $slug = strtolower($title);
    $slug = str_replace('&','and',$slug);
    $slug = str_replace('@','at',$slug);
    $slug = str_replace('!','',$slug);
    $slug = str_replace('£','',$slug);
    $slug = str_replace('$','',$slug);
    $slug = str_replace('%','',$slug);
    $slug = str_replace('^','',$slug);
    $slug = str_replace('*','',$slug);
    $slug = str_replace('(','',$slug);
    $slug = str_replace(')','',$slug);
    $slug = str_replace('"','',$slug);
    $slug = str_replace("'",'',$slug);
    
    $slug = str_replace('    ','-',$slug);
    $slug = str_replace('   ','-',$slug);
    $slug = str_replace('  ','-',$slug);
    $slug = str_replace(' ','-',$slug);
    
    return $slug;
}

function graphite_prepareSlug($title,$table,$tableslug){
    $slug = graphite_prepareSlugString($title);
    $slug = graphite_slugItteration($slug,$table,$tableslug);
    return $slug;
}

function graphite_logActivity($username, $activity){
    
}

function graphite_checkContentActive($type){
    if($GLOBALS[GRAPHITE_SETTINGS]->get($type, 'enabled') != 'true'){
        graphite_notification('warning', 'This content type is not currently available.');
        header('Location: /admin')
            or die("FATAL ERROR");
    }
}

function graphite_replaceEntities($string){
    $string = str_replace("'",'&apos;',$string);
    $string = str_replace('"','&quot;',$string);
    
    return $string;
}

function graphite_checkLock($userid){
    if($GLOBALS[GRAPHITE_SETTINGS]->get('graphite', 'lock') == 'true'){
        if(graphite_database_lookupValue('users', 'userid', $userid, 'permissions') < 5){
            graphite_SWnotification('error', 'Locked', 'Graphite is currently locked, no changes could be made.');
            header("Location: {$_SERVER['HTTP_REFERER']}")
                or die("FATAL ERROR");
        }
    }
}
?>