<?php
error_reporting(0);
require_once 'Config/Lite.php';
$GENERAL_SETTINGS = new Config_Lite($ROOTLOCATION.'../config/general.ini');
$GRAPHITE_SETTINGS = new Config_Lite($ROOTLOCATION.'../config/graphite.ini');

$sqlHOST = $GENERAL_SETTINGS->get('database', 'host');
$sqlUSER = $GENERAL_SETTINGS->get('database', 'user');
$sqlPASS = $GENERAL_SETTINGS->get('database', 'pass');
$sqlDATA = $GENERAL_SETTINGS->get('database', 'data');

$siteTitle = $GENERAL_SETTINGS->get('general', 'siteTitle');

$versionGraphite = $GRAPHITE_SETTINGS->get('graphite', 'version');

if($GRAPHITE_SETTINGS->get('graphite', 'installed') == 'true'){
    header("Location: /admin")
        or die("FATAL ERROR");
}

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

session_start();
session_destroy();
setcookie("userid", '', time()-604800, "/");

function graphite_randomString($length = 11) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>