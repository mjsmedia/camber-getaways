<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkLock($_SESSION['userid']);

$sliderid = graphite_randomString();
$name = $_POST['name'];

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO sliders (sliderid,name)
    VALUES ('$sliderid','$name');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Slider created.');
    header('Location: /admin/sliders/edit/?id='.$sliderid);
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/sliders/');
}

mysqli_close($conn);
?>