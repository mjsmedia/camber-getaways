<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$id = $_GET['id'];
$sliderid = graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'sliderid');

graphite_item_exists('sliders_slides', 'slideid', $id , '/admin/sliders');

$pageTitle = 'Edit Slider';            //Page Title
$pageSlug = 'slider';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-expand"></i> Sliders <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('sliders', 'sliderid',$sliderid, 'name');?></small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('sliders', 'sliderid',$sliderid, 'name');?></h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/sliders/edit/?id=<?php echo $sliderid;?>"><i class="fas fa-angle-left"></i> Back</a>
                    
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <h2>Edit Slide</h2>
            <form action="saveslide.php" method="post" enctype="multipart/form-data" onsubmit="buttonFeedback('submit', 'Saving')">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Title <span class="badge badge-danger">Required</span></label>
                            <input type="text" class="form-control" name="title" required value="<?php echo graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'title');?>">
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea class="form-control" name="content" rows="5"><?php echo graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'content');?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Primary Button Label</label>
                            <input type="text" class="form-control" name="primaryButtonLabel" value="<?php echo graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'primaryButtonLabel');?>">
                        </div>
                        <div class="form-group">
                            <label>Primary Button Link</label>
                            <input type="text" class="form-control" name="primaryButtonLink" value="<?php echo graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'primaryButtonLink');?>">
                        </div>
                        <div class="form-group">
                            <label>Secondary Button Label</label>
                            <input type="text" class="form-control" name="secondaryButtonLabel" value="<?php echo graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'secondaryButtonLabel');?>">
                        </div>
                        <div class="form-group">
                            <label>Secondary Button Link</label>
                            <input type="text" class="form-control" name="secondaryButtonLink" value="<?php echo graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'secondaryButtonLink');?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Background Image <span class="badge badge-danger">Required</span></label>
                            <div class="slim" data-force-size="2000,1200" data-min-size="1280,720" data-max-file-size="8" data-force-type="jpg">
                                <img src="/images/uploads/<?php echo graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'imageid');?>-full.jpg" alt=""/>
                                <input type="file" name="slim[]" accept="image/jpeg, image/png">
                            </div>
                            <small class="form-text text-muted">File must be .jpg or .png</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <input type="hidden" name="sliderid" value="<?php echo $sliderid;?>">
                            <input type="hidden" name="slideid" value="<?php echo $id;?>">
                            <button id="submit" type="submit" class="btn btn-primary">Save Changes</button>
                            <a href="#" onclick="deleteSlider('<?php echo $id;?>')" class="btn btn-danger">Delete Slide</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        
        <script>
            function deleteSlider(slideid){
                swal({
                    title: "Are you sure?",
                    text: "The slide will be perminantly deleted.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location.replace('/admin/sliders/edit/deleteslide.php?id='+slideid);
                    }
                });
            }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>