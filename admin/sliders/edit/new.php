<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$id = $_GET['id'];

graphite_item_exists('sliders', 'sliderid', $id , '/admin/sliders');

$pageTitle = 'Edit Slider';            //Page Title
$pageSlug = 'slider';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-expand"></i> Sliders <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('sliders', 'sliderid', $id, 'name');?></small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('sliders', 'sliderid', $id, 'name');?></h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/sliders/edit/?id=<?php echo $id;?>"><i class="fas fa-angle-left"></i> Back</a>
                    
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <h2>New Slide</h2>
            <form action="createslide.php" method="post" enctype="multipart/form-data" onsubmit="buttonFeedback('submit', 'Creating')">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Title <span class="badge badge-danger">Required</span></label>
                            <input type="text" class="form-control" name="title" required>
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea class="form-control" name="content" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Primary Button Label</label>
                            <input type="text" class="form-control" name="primaryButtonLabel">
                        </div>
                        <div class="form-group">
                            <label>Primary Button Link</label>
                            <input type="text" class="form-control" name="primaryButtonLink">
                        </div>
                        <div class="form-group">
                            <label>Secondary Button Label</label>
                            <input type="text" class="form-control" name="secondaryButtonLabel">
                        </div>
                        <div class="form-group">
                            <label>Secondary Button Link</label>
                            <input type="text" class="form-control" name="secondaryButtonLink">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Background Image <span class="badge badge-danger">Required</span></label>
                            <div class="slim" data-force-size="2000,1200" data-min-size="1280,720" data-max-file-size="8" data-force-type="jpg">
                                <input type="file" name="slim[]" accept="image/jpeg, image/png" required>
                            </div>
                            <small class="form-text text-muted">File must be .jpg or .png</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <input type="hidden" name="sliderid" value="<?php echo $id;?>">
                            <button id="submit" type="submit" class="btn btn-primary">Create Slide</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="modal fade pageSettings" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving Changes')">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Title <small class="text-danger">Required</small></label>
                                        <input type="text" name="name" class="form-control" required value="<?php echo graphite_database_lookupValue('sliders', 'sliderid', $id, 'name');?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="sliderid" value="<?php echo $id;?>">
                            <a href="#"  onclick="deleteSlider('<?php echo $id;?>')" class="btn btn-danger">Delete Page</a>
                            <button id="save" type="submit" class="btn btn-primary">Save Changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        
        <script>
            function deleteSlider(sliderid){
                swal({
                    title: "Are you sure?",
                    text: "The slider will be perminantly deleted.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location.replace('/admin/sliders/edit/delete.php?id='+sliderid);
                    }
                });
            }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>