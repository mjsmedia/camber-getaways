<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$id = $_GET['id'];

graphite_item_exists('sliders', 'sliderid', $id , '/admin/sliders');

$pageTitle = 'Edit Slider';            //Page Title
$pageSlug = 'slider';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-expand"></i> Sliders <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('sliders', 'sliderid', $id, 'name');?></small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('sliders', 'sliderid', $id, 'name');?></h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/sliders"><i class="fas fa-angle-left"></i> All Sliders</a>
                    <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){echo'
                    <a class="btn btn-secondary" href="#" data-toggle="modal" data-target=".pageSettings">Slider Settings <i class="fa fa-cog"></i></a>
                    ';}?>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <?php graphite_sliders_slides($id);?>
                    <a href="new.php?id=<?php echo $id;?>">
                        <div class="card card-inverse" style="margin-bottom:10px; background-color: #333; border-color: #333;">
                            <div class="card-block text-center">
                                <i class="fas fa-plus-square fa-3x"></i>
                                <h4 class="card-title">New Slide</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="modal fade pageSettings" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="save.php" method="post" onsubmit="buttonFeedback('save', 'Saving Changes')">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Title <small class="text-danger">Required</small></label>
                                        <input type="text" name="name" class="form-control" required value="<?php echo graphite_database_lookupValue('sliders', 'sliderid', $id, 'name');?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="sliderid" value="<?php echo $id;?>">
                            <a href="#"  onclick="deleteSlider('<?php echo $id;?>')" class="btn btn-danger">Delete Page</a>
                            <button id="save" type="submit" class="btn btn-primary">Save Changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
        
        <script>
            function deleteSlider(sliderid){
                swal({
                    title: "Are you sure?",
                    text: "The slider will be perminantly deleted.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location.replace('/admin/sliders/edit/delete.php?id='+sliderid);
                    }
                });
            }
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>