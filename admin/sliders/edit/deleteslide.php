<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];
$sliderid = graphite_database_lookupValue('sliders_slides', 'slideid', $id, 'sliderid');



// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM sliders_slides WHERE slideid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_SWnotification('warning', 'Deleted','The Slide has been deleted.');
    header("Location: /admin/sliders/edit/?id=".$sliderid);
} else {
    graphite_SWnotification('error', 'Whoops!','Could not delete slide.');
    header("Location: {$_SERVER['HTTP_REFERER']}")
        or die("Error");
}

mysqli_close($conn);
?>