<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

$sliderid = $_POST['sliderid'];
$slideid = $_POST['slideid'];
$title = $_POST['title'];
$content = str_replace("'","&#39;",$_POST['content']);
$primaryButtonLabel = $_POST['primaryButtonLabel'];
$primaryButtonLink = $_POST['primaryButtonLink'];
$secondaryButtonLabel = $_POST['secondaryButtonLabel'];
$secondaryButtonLink = $_POST['secondaryButtonLink'];


require_once $ROOTLOCATION.'php/Slim/slim.php';
$images = Slim::getImages();

if($images == false){

    $imageid = graphite_database_lookupValue('sliders_slides', 'slideid', $slideid, 'imageid');

}else{
    foreach ($images as $image) {
        $imageid = graphite_createImage('slider');
        if($imageid == false){
            die("Image Processing Error");
        }else{
            $imageName = $imageid.'-full.jpg';
            
            $data = $image['output']['data'];
            Slim::saveFile($data, $imageName, $GLOBALS[ROOTLOCATION].'../images/uploads/', false);
            
            graphite_optimiseImage($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageName, 500, 500, 50, 'crop', $GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-thumbnail.jpg');
            
            if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-full.jpg', '/uploads/'.$imageid.'-full.jpg');
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-thumbnail.jpg', '/uploads/'.$imageid.'-thumbnail.jpg');
            }
        }
        
    }
}



// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE sliders_slides
SET title= '$title', content= '$content', primaryButtonLabel='$primaryButtonLabel', primaryButtonLink='$primaryButtonLink', secondaryButtonLabel='$secondaryButtonLabel', secondaryButtonLink='$secondaryButtonLink', imageid= '$imageid'
WHERE slideid = '$slideid';";

if ($conn->query($sql) === TRUE) {
    graphite_SWnotification('success', 'Yay!', 'Your changes have been saved!');
    header('Location: /admin/sliders/edit/slide.php?id='.$slideid);
} else {
    graphite_SWnotification('error', 'Whoops!','Something went wrong when updating the slide in the database: '.$conn->error);
    header("Location: {$_SERVER['HTTP_REFERER']}")
        or die("Error");
}

mysqli_close($conn);
?>