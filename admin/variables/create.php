<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkContentActive('variables');

graphite_checkLock($_SESSION['userid']);

$variableid = graphite_randomString();
$name = $_POST['name'];
$value = $_POST['value'];

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO variables (variableid,name,value)
    VALUES ('$variableid','$name','$value');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Variable Created.');
    header('Location: /admin/variables/');
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/variables/');
}

mysqli_close($conn);
?>