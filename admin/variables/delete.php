<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(2);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM variables WHERE variableid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'Variable has been deleted.');
    header("Location: /admin/variables");
} else {
    graphite_notification('error', 'Could not delete variable.');
    header("Location: /admin/variables");
}

mysqli_close($conn);
?>