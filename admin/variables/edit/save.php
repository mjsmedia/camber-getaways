<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkContentActive('variables');

graphite_checkLock($_SESSION['userid']);

$variableid = $_POST['variableid'];
$name = $_POST['name'];
$value = $_POST['value'];

if(empty($name)){
    $name = graphite_database_lookupValue('variables', 'variableid', $variableid, 'name');
}

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE variables
SET name= '$name', value= '$value'
WHERE variableid = '$variableid';";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Changes saved.');
    header('Location: /admin/variables/edit/?id='.$variableid);
} else {
    graphite_notification('error', 'An error occured while saving changes.');
    header('Location: /admin/variables/edit/?id='.$variableid)
        or die("FATAL ERROR");
}

mysqli_close($conn);
?>