<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(1);

graphite_checkContentActive('variables');

$pageTitle = 'Variables';            //Page Title
$pageSlug = 'variables';             //Page Slug

$id = $_GET['id'];

graphite_item_exists('variables', 'variableid', $id , '/admin/variables');
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-exchange-alt"></i> Variables <small class="text-muted hidden-lg-up">Edit</small></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 hidden-md-down">
                    <h1>Edit Variable</h1>
                </div>
                <div class="col-12 col-lg-8 hidden-md-down text-right">
                    <a class="btn btn-secondary" href="/admin/variables"><i class="fas fa-angle-left"></i> All Variables</a>
                </div>
            </div>
            <hr>
            <form action="save.php" method="post" onsubmit="buttonFeedback('submit', 'Saving Changes')">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <?php if(graphite_database_lookupValue('users', 'userid', $_SESSION['userid'], 'permissions') >= 2){
                    echo'
                    <div class="form-group">
                        <label>Name <small class="text-muted">Required</small></label>
                        <input type="text" class="form-control" name="name" required value="'.graphite_database_lookupValue('variables', 'variableid', $id, 'name').'">
                    </div>
                    ';}else{echo'
                    <div class="form-group">
                        <label>Name <small class="text-muted">Required</small></label>
                        <input type="text" class="form-control" value="'.graphite_database_lookupValue('variables', 'variableid', $id, 'name').'" disabled>
                    </div>
                    ';}?>
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="form-group">
                        <label>Value</label>
                        <input type="text" class="form-control" name="value" value="<?php echo graphite_database_lookupValue('variables', 'variableid', $id, 'value');?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 text-right">
                    <input type="hidden" name="variableid" value="<?php echo $id;?>">
                    <button id="submit" class="btn btn-primary" type="submit">Save Changes</button>
                </div>
            </div>
            </form>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>
    </body>
</html>
<?php graphite_clearNotification();?>