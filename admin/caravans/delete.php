<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM caravans WHERE caravanid='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'Caravan Deleted');
    header('Location: /admin/caravans');
} else {
    graphite_notification('error', 'Could not delete Caravan.');
    header('Location: /admin/caravans/edit/?id='.$caravanid);
}

mysqli_close($conn);
?>