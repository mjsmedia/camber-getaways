<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

$caravanid = graphite_randomString();
$name = $_POST['name'];

//--

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO caravans (caravanid,name,description,price)
    VALUES ('$caravanid','$name','','');";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Slider created.');
    header('Location: /admin/caravans/edit/?id='.$caravanid);
} else {
    $errorMsg = $conn->error;
    graphite_notification('error', 'An error occured: ' . $errorMsg);
    header('Location: /admin/caravans/');
}

mysqli_close($conn);
?>