<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

graphite_checkLock($_SESSION['userid']);

//---

$id = $_GET['id'];

$caravanid = graphite_database_lookupValue('caravans_images', 'caravanImageID', $id, 'caravanid');

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM caravans_images WHERE caravanImageID='$id'";

if ($conn->query($sql) === TRUE) {
    graphite_notification('warning', 'Photo Deleted.');
    header('Location: /admin/caravans/edit/?id='.$caravanid);
} else {
    graphite_notification('error', 'Photo Deleted.');
    header('Location: /admin/caravans/edit/?id='.$caravanid);
}

mysqli_close($conn);
?>