<?php
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$caravanImageID = graphite_randomString();
$caption = $_POST['caption'];
$caravanid = $_POST['caravanid'];

$position = graphite_database_resultCount('caravans_images', 'caravanid', $caravanid)+1;

require_once $ROOTLOCATION.'php/Slim/slim.php';
$images = Slim::getImages();

if($images == false){

    $imageid = '';

}else{
    foreach ($images as $image) {
        $imageid = graphite_createImage('Caravans');
        if($imageid == false){
            die("Image Processing Error");
        }else{
            $imageName = $imageid.'-full.jpg';
            
            $data = $image['output']['data'];
            Slim::saveFile($data, $imageName, $GLOBALS[ROOTLOCATION].'../images/uploads/', false);
            
            graphite_optimiseImage($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageName, 500, 500, 50, 'crop', $GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-thumbnail.jpg');
            
            if($GLOBALS[GENERAL_SETTINGS]->get('aws', 'enabled') == 'true'){
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-full.jpg', '/uploads/'.$imageid.'-full.jpg');
                graphite_uploadToS3($GLOBALS[ROOTLOCATION].'../images/uploads/'.$imageid.'-thumbnail.jpg', '/uploads/'.$imageid.'-thumbnail.jpg');
            }
        }
        
    }
}

// Create connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO caravans_images (caravanImageID,caravanid,imageid,caption,position)
    VALUES ('$caravanImageID','$caravanid','$imageid','$caption',$position);";

if ($conn->query($sql) === TRUE) {
    graphite_notification('success', 'Photo Uploaded.');
    header('Location: /admin/caravans/edit/?id='.$caravanid);
} else {
    graphite_notification('error', 'An error occured.');
    header('Location: /admin/caravans/edit/?id='.$caravanid);
}

mysqli_close($conn);
?>