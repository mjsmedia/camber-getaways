<?php //PAGE CONFIG
$ROOTLOCATION = '../../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

graphite_users_authorise();
graphite_users_permissionsCheck(0);

$id = $_GET['id'];

$pageTitle = 'Caravans';            //Page Title
$pageSlug = 'caravans';             //Page Slug
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/OWA.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <h4><i class="fas fa-home"></i> Caravans <small class="text-muted hidden-lg-up"><?php echo graphite_database_lookupValue('caravans', 'caravanid', $id, 'name');?></small></h4>
                </div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-12 hidden-md-down">
                    <h1><?php echo graphite_database_lookupValue('caravans', 'caravanid', $id, 'name');?></h1>
                </div>
            </div>
        </div>
        <hr>
        <div class="container mb-4">
            <div class="row">
                <div class="col-12 col-md-6">
                    <form action="save.php" method="post">
                        <div class="form-group">
                            <label>Title</label>
                            <input name="name" type="text" class="form-control" value="<?php echo graphite_database_lookupValue('caravans', 'caravanid', $id, 'name');?>">
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input name="price" type="text" class="form-control" value="<?php echo graphite_database_lookupValue('caravans', 'caravanid', $id, 'price');?>">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control" rows="5"><?php echo graphite_database_lookupValue('caravans', 'caravanid', $id, 'description');?></textarea>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="caravanid" value="<?php echo $id;?>">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                            <a href="/admin/caravans/delete.php?id=<?php echo $id;?>" class="btn btn-danger">Delete</a>
                        </div>
                    </form>
                </div>
                <style>
                    .imageList{
                        margin: 0;
                        padding: 0;
                        list-style: none;
                    }
                    .imageList li{
                        padding: 10px;
                        background-color: #dddddd;
                        color: black;
                        position: relative;
                        border-radius: 5px;
                        margin-bottom: 10px;
                    }
                    .imageList li:hover{
                        cursor: move;
                    }
                    .imageList li:active{
                        box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.2);
                    }
                    .imageList li .imageTitle{
                        margin: 0;
                        font-weight: bold;
                        font-size: 14px;
                        line-height: 14px;
                        margin-bottom: 5px;
                    }
                    .imageList li .imageCaption{
                        margin: 0;
                        margin-bottom: 10px;
                        font-size: 12px;
                        line-height: 12px;
                        white-space:nowrap;
                        overflow: hidden;
                        text-overflow: ellipsis;
                    }
                    .imageList li img{
                        width: 100%;
                        border-radius: 3px;
                    }
                    .imageList li .imageDelete{
                        background-color: #bcbcbc;
                        border-radius: 3px;
                        border: none;
                        position: absolute;
                        bottom: 0px;
                        padding: 5px;
                        font-size: 15px;
                        line-height: 15px;
                        color: black;
                    }
                    .imageList li .imageDelete span{
                        display: none;
                    }
                    .imageList li .imageDelete:hover{
                        color: white;
                        background-color: red;
                    }
                    .imageList li .imageDelete:hover span{
                        display:contents;
                    }
                    .imageList .placeholder{
                        background-color: #fffdc8;
                    }
                </style>
                <div class="col-12 col-md-6">
                    <p><small><em>Click and drag to change the order of images.</em></small></p>
                    <ul class="imageList">
                        <?php graphite_caravans_photosList($id);?>
                    </ul>
                    <button class="btn btn-secondary w-100" data-toggle="modal" data-target="#addPhoto">Add Photo <i class="fas fa-plus-square"></i></button>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addPhoto" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Photo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="newImage.php" method="post"  enctype="multipart/form-data" onsubmit="buttonFeedback('uploadButton', 'Uploading')">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="slim" data-force-size="1920,1080" data-force-type="jpg">
                                    <input type="file" name="slim[]" accept="image/jpeg, image/png">
                                </div>
                                <small class="form-text text-muted">File must be .jpg or .png</small>
                            </div>
                            <div class="form-group">
                                <input type="text" name="caption" class="form-control" placeholder="Caption">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="caravanid" value="<?php echo $id;?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button id="uploadButton" type="submit" class="btn btn-primary">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php include_once $ROOTLOCATION.'includes/notification.php';?>

        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script>
            $( ".imageList" ).sortable({
                placeholder: "placeholder",
                // Cancel the drag when selecting contenteditable items, buttons, or input boxes
                cancel: ":input,button,[contenteditable],.addNew,.modal",
                // Triggered when the user has finished moving a row
                update: function (event, ui) {
                    // sortable() - Creates an array of the elements based on the element's id. 
                    // The element id must be a word separated by a hyphen, underscore, or equal sign. For example, <tr id='item-1'>
                    var data = $(this).sortable('serialize');

                    //alert(data); // Uncomment this to see what data will be sent to the server

                    // AJAX POST to server
                    $.ajax({
                        data: data,
                        type: 'POST',
                        url: 'refreshOrder.php',
                        success: function(response) {
                            //alert(response); // Uncomment this to see the server's response
                        }
                    });

                }
            });
        </script>
    </body>
</html>
<?php graphite_clearNotification();?>