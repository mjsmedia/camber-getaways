function submitForm(){
    var formValid = true;

    var people = document.getElementById('people').value;
    var startDate = document.getElementById('startDate').value;
    var endDate = document.getElementById('endDate').value;

    var SubmitButton = document.getElementById('submit');


    classie.remove( document.querySelector( '#people' ), 'custom-form-invalid' );
    classie.remove( document.querySelector( '#startDate' ), 'custom-form-invalid' );
    classie.remove( document.querySelector( '#endDate' ), 'custom-form-invalid' );

    
    if(startDate != ''){
        if(endDate != ''){
            document.getElementById('searchForm').submit();
        }else{
            classie.add( document.querySelector( '#endDate' ), 'custom-form-invalid' );
            formValid = false;
        }
    }else{
        document.getElementById('searchForm').submit();
    }


    if(formValid === true){
        document.getElementById('searchForm').submit();
    }
}

function homeSearchForm(){
    var startDate = document.getElementById('startDate').value;
    var endDate = document.getElementById('endDate').value;
}