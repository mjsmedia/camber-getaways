function resetForm(){
    document.getElementById('submit').innerHTML = 'Send <i class="fas fa-paper-plane">';
    document.getElementById("contactFormMailchimp").reset();
}
function submitForm(){
    document.getElementById('submit').innerHTML = 'Sending <i class="fas fa-spinner fa-pulse"></i>';

    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var phone = document.getElementById('phone').value;
    var message = document.getElementById('message').value;

    var http = new XMLHttpRequest();
    var url = "/sendcontactmailchimp.php";
    var params = "name="+name+"&email="+email+"&phone="+phone+"&message="+message;
    http.open("POST", url, true);

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            var contactForm = JSON.parse(http.responseText);
            if(contactForm.status == 'success'){
                document.getElementById('submit').innerHTML = 'Sent <i class="fa fa-thumbs-up"></i>';
                swal("Success!", contactForm.response, "success").then((value) => {resetForm()});
            }else if(contactForm.status == 'error'){
                document.getElementById('submit').innerHTML = "Send";
                swal("Whoops!", contactForm.response, "error");
            }
        }
    }
    http.send(params);
}