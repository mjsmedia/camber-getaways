console.log("Search Version: 2019.1");

function countSelectedOptions(){
    var selectedCount = 0;

    $('.optionsCheckbox').each(function(i, obj) {
        if(obj.checked){
            selectedCount ++;
        }
    });

    if(selectedCount == 0){
        document.getElementById('moreOptionsButton').innerHTML = "None Selected";
    }else{
        document.getElementById('moreOptionsButton').innerHTML = selectedCount+" Selected";
    }
}

function showSearchOptions(){
    $('#searchOptions').collapse('show');
    $( "#searchOptionsButton" ).hide();
}
function hideSearchOptions(){
    $('#searchOptions').collapse('hide');
    $( "#searchOptionsButton" ).show();
}

function resetForm(){
    document.getElementById('submit').innerHTML = "Search";
    document.getElementById("contactForm").reset();
}

function basicSubmitForm(){
    var startDate = document.getElementById('startDate').value;
    var endDate = document.getElementById('endDate').value;
    var formValid = true;

    classie.remove( document.querySelector( '#startDate' ), 'custom-form-invalid' );
    classie.remove( document.querySelector( '#endDate' ), 'custom-form-invalid' );


    if(startDate != ''){
        if(endDate != ''){
            formValid = true;
        }else{
            classie.add( document.querySelector( '#endDate' ), 'custom-form-invalid' );
            formValid = false;
        }
    }else{
        formValid = true;
    }


    if(formValid === true){
        document.getElementById('searchForm').submit();
    }
}

function getBanners(startdate,enddate){
    console.log("Updating Banners: "+startdate+" - "+enddate);
    document.getElementById('banners').innerHTML = "";

    var http = new XMLHttpRequest();
    var url = "getbanners.php";
    var params = "startdate="+startdate+"&enddate="+enddate;
    http.open("POST", url, true);

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            var banners = JSON.parse(http.responseText);
            console.log(banners);
            if(banners.status == 'success'){
                var markup = "";
                var bannerItems = banners.results;
                bannerItems.forEach(function(result) {
                    var bannerStyle = result[0];
                    var bannerTitle = result[1];
                    var bannerContent = result[2];

                    switch(bannerStyle){
                        default:
                            var bannerStyle = '';
                            var bannerIcon = '';
                            break;
                        case '1':
                            var bannerStyle = ' banner--info';
                            var bannerIcon = '<i class="fas fa-info-circle"></i> ';
                            break;
                        case '2':
                            var bannerStyle = ' banner--warning';
                            var bannerIcon = '<i class="fas fa-exclamation-triangle"></i> ';
                            break;
                    }

                    markup += '<div class="banner'+bannerStyle+'"><p class="banner__title">'+bannerIcon+bannerTitle+'</p><p class="banner__content">'+bannerContent+'</p></div>';

                    /*
                    switch($row['style']){
            default:
                $bannerStyle = '';
                $bannerIcon = '';
                break;
            case 1:
                $bannerStyle = ' banner--info';
                $bannerIcon = '<i class="fas fa-info-circle"></i> ';
                break;
            case 2:
                $bannerStyle = ' banner--warning';
                $bannerIcon = '<i class="fas fa-exclamation-triangle"></i> ';
                break;
        }*/
                });
                document.getElementById('banners').innerHTML = markup;
            }
        }
    }
    http.send(params);
}

function submitFormGeneral(submitType){

    if(submitType == ''){
        submitType = 'open';
    }
    var formValid = true;

    var people = document.getElementById('people').value;
    var startDate = document.getElementById('startDate').value;
    var endDate = document.getElementById('endDate').value;

    var sortBy = document.getElementById('sortBy').value;

    var SubmitButton = document.getElementById('submit');

    var options = '';

    $(".optionsCheckbox").each(function( index ){
        if(this.checked){
            var optionID = this.id;
            console.log(optionID);
            options += '&'+optionID+'=1';
        }
    });

    classie.remove( document.querySelector( '#people' ), 'custom-form-invalid' );
    classie.remove( document.querySelector( '#startDate' ), 'custom-form-invalid' );
    classie.remove( document.querySelector( '#endDate' ), 'custom-form-invalid' );

    if(startDate != ''){
        if(endDate == ''){
            classie.add( document.querySelector( '#endDate' ), 'custom-form-invalid' );
            formValid = false;
        }
    }

    if(formValid === true){
        document.getElementById('properties').innerHTML = "";
        document.getElementById('propertyCount').innerHTML = "";
        SubmitButton.innerHTML = 'Searching... <i class="fas fa-spinner fa-pulse"></i>';

        getBanners(startDate,endDate);


        $("#people").prop('disabled', true);
        $("#startDate").prop('disabled', true);
        $("#endDate").prop('disabled', true);
        $("#sortBy").prop('disabled', true);
        $(".optionsCheckbox").prop('disabled', true);

        $( "#progress" ).show();

        var http = new XMLHttpRequest();
        var url = "searchProperties.php";
        var params = "submitType="+submitType+"&people="+people+"&startDate="+startDate+"&endDate="+endDate+options+"&sortBy="+sortBy;
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var properties = JSON.parse(http.responseText);

                if(properties.status == 'success'){
                    SubmitButton.innerHTML = 'Search <i class="fas fa-search"></i>';
                    var propertyResults = properties.results;
                    var markup = "";
                    var countMarkup = "";
                    var count = 0;
                    var nights = properties.numberNights;
                    propertyResults.forEach(function(result) {
                        count ++;
                        var propertyName = result[0];
                        var propertySleeps = result[1];
                        var propertyPhotoCount = result[2];
                        var propertyLink = result[3];
                        var propertyPrice = result[4];
                        var propertyTypeID = result[5];
                        var propertyPets = result[6];
                        var propertyBeds = result[7];
                        var propertyDiscount = result[8];
                        var propertyPhoto1 = result[9];
                        var propertyPhoto2 = result[10];
                        var propertyPhoto3 = result[11];

                        if(startDate !== ''){
                            propertyLink = propertyLink+'?start='+startDate+'&end='+endDate;
                        }

                        if(propertyTypeID == '7580' || propertyTypeID == '7581'){
                            propertyLink = '/holiday-homes/'+propertyLink;
                        }else{
                            propertyLink = '/holiday-park/'+propertyLink;
                        }

                        if(propertyPets == 1){
                            var petsIcon = '<li><i class="fas fa-paw"></i></li>';
                        }else{
                            var petsIcon = '';
                        }

                        if(startDate != '' && endDate != ''){
                            if(propertyDiscount != ''){
                                if(propertyDiscount === propertyPrice){
                                    var priceLabel = 'From £'+propertyDiscount+' For '+nights+' nights';
                                    var discountTag = '';
                                }else{
                                    var priceLabel = 'From <span class="strike">£'+propertyPrice+'</span> For '+nights+' nights';
                                    var discountTag = '<div class="infobadge infobadge-discount">£'+propertyDiscount+'</div>';
                                }
                            }else{
                                var priceLabel = 'From £'+propertyPrice+' For '+nights+' nights';
                                var discountTag = '';
                            }
                        }else{
                            var priceLabel = 'From £'+propertyPrice;
                            var discountTag = '';
                        }



                        if(propertyPhotoCount >= 3){
                            markup += '<div class="col-12 col-md-6 col-xl-4 animated pulse"><a href="'+propertyLink+'"><div class="property"><div class="main-img" style="background: url('+propertyPhoto1+')"></div><div class="second-img" style="background: url('+propertyPhoto2+')"></div><div class="third-img" style="background: url('+propertyPhoto3+')"></div><div class="infobadge infobadge-price">'+priceLabel+'</div>'+discountTag+'<div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBeds+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';

                        }else{
                            markup += '<div class="col-12 col-md-6 col-xl-4 animated pulse"><a href="'+propertyLink+'"><div class="property" style="background-image:url('+propertyPhoto1+')"><div class="infobadge infobadge-price">'+priceLabel+'</div>'+discountTag+'<div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBeds+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';
                        }

                    });
                    if(count == 0){
                        countMarkup += '<p class="lead mb-4">No results could be found.</p><p class="m-0"><strong>Minimum stay for Holiday Park: 2 Nights.</strong></p><p class="m-0"><strong>Minimum stay for Holiday Houses: 3 Nights.</strong></p>'
                    }else{
                        countMarkup += '<p class="lead m-0">Found '+count+' results.</p>'
                    }
                    document.getElementById('properties').innerHTML = markup;
                    document.getElementById('propertyCount').innerHTML = countMarkup;

                    $("#people").prop('disabled', false);
                    $("#startDate").prop('disabled', false);
                    $("#endDate").prop('disabled', false);
                    $("#sortBy").prop('disabled', false);
                    $(".optionsCheckbox").prop('disabled', false);

                    $( "#progress" ).hide();


                }else if(properties.status == 'error'){
                    swal("Whoops!", properties.errorResponse, "error");
                    document.getElementById('submit').innerHTML = 'Search <i class="fas fa-search"></i>';
                    $( "#progress" ).hide();
                    document.getElementById('propertyCount').innerHTML = '<p class="lead m-0">An error occured.</p>';
                }
            }
        }
        http.send(params);
    }
}

function specificSearch(){

    var query = document.getElementById('query').value;
    var formValid = true;
    var submitbuttonspecific = document.getElementById('submitbuttonspecific');
    document.getElementById('propertyResults').innerHTML = '';
    var markup = '';
    document.getElementById('resultQuery').innerHTML = query;

    classie.remove( document.querySelector( '#query' ), 'custom-form-invalid' );


    if(query == ''){
        classie.add( document.querySelector( '#query' ), 'custom-form-invalid' );
        formValid = false;
    }else if(query.length < 3){
        classie.add( document.querySelector( '#query' ), 'custom-form-invalid' );
        formValid = false;
        swal("Not Quite...", "Your search query isn't long enough, try adding more letters or numbers.", "error");
    }else{
        formValid = true;
    }


    if(formValid === true){

        submitbuttonspecific.innerHTML = 'Searching... <i class="fas fa-spinner fa-pulse"></i>';
        $( "#spinner" ).show();
        $('#specificSearchResults').modal('show');

        var http = new XMLHttpRequest();
        var url = "specificSearch.php";
        var params = "query="+query;
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        http.onreadystatechange = function() {//Call a function when the state changes.

            if(http.readyState == 4 && http.status == 200) {
                submitbuttonspecific.innerHTML = 'Find';
                var JSONResponse = JSON.parse(http.responseText);
                if(JSONResponse.status == 'success'){

                    if(JSONResponse.count == 1){
                        window.location.href = "/holiday-park/"+JSONResponse.singleResultCode;
                    }else if(JSONResponse.count == 0){
                        swal("No Results", "Unfortunately we couldn't find what you are looking for.", "error");
                    }else{
                        var propertyResults = JSONResponse.results;
                        propertyResults.forEach(function(result) {

                            var propertyCode = result[0];
                            var propertyName = result[1];
                            var propertyPhotoCount = result[2];
                            var propertyBedrooms = result[3];
                            var propertySleeps = result[4];
                            var propertyPets = result[5];
                            var propertyPhoto1 = result[6];
                            var propertyPhoto2 = result[7];
                            var propertyPhoto3 = result[8];

                            if(propertyPets == 1){
                                var petsIcon = '<li><i class="fas fa-paw"></i></li>';
                            }else{
                                var petsIcon = '';
                            }



                            if(propertyPhotoCount >= 3){
                                markup += '<div class="col-12 col-md-6"><a href="/holiday-park/'+propertyCode+'"><div class="property"><div class="main-img" style="background: url('+propertyPhoto1+')"></div><div class="second-img" style="background: url('+propertyPhoto2+')"></div><div class="third-img" style="background: url('+propertyPhoto3+')"></div><div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBedrooms+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';

                            }else{
                                markup += '<div class="col-12 col-md-6"><a href="/holiday-park/'+propertyCode+'"><div class="property" style="background-image:url('+propertyPhoto1+')"><div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBedrooms+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';
                            }

                        });

                        document.getElementById('propertyResults').innerHTML = markup;
                        $( "#spinner" ).hide();
                        $('#specificSearchResults').modal('show');
                    }

                }else if(JSONResponse.status == 'error'){
                    swal("Whoops!", JSONResponse.errorResponse, "error");
                }
            }
        }
        http.send(params);
    }else{
        console.warn("Form Invalid");
    }
}

function submitFormCollection(collection){
    $( "#progress" ).show();
    var http = new XMLHttpRequest();
    var url = "/search/searchProperties.php";
    var params = "collection="+collection;
    http.open("POST", url, true);

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            var properties = JSON.parse(http.responseText);

            if(properties.status == 'success'){
                var propertyResults = properties.results;
                var markup = "";
                var countMarkup = "";
                var count = 0;
                var nights = properties.numberNights;
                propertyResults.forEach(function(result) {
                    count ++;
                    var propertyName = result[0];
                    var propertySleeps = result[1];
                    var propertyPhotoCount = result[2];
                    var propertyLink = result[3];
                    var propertyPrice = result[4];
                    var propertyTypeID = result[5];
                    var propertyPets = result[6];
                    var propertyBeds = result[7];
                    var propertyDiscount = result[8];
                    var propertyPhoto1 = result[9];
                    var propertyPhoto2 = result[10];
                    var propertyPhoto3 = result[11];


                    if(propertyTypeID == '7580' || propertyTypeID == '7581'){
                        propertyLink = '/holiday-homes/'+propertyLink;
                    }else{
                        propertyLink = '/holiday-park/'+propertyLink;
                    }

                    if(propertyPets == 1){
                        var petsIcon = '<li><i class="fas fa-paw"></i></li>';
                    }else{
                        var petsIcon = '';
                    }


                    var priceLabel = 'From £'+propertyPrice;
                    var discountTag = '';


                    if(collection == 'premier'){
                        if(propertyPhotoCount >= 3){
                            markup += '<div class="col-12 col-md-6"><a href="'+propertyLink+'"><div class="property-premier"><div class="main-img" style="background: url('+propertyPhoto1+')"></div><div class="second-img" style="background: url('+propertyPhoto2+')"></div><div class="third-img" style="background: url('+propertyPhoto3+')"></div><div class="infobadge infobadge-price">'+priceLabel+'</div>'+discountTag+'<div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBeds+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';

                        }else{
                            markup += '<div class="col-12 col-md-6"><a href="'+propertyLink+'"><div class="property-premier" style="background-image:url('+propertyPhoto1+')"><div class="infobadge infobadge-price">'+priceLabel+'</div>'+discountTag+'<div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBeds+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';
                        }
                    }else if(collection == 'essentials'){
                        if(propertyPhotoCount >= 3){
                            markup += '<div class="col-12 col-md-6 col-xl-4 animated pulse"><a href="'+propertyLink+'"><div class="property"><div class="main-img" style="background: url('+propertyPhoto1+')"></div><div class="second-img" style="background: url('+propertyPhoto2+')"></div><div class="third-img" style="background: url('+propertyPhoto3+')"></div><div class="infobadge infobadge-price">'+priceLabel+'</div>'+discountTag+'<div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBeds+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';

                        }else{
                            markup += '<div class="col-12 col-md-6 col-xl-4 animated pulse"><a href="'+propertyLink+'"><div class="property" style="background-image:url('+propertyPhoto1+')"><div class="infobadge infobadge-price">'+priceLabel+'</div>'+discountTag+'<div class="details"><p class="name">'+propertyName+'</p><ul class="details-icons"><li><i class="fas fa-male"></i> '+propertySleeps+'</li><li><i class="fas fa-bed"></i> '+propertyBeds+'</li><li><i class="fas fa-camera"></i> '+propertyPhotoCount+'</li>'+petsIcon+'</ul></div></div></a></div>';
                        }
                    }

                });
                if(count == 0){
                    countMarkup += '<p class="lead m-0">No results could be found.</p>'
                }else{
                    countMarkup += '<p class="lead m-0">Found '+count+' results.</p>'
                }
                document.getElementById('properties').innerHTML = markup;


                $( "#progress" ).hide();


            }else if(properties.status == 'error'){
                swal("Whoops!", properties.errorResponse, "error");
                document.getElementById('submit').innerHTML = 'Search <i class="fas fa-search"></i>';
                $( "#progress" ).hide();
                document.getElementById('propertyCount').innerHTML = '<p class="lead m-0">An error occured.</p>';
            }
        }
    }
    http.send(params);
}
