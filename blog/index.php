<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Blog';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'The latest news from the team at Camber Getaways.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.0),rgba(0, 0, 0, 0.0)),url(/images/camber4.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Our Blog</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <?php graphite_blogPosts('');?>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php
        graphite_GraphiteFooter_AddItem('New Post','blog/new/');
        graphite_GraphiteFooter();
        ?>
    </body>
</html>