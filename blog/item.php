<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = graphite_blogPostValue($_GET['postid'],'title').' - Blog';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = false;

if(empty(graphite_blogPostValue($_GET['postid'],'blogid'))){
    include $ROOTLOCATION.'404.shtml';
    exit or die("Error: 404 Not Found");
}

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = graphite_blogPostValue($_GET['postid'],'metaImageURL');      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = graphite_blogPostValue($_GET['postid'],'metaDescription');   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(<?php echo graphite_blogPostValue($_GET['postid'],'featuredImage');?>);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center">
                        <h1 class="postTitle"><?php echo graphite_blogPostValue($_GET['postid'],'title');?></h1>
                        <p class="postDate"><?php echo graphite_blogPostValue($_GET['postid'],'publishDate');?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <?php echo graphite_blogPostValue($_GET['postid'],'content');?>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php
        graphite_GraphiteFooter_AddItem('New Post','blog/new/');
        graphite_GraphiteFooter_AddItem('Edit Post','blog/edit/?id='.graphite_blogPostValue($_GET['postid'],'blogid'));
        graphite_GraphiteFooter();
        ?>
    </body>
</html>