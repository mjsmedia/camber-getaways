<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'The Premier Collection';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = false;
if($_GET['c'] == 1){
    $classyMode = 'large';
}else{
    $classyMode = false;
}

$seoActive = false;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = '';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid search-section premier">
            <div class="container">
                
                <div class="row">
                    <div class="col-12">
                        <h1>The Premier Collection</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid search-section background-default">
            <div class="container">
                <div class="row mt-4" id="progress">
                    <div class="col-12 text-center">
                        <p class="lead">Searching...</p>
                        <i class="fas fa-spinner fa-pulse fa-3x"></i>
                    </div>
                </div>
                <div class="row" id="properties">
                    
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <style>
            #progress{
                display: none;
            }
        </style>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="/js/searchForm.js?v3"></script>
        <script>
            $(function() {
                submitFormCollection('premier');
            });
        </script>
    </body>
</html>