<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Dog Friendly';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'At Camber Getaways we understand that pets are family, and luckily we have caravans that are dog friendly so your furry friends can join you on your holiday!';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(/images/dogs0.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Dog Friendly</h1>
                        <h3 class="mb-0">We love dogs!</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('XDST02ZQ2FT');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-secondary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('KZGAEUQ5RWI');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="half-half background-blue background-stitch-white">
            <div class="image-half" style="background-image: url(/images/dogs1.jpg)"></div>
            <div class="text-half">
                <div class="container">
                    <?php graphite_pageSection('C5FX58IDQQP');?>
                    <a href="/search/?large-pets=1" class="button m-0">Search Dog Friendly Properties</a>
                    <img src="/images/dogs1.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12">
                        <?php graphite_pageSection('O6ROAE14RU7');?>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
    </body>
</html>