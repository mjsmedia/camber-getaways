<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = '';            //Page Title
$pageTitleOverride = 'Camber Getaways - Luxury Holiday Lets By The Sea';
$pageSlug = 'home';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Camber Sands boasts of 7 miles of golden sand, along with rolling dunes perfect for your UK beach holiday.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-75" style="background: linear-gradient(rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0.15),rgb(255, 255, 255)),url(/images/beach3.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center">
                        <h1>Luxury Holiday Lets <br class="d-md-none">By The Sea</h1>
                        <p class="mb-4">at Camber Sands, East Sussex</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section pt-0 background-default">
            <div class="container section-pullup">
                <div class="row">
                    <div class="col-12">
                        <div class="homeSearch" style="background-image: linear-gradient(to top, #48c6ef 0%, #6f86d6 100%); color:white;">
                            <form id="searchForm" action="/search/process.php" method="post">
                                <input type="hidden" name="type" value="holiday-park">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label>How many people?</label>
                                            <select name="people" id="people" class="form-control custom-form-select">
                                                <option value="">Any</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="col-12 col-md-6 col-lg-3">
                                        <div class="form-group">
                                            <label>What Type?</label>
                                            <select name="type" class="form-control custom-form-select">
                                                <option value="any">Any</option>
                                                <option value="holiday-park">Camber Holiday Park</option>
                                                <option value="holiday-homes">Holiday Houses</option>
                                            </select>
                                        </div>
                                    </div>
                                    -->
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label>Arrival Date?</label>
                                            <input name="startDate" id="startDate" class="form-control custom-form-control datePickerStart" placeholder="Any" onchange="unlockDeparture()">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label>Departure Date?</label>
                                            <input name="endDate" id="endDate" class="form-control custom-form-control datePickerEnd custom-disabled" placeholder="Select Arrival" disabled onchange="validateDeparture()">
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="button" class="button button-search-home mt-3" onclick="submitForm()">Find your Getaway <i class="fas fa-search"></i></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
				<?php if(!empty($GENERAL_SETTINGS->get('homepageadvert', 'imageUrl'))){
					if(!empty($GENERAL_SETTINGS->get('homepageadvert', 'linkUrl'))){?>
					<a href="<?php echo $GENERAL_SETTINGS->get('homepageadvert', 'linkUrl');?>"><img src="<?php echo $GENERAL_SETTINGS->get('homepageadvert', 'imageUrl');?>" alt="" style="width:100%;height:auto;border-radius:10px;margin-bottom:30px;"></a>
				<?php }else{?>
					<img src="<?php echo $GENERAL_SETTINGS->get('homepageadvert', 'imageUrl');?>" alt="" style="width:100%;height:auto;border-radius:10px;margin-bottom:30px;">
				<?php }}?>
                <?php getHomepageBanners();?>
                <div class="row align-items-center" style="padding:30px; margin-left:0px; margin-right:0; margin-bottom:30px; background:#0092d6;color:white;border-radius:10px; ">
                    <div class="col-12 col-lg-6 my-2">
                        <h2 class="mb-0">Subscribe to our mailing list</h2>
                        <p class="mb-0">For Latest News, Offers and more!</p>
                    </div>
                    <div class="col-12 col-lg-6  my-2 text-center text-lg-right">
                        <div id="mc_embed_signup">
                            <form action="https://cambergetaways.us10.list-manage.com/subscribe/post?u=844ff14c1e3a54ac681a558a8&amp;id=33e6f4b52c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="row">
                                        <div class="col-12 col-lg-8 my-2">
                                            <div class="form-group mb-0">
                                                <input type="email" value="" name="EMAIL" class="form-control custom-form-control" id="mce-EMAIL" required autocomplete="email" placeholder="Your Email Address">
                                            </div>
                                            <div id="mce-responses" class="clear">
                                                <div class="response" id="mce-error-response" style="display:none"></div>
                                                <div class="response" id="mce-success-response" style="display:none"></div>
                                            </div>
                                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_844ff14c1e3a54ac681a558a8_4af90fffa8" tabindex="-1" value=""></div>
                                        </div>
                                        <div class="col-12 col-lg-4 my-2">
                                            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button mr-0 mb-0" style="background:white;border:none;color:#0092d6;width:100%;height:54px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
				<!--
                <div class="top-box">
                <div class="row">

                    <div class="col-12 col-md-12 col-lg-4" style="margin-bottom:50px;">
                        <div class="events-box">
                            <div class="title">
                                <p>Upcoming Events</p>
                            </div>
                            <div class="content">
                                <ul class="upcoming-events">
                                    <?php //getNextEvents();?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4" style="margin-bottom:50px;">
                        <div class="events-box">
                            <div class="title">
                                <p>Featured Property</p>
                            </div>
                            <div class="content">
                                <?php //getFeaturedProperty($GRAPHITE_SETTINGS->get('custom', 'featuredPropertyID'),$GRAPHITE_SETTINGS->get('custom', 'featuredPropertyTag'));?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4" style="margin-bottom:50px;">
                        <div class="events-box">
                            <div class="title">
                                <p>Last Minute Offers</p>
                            </div>
                            <div class="content">
                                <div class="last-minute-box">
                                    <p class="lead">Looking to get away this weekend?</p>
                                    <p class="mt-4">Browse our last minute offers to see what's available.</p>
                                    <a href="/search/last-minute.php" class="button mt-4">View Offers <i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
			-->
                <!--
                <div class="row" style="margin-bottom:100px;">
                    <div class="col-12 col-md-6 my-2">
                        <a href="/collections/essentials">
                            <div class="collection-box collection-box__essentials" style="background-image: url(/images/essentials.jpg)">
                                <div class="content">
                                    <p class="title">Essentials</p>
                                    <p class="subtitle">Collection</p>
                                    <p class="description">When all you want is somewhere to rest your head and keep costs low.</p>
                                    <p class="action">View Properties <i class="fas fa-long-arrow-alt-right"></i></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 my-2">
                        <a href="/collections/premier">
                            <div class="collection-box collection-box__premier" style="background-image: url(/images/premier.jpg)">
                                <div class="content">
                                    <p class="title">Premier</p>
                                    <p class="subtitle">Collection</p>
                                    <p class="description">When you want something a little special</p>
                                    <p class="action">View Properties <i class="fas fa-long-arrow-alt-right"></i></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-8">
                        <a href="/holiday-park">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/park-dean.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Camber Holiday Park</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Our Family Holiday Park is loaded with fun and entertainment, which any of our guests are welcome to enjoy.  The facilities include four indoor pools, a spa and sauna, an outdoor splash pool, crazy golf, go-karting, zip wire, archery and much more.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a href="/camber-sands">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/camber-sands-2.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Camber Sands</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>With 7 miles of sand and sea, Camber Beach is the perfect day out for all the family. Whether you enjoy a game of hide and seek in the rolling dunes, a swim in the cool sea or just to kick back and relax, Camber is perfect.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <!--
                    <div class="col-12 col-md-6 col-lg-4">
                        <a href="/e-bikes">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/bikes0.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>eBike Hire</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Want to zip about on the local landscape? Hire one of our eBikes to help you explore the surrounding area or take advantage of the cycling routes.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
-->
                    <div class="col-12 col-md-6 col-lg-4">
                        <a href="/about">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dogs0.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>About us</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Find out about the team that makes the magic happen!</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-8">
                        <a href="https://www.questcottages.co.uk/">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/holiday-homes.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Holiday Houses</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>We have a selection of holiday homes throughout Camber, including the popular area of Whitesands. We also offer accommodation in the beautiful town of Rye, less than 5 minutes from Camber.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-8">
                        <a href="/area">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/camber-map.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Rye and Surrounding Area</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Fancy a spot of sight-seeing? Check out some of the places surrounding Camber Sands.</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a href="/local-attractions/">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/local-attractions.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>Local Attractions</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Lydd Golf Club is a well rated golfing experience with their Clubhouse offering a warm and friendly welcome. Alongside the championship quality 18-hole course, the club offers an 18 hole putting course, perfect for a family day out. </p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!--
                    <div class="col-12 col-md-6 col-lg-4">
                        <a href="/about">
                            <div class="featureBox text-center text-lg-left" style="background-image: url(/images/dogs0.jpg)">
                                <div class="featureBoxOverlay">
                                    <div class="featureBoxTitle">
                                        <p>About us</p>
                                    </div>
                                    <div class="featureBoxContent">
                                        <p>Find out about the team that makes the magic happen!</p>
                                        <p class="more">More Info <i class="fas fa-caret-right"></i></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    -->
                </div>
				<div class="row">
					<div class="col-12 text-center">
						<iframe src="https://rcm-eu.amazon-adsystem.com/e/cm?o=2&p=48&l=ez&f=ifr&linkID=ef118230ccd6f3df64599d7e5e5efa8a&t=cambergetaway-21&tracking_id=cambergetaway-21" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
					</div>
				</div>
            </div>
        </div>
        <div class="half-half background-yellow background-stitch-white">
            <div class="text-half">
                <div class="container">
                    <?php graphite_pageSection('2FZ97PWMTZ7');?>
                    <img src="/images/camber-sands-2.jpg" alt="">
                </div>
            </div>
            <div class="image-half" style="background-image: url(/images/camber-sands-2.jpg)"></div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="/js/simpleSearchForm.js"></script>
    </body>
</html>
