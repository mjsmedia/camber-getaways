<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$results = array();

function produceError($response,$debug){
    $properties->results = '';
    $properties->status = 'error';
    if($_GET['debug'] == 'true'){
        $properties->errorResponse = $response.' ['.$debug.']';
    }else{
        $properties->errorResponse = $response;
    }
    echo json_encode($properties);
    exit;
}

$range = '';

if($_POST['submitType'] == 'holiday-houses'){
    $range = '&typeID_list=7580,7581';
}elseif($_POST['submitType'] == 'holiday-park'){
    if(empty($_POST['caravan']) && empty($_POST['chalet']) && empty($_POST['mini-lodge']) && empty($_POST['full-lodge']) && empty($_POST['holiday-home'])){
        $range = '&typeID_list=7577,7590,7578,7579';
    }else{
        $typeList = array();

        if($_POST['caravan'] == 1){
            array_push($typeList, "7577");
        }
        if($_POST['chalet'] == 1){
            array_push($typeList, "7590");
        }
        if($_POST['mini-lodge'] == 1){
            array_push($typeList, "7578");
        }
        if($_POST['full-lodge'] == 1){
            array_push($typeList, "7579");
        }

        $typeList = implode(',',$typeList);

        $range = '&typeID_list='.$typeList;
    }
}else{
    if(empty($_POST['caravan']) && empty($_POST['chalet']) && empty($_POST['mini-lodge']) && empty($_POST['full-lodge']) && empty($_POST['holiday-home'])){
        $range = '&typeID_list=7577,7590,7578,7579,7581,7580';
    }else{
        $typeList = array();

        if($_POST['caravan'] == 1){
            array_push($typeList, "7577");
        }
        if($_POST['chalet'] == 1){
            array_push($typeList, "7590");
        }
        if($_POST['mini-lodge'] == 1){
            array_push($typeList, "7578");
        }
        if($_POST['full-lodge'] == 1){
            array_push($typeList, "7579");
        }
        if($_POST['holiday-home'] == 1){
            array_push($typeList, "7580");
            array_push($typeList, "7581");
        }

        $typeList = implode(',',$typeList);

        $range = '&typeID_list='.$typeList;
    }
}

if(empty($_POST['people'])){
    $sleeps = '';
}else{
    $sleeps = '&sleeps='.$_POST["people"];
}

if(empty($_POST['startDate'])){
    $startdate = '';
}else{
    $startdate = '&startdate='.$_POST["startDate"];
}

if(empty($_POST['endDate'])){
    $numbernights = '';
    $nights = '';
    $pricerestriction = '';
}else{
    $date1=date_create($_POST['startDate']);
    $date2=date_create($_POST['endDate']);
    $diff=date_diff($date1,$date2);
    $nights = $diff->format("%a");
    $numbernights = '&numbernights='.$nights;
    $pricerestriction = '&no_price_err=1';
}

// IF START DATE ISNT SET
/*if(empty($nightsdate)){
    $numbernights = '';
    $pricerestriction = '';
}else{
    $pricerestriction = '&no_price_err=1';
}*/

if(empty($_POST['startDate']) && empty($_POST['endDate'])){
    if(empty($_POST['sortBy'])){
        $sortBy = '&orderby=minprice_asc';
    }
    switch($_POST['sortBy']){
        case 'priceAsc':
            $sortBy = '&orderby=minprice_asc';
            break;
        case 'priceDesc':
            $sortBy = '&orderby=minprice_desc';
            break;
        case 'sleepsAsc':
            $sortBy = '&orderby=sleeps';
            break;
        case 'sleepsDesc':
            $sortBy = '&orderby=sleeps_desc';
            break;
    }
}else{
    if(empty($_POST['sortBy'])){
        $sortBy = '&orderby=minprice_ratediscount_asc';
    }
    switch($_POST['sortBy']){
        case 'priceAsc':
            $sortBy = '&orderby=minprice_ratediscount_asc';
            break;
        case 'priceDesc':
            $sortBy = '&orderby=minprice_ratediscount_desc';
            break;
        case 'sleepsAsc':
            $sortBy = '&orderby=sleeps';
            break;
        case 'sleepsDesc':
            $sortBy = '&orderby=sleeps_desc';
            break;
    }
}

$options = '';
//External Features
if($_POST['decking'] == 1){
    $options .= '&variableID=184201-1';
}
if($_POST['double-glazing'] == 1){
    $options .= '&variableID=184202-1';
}
if($_POST['hot-tub'] == 1){
    $options .= '&variableID=184200-1';
}
if($_POST['private-parking'] == 1){
    $options .= '&variableID=184199-1';
}
if($_POST['patio'] == 1){
    $options .= '&variableID=184203-1';
}
if($_POST['garden'] == 1){
    $options .= '&variableID=194503-1';
}

//Internal Features
if($_POST['bath'] == 1){
    $options .= '&variableID=184213-1';
}
if($_POST['bedroom-tv'] == 1){
    $options .= '&variableID=184208-1';
}
if($_POST['central-heating'] == 1){
    $options .= '&variableID=184212-1';
}
if($_POST['dishwasher'] == 1){
    $options .= '&variableID=184210-1';
}
if($_POST['en-suite'] == 1){
    $options .= '&variableID=184204-1';
}
if($_POST['second-wc'] == 1){
    $options .= '&variableID=184214-1';
}
if($_POST['sky-tv'] == 1){
    $options .= '&variableID=184207-1';
}
if($_POST['tumble-dryer'] == 1){
    $options .= '&variableID=184211-1';
}
if($_POST['washing-machine'] == 1){
    $options .= '&variableID=184209-1';
}
if($_POST['large-pets'] == 1){
    $options .= '&variableID=184206-1';
}
if($_POST['small-pets'] == 1){
    $options .= '&variableID=184205-1';
}

//Park Location
if($_POST['close-to-entertainment'] == 1){
    $options .= '&variableID=184195-1';
}
if($_POST['close-to-beach'] == 1){
    $options .= '&variableID=184187-1';
}
if($_POST['central'] == 1){
    $options .= '&variableID=184196-1';
}
if($_POST['edge-of-park'] == 1){
    $options .= '&variableID=184197-1';
}
if($_POST['views'] == 1){
    $options .= '&variableID=184198-1';
}
if($_POST['ash-view'] == 1){
    $options .= '&variableID=184193-1';
}
if($_POST['kentish-view'] == 1){
    $options .= '&variableID=184190-1';
}
if($_POST['long-beach-landings'] == 1){
    $options .= '&variableID=184191-1';
}
if($_POST['maple-park-ridge'] == 1){
    $options .= '&variableID=184188-1';
}
if($_POST['south-beach-landings'] == 1){
    $options .= '&variableID=184192-1';
}
if($_POST['sussex-park'] == 1){
    $options .= '&variableID=184189-1';
}
if($_POST['willow-way'] == 1){
    $options .= '&variableID=184194-1';
}

if($_POST['collection'] == 'premier'){
    $xml=simplexml_load_file($apiUrl."filter3.asp?siteID=42986&basic_details=1&grouplist=35176,35129&orderby=minprice_desc") or produceError('Could not connect to property list. E:1','Could not connect to Supercontrol filter3.');
}elseif($_POST['collection'] == 'essentials'){
    $xml=simplexml_load_file($apiUrl."filter3.asp?siteID=42986&basic_details=1&grouplist=35132,35131&orderby=minprice_asc") or produceError('Could not connect to property list. E:1','Could not connect to Supercontrol filter3.');
}else{
    $xml=simplexml_load_file($apiUrl."filter3.asp?siteID=42986&basic_details=1".$pricerestriction.$sleeps.$range.$startdate.$numbernights.$options."&searchtype=AND".$sortBy) or produceError('Could not connect to property list. E:1','Could not connect to Supercontrol filter3.');
    //produceError($apiUrl."filter3.asp?siteID=42986&basic_details=1".$pricerestriction.$sleeps.$range.$startdate.$numbernights.$options."&searchtype=AND".$sortBy,'Could not connect to Supercontrol filter3.');
}

foreach ($xml->property as $element) {
    $propertycode = (string)$element->propertycode;

    if(!empty($element->lastupdate)){
        // Create the DateTime object
        $date = date_create_from_format('d/m/Y H:i:s', $element->lastupdate);
        // Print the date in Y-m-d format.
        $lastupdate = date('Y-m-d H:i:s', $date->getTimestamp());
    }else{
        $lastupdate = date('Y-m-d H:i:s');
    }

    if(graphite_database_resultCount('properties', 'propertycode', $propertycode) < 1){

        /* If the property does not exist in the local database... Add it. */

        $xml2=simplexml_load_file($apiUrl."filter3.asp?siteID=42986&propertycode=".$propertycode) or produceError('Could not connect to property list. E:2','Could not connect to Supercontrol filter3.');
        foreach ($xml2->property as $element2) {
            $typeID = (string)$element2->typename[typeID];
            $propertyname = (string)$element2->propertyname;
            $sleeps = (string)$element2->sleeps;
            $photocount = (string)$element2->photos->photocount;
            $minprice = (string)$element2->minprice;
            $bedrooms = (string)$element2->bedrooms_new;
            $variables = array();

            foreach($element2->propertyvariables->propertyvariable as $variable){
                array_push($variables,$variable);
            }
            $variableids = array();

            foreach ($variables as $variable){
                array_push($variableids, $variable[id]);
            }

            if (in_array('variableID184206', $variableids) || in_array('variableID184205', $variableids)) {
                $pets = 1;
            }else{
                $pets = 0;
            }

            $photocountNum = (int)$photocount;

            if($photocountNum < 1){
                $photo1 = 'none';
                $photo2 = 'none';
                $photo3 = 'none';
            }elseif($photocountNum < 3){
                $photo1 = (string)$element2->photos->img->main[url];
                $photo2 = 'none';
                $photo3 = 'none';
            }else{
                $photosXML = simplexml_load_file($apiUrl."property_main_gallery.asp?id=".$propertycode) or produceError('Could not connect to photo list. E:6','Could not connect to Supercontrol gallery.');
                $count = 1;
                foreach($photosXML->image as $image){
                    switch($count){
                        case 1:
                            $photo1 = (string)$image->url;
                            break;
                        case 2:
                            $photo2 = (string)$image->url;
                            break;
                        case 3:
                            $photo3 = (string)$image->url;
                            break;
                    }
                    $count++;
                    if($count > 3){break;}
                }
            }


            // Create connection
            $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "INSERT INTO properties (propertycode,lastupdate,typeID,propertyname,sleeps,photocount,minprice,beds,pets,photo1,photo2,photo3)
                VALUES ('$propertycode','$lastupdate','$typeID','$propertyname','$sleeps','$photocount','$minprice','$bedrooms','$pets','$photo1','$photo2','$photo3');";

            if ($conn->query($sql) === TRUE) {
                //Success
            } else {
                produceError('Could not process properties. E:4',$conn->error);
            }

            mysqli_close($conn);
        }

        if(empty($startdate) || empty($numbernights)){
            $priceString = number_format($minprice,2);
        }else{
            $price = $element->prices->price->rate;

            $discount = $element->prices->price->discount;

            if($discount == 0){
                $discountPrice = '';
            }else{
                $discountPrice = $price - $discount;
                $discountPrice = number_format($discountPrice,2);
            }

            $priceString = (string)$element->prices->price->rate;
            $priceString = number_format($priceString,2);
        }
        array_push($results,array($propertyname,$sleeps,$photocount,$propertycode,$priceString,$typeID,$pets,$bedrooms,$discountPrice,$photo1,$photo2,$photo3));



    }else{
        if(strtotime($lastupdate) > strtotime(graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'lastupdate'))){

            /* If property exists locally but is outdated */

            $xml2=simplexml_load_file($apiUrl."filter3.asp?siteID=42986&propertycode=".$propertycode) or produceError('Could not connect to property list. E:3','Could not connect to Supercontrol filter3.');
            foreach ($xml2->property as $element2) {
                $typeID = (string)$element2->typename[typeID];
                $propertyname = (string)$element2->propertyname;
                $sleeps = (string)$element2->sleeps;
                $photocount = (string)$element2->photos->photocount;
                $minprice = (string)$element2->minprice;
                $bedrooms = (string)$element2->bedrooms_new;
                $variables = array();

                foreach($element2->propertyvariables->propertyvariable as $variable){
                    array_push($variables,$variable);
                }
                $variableids = array();

                foreach ($variables as $variable){
                    array_push($variableids, $variable[id]);
                }

                if (in_array('variableID184206', $variableids) || in_array('variableID184205', $variableids)) {
                    $pets = 1;
                }else{
                    $pets = 0;
                }

                $photocountNum = (int)$photocount;

                if($photocountNum < 1){
                    $photo1 = 'none';
                    $photo2 = 'none';
                    $photo3 = 'none';
                }elseif($photocountNum < 3){
                    $photo1 = (string)$element2->photos->img->main[url];
                    $photo2 = 'none';
                    $photo3 = 'none';
                }else{
                    $photosXML = simplexml_load_file($apiUrl."property_main_gallery.asp?id=".$propertycode) or produceError('Could not connect to photo list. E:6','Could not connect to Supercontrol gallery.');
                    $count = 1;
                    foreach($photosXML->image as $image){
                        switch($count){
                            case 1:
                                $photo1 = (string)$image->url;
                                break;
                            case 2:
                                $photo2 = (string)$image->url;
                                break;
                            case 3:
                                $photo3 = (string)$image->url;
                                break;
                        }
                        $count++;
                        if($count > 3){break;}
                    }
                }

                // Create connection
                $conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                $sql = "UPDATE properties
                SET typeID= '$typeID', propertyname= '$propertyname', sleeps= '$sleeps', photocount= '$photocount', lastupdate= '$lastupdate', minprice= '$minprice', beds= '$bedrooms', pets= '$pets', photo1= '$photo1', photo2= '$photo2', photo3= '$photo3'
                WHERE propertycode = '$propertycode';";

                if ($conn->query($sql) === TRUE) {
                    //Updated
                } else {
                    produceError('Could not process properties. E:5',$conn->error);
                }

                mysqli_close($conn);
            }

            if(empty($startdate) || empty($numbernights)){
                $priceString = number_format($minprice,2);
            }else{
                $price = $element->prices->price->rate;

                $discount = $element->prices->price->discount;

                if($discount == 0){
                    $discountPrice = '';
                }else{
                    $discountPrice = $price - $discount;
                    $discountPrice = number_format($discountPrice,2);
                }

                $priceString = (string)$element->prices->price->rate;
                $priceString = number_format($priceString,2);
            }
            array_push($results,array($propertyname,$sleeps,$photocount,$propertycode,$priceString,$typeID,$pets,$bedrooms,$discountPrice,$photo1,$photo2,$photo3));
        }else{
            $typeID = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'typeID');
            $propertyname = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'propertyname');
            $shortdescription = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'shortdescription');
            $sleeps = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'sleeps');
            $photocount = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'photocount');
            $pets = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'pets');
            $bedrooms = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'beds');
            $photo1 = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'photo1');
            $photo2 = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'photo2');
            $photo3 = graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'photo3');

            if(empty($startdate) || empty($numbernights)){
                $priceString = number_format(graphite_database_lookupValue('properties', 'propertycode', $propertycode, 'minprice'),2);
            }else{
                $price = $element->prices->price->rate;

                $discount = $element->prices->price->discount;

                if($discount == 0){
                    $discountPrice = '';
                }else{
                    $discountPrice = $price - $discount;
                    $discountPrice = number_format($discountPrice,2);
                }

                $priceString = (string)$element->prices->price->rate;
                $priceString = number_format($priceString,2);
            }
            array_push($results,array($propertyname,$sleeps,$photocount,$propertycode,$priceString,$typeID,$pets,$bedrooms,$discountPrice,$photo1,$photo2,$photo3));
        }
    }
}
$properties->status = 'success';
$properties->numberNights = $nights;
$properties->results = $results;

echo json_encode($properties);
exit;

?>