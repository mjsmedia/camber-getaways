<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Search';            //Page Title
$pageSlug = '';             //Page Slug
$menuTransparent = false;


$seoActive = false;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = '';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid search-section background-blue background-stitch-white" style="transition-duration:1s">
            <div class="container">
                <div class="collapse dont-collapse-desktop" id="searchOptions">
                <form>
                <div class="row justify-content-center mb-4">
                    <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                        <div class="form-group">
                            <label>How many people?</label>
                            <select id="people" class="form-control custom-form-select">
                                <option <?php if($_GET['people'] == ''){echo'selected';}?> value="">Any</option>
                                <option <?php if($_GET['people'] == '1'){echo'selected';}?> value="1">1</option>
                                <option <?php if($_GET['people'] == '2'){echo'selected';}?> value="2">2</option>
                                <option <?php if($_GET['people'] == '3'){echo'selected';}?> value="3">3</option>
                                <option <?php if($_GET['people'] == '4'){echo'selected';}?> value="4">4</option>
                                <option <?php if($_GET['people'] == '5'){echo'selected';}?> value="5">5</option>
                                <option <?php if($_GET['people'] == '6'){echo'selected';}?> value="6">6</option>
                                <option <?php if($_GET['people'] == '7'){echo'selected';}?> value="7">7</option>
                                <option <?php if($_GET['people'] == '8'){echo'selected';}?> value="8">8</option>
                                <option <?php if($_GET['people'] == '9'){echo'selected';}?> value="9">9</option>
                                <option <?php if($_GET['people'] == '10'){echo'selected';}?> value="10">10</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 col-xl-3">
                        <div class="form-group">
                            <label>Arrival Date?</label>
                            <input name="startDate" id="startDate" class="form-control custom-form-control datePickerStart" placeholder="Any" value="<?php echo $_GET['start'];?>" onchange="unlockDeparture()">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 col-xl-3">
                        <div class="form-group">
                            <label>Departure Date?</label>
                            <input name="endDate" id="endDate" class="form-control custom-form-control datePickerEnd" placeholder="Any" value="<?php echo $_GET['end'];?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                            <div class="form-group">
                                <label>Features</label>
                                <button type="button" id="moreOptionsButton" class="form-control custom-form-control" data-toggle="modal" data-target="#moreOptions">None Selected</button>
                            </div>
                        </div>
                    <div class="col-12 col-xl-2 text-center text-xl-right">
                        <button type="button" id="submit" class="button button-search" onclick="submitFormGeneral();hideSearchOptions()">Search <i class="fas fa-search"></i></button>
                    </div>
                </div>
                    <div class="modal" id="moreOptions" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                <button class="custom-modal-close" data-dismiss="modal" onclick="countSelectedOptions()"><i class="fas fa-times"></i></button>
                                    <div class="row">
                                        <div class="col-12 col-lg-4 mb-4">
                                            <h6 class="mb-4">External Features</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Decking
                                                    <input id="decking" class="optionsCheckbox" type="checkbox" name="decking" value="1" <?php if($_GET['decking'] == 1){echo 'checked';}?> onchange="submitFormGeneral()">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Double Glazing
                                                    <input id="double-glazing" class="optionsCheckbox" type="checkbox" name="double-glazing" value="1" <?php if($_GET['double-glazing'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Private Hot Tub
                                                    <input id="hot-tub" class="optionsCheckbox" type="checkbox" name="hot-tub" value="1" <?php if($_GET['hot-tub'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Private Parking
                                                    <input id="private-parking" class="optionsCheckbox" type="checkbox" name="private-parking" value="1" <?php if($_GET['private-parking'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Patio
                                                    <input id="patio" class="optionsCheckbox" type="checkbox" name="patio" value="1" <?php if($_GET['patio'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <h6 class="mb-4 mt-4">Property Type</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Caravan
                                                    <input id="caravan" class="optionsCheckbox" type="checkbox" name="caravan" value="1" <?php if($_GET['caravan'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Chalet
                                                    <input id="chalet" class="optionsCheckbox" type="checkbox" name="chalet" value="1" <?php if($_GET['chalet'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Mini Lodge
                                                    <input id="mini-lodge" class="optionsCheckbox" type="checkbox" name="mini-lodge" value="1" <?php if($_GET['mini-lodge'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Full Lodge
                                                    <input id="full-lodge" class="optionsCheckbox" type="checkbox" name="full-lodge" value="1" <?php if($_GET['full-lodge'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Holiday Home
                                                    <input id="holiday-home" class="optionsCheckbox" type="checkbox" name="holiday-home" value="1" <?php if($_GET['holiday-home'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4 mb-4">
                                            <h6 class="mb-4">Internal Features</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Bath
                                                    <input id="bath" class="optionsCheckbox" type="checkbox" name="bath" value="1" <?php if($_GET['bath'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Bedroom TV
                                                    <input id="bedroom-tv" class="optionsCheckbox" type="checkbox" name="bedroom-tv" value="1" <?php if($_GET['bedroom-tv'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Central Heating
                                                    <input id="central-heating" class="optionsCheckbox" type="checkbox" name="central-heating" value="1" <?php if($_GET['central-heating'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Dishwasher
                                                    <input id="dishwasher" class="optionsCheckbox" type="checkbox" name="dishwasher" value="1" <?php if($_GET['dishwasher'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">En Suite
                                                    <input id="en-suite" class="optionsCheckbox" type="checkbox" name="en-suite" value="1" <?php if($_GET['en-suite'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Second WC
                                                    <input id="second-wc" class="optionsCheckbox" type="checkbox" name="second-wc" value="1" <?php if($_GET['second-wc'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Sky TV
                                                    <input id="sky-tv" class="optionsCheckbox" type="checkbox" name="sky-tv" value="1" <?php if($_GET['sky-tv'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Tumble Dryer
                                                    <input id="tumble-dryer" class="optionsCheckbox" type="checkbox" name="tumble-dryer" value="1" <?php if($_GET['tumble-dryer'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Washing Machine
                                                    <input id="washing-machine" class="optionsCheckbox" type="checkbox" name="washing-machine" value="1" <?php if($_GET['washing-machine'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">1 Small Pet Friendly
                                                    <input id="small-pets" class="optionsCheckbox" type="checkbox" name="small-pets" value="1" <?php if($_GET['small-pets'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">1 or 2 Pets (Any Size) Friendly
                                                    <input id="large-pets" class="optionsCheckbox" type="checkbox" name="large-pets" value="1" <?php if($_GET['large-pets'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>

                                        </div>
                                        <div class="col-12 col-lg-4 mb-4">
                                            <h6 class="mb-4">Location</h6>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Close To Entertainment
                                                    <input id="close-to-entertainment" class="optionsCheckbox" type="checkbox" name="close-to-entertainment" value="1" <?php if($_GET['close-to-entertainment'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Close to Beach
                                                    <input id="close-to-beach" class="optionsCheckbox" type="checkbox" name="close-to-beach" value="1" <?php if($_GET['close-to-beach'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Central
                                                    <input id="central" class="optionsCheckbox" type="checkbox" name="central" value="1" <?php if($_GET['central'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Edge of Park
                                                    <input id="edge-of-park" class="optionsCheckbox" type="checkbox" name="edge-of-park" value="1" <?php if($_GET['edge-of-park'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Views
                                                    <input id="views" class="optionsCheckbox" type="checkbox" name="views" value="1" <?php if($_GET['views'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Ash View
                                                    <input id="ash-view" class="optionsCheckbox" type="checkbox" name="ash-view" value="1" <?php if($_GET['ash-view'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Kentish View
                                                    <input id="kentish-view" class="optionsCheckbox" type="checkbox" name="kentish-view" value="1" <?php if($_GET['kentish-view'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Long Beach Landings
                                                    <input id="long-beach-landings" class="optionsCheckbox" type="checkbox" name="long-beach-landings" value="1" <?php if($_GET['long-beach-landings'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Maple Park / Maple Ridge
                                                    <input id="maple-park-ridge" class="optionsCheckbox" type="checkbox" name="maple-park-ridge" value="1" <?php if($_GET['maple-park-ridge'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">South Beach Landings
                                                    <input id="south-beach-landings" class="optionsCheckbox" type="checkbox" name="south-beach-landings" value="1" <?php if($_GET['south-beach-landings'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Sussex Park
                                                    <input id="sussex-park" class="optionsCheckbox" type="checkbox" name="sussex-park" value="1" <?php if($_GET['sussex-park'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="new-checkbox">
                                                <label class="new-checkbox-container">Willow Way
                                                    <input id="willow-way" class="optionsCheckbox" type="checkbox" name="willow-way" value="1" <?php if($_GET['willow-way'] == 1){echo 'checked';}?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="button button-modal-done" data-dismiss="modal" onclick="countSelectedOptions();submitFormGeneral();hideSearchOptions()">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                    <button id="searchOptionsButton" class="button button-search m-0 w-100" type="button" onclick="showSearchOptions()">Refine Search <i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="row justify-content-center mt-4">
                        <div class="col-12 col-md-4 col-lg-3">
                            <div class="form-group m-0">
                                <select id="sortBy" class="custom-outline-select" onchange="submitFormGeneral()">
                                    <option value="priceAsc">Price: Low to High</option>
                                    <option value="priceDesc">Price: High to Low</option>
                                    <option value="sleepsAsc">Capacity: Low to High</option>
                                    <option value="sleepsDesc">Capacity: High to Low</option>
                                </select>
                            </div>
                        </div>
                    <div class="col-12 col-md-4 col-lg-3">
                            <a href="/search/" class="button" style="height:38px;padding:7px 18px;">View All Properties</a>
                        </div>
                    </div>
            </div>
        </div>
        <div class="container-fluid search-section background-default background-stitch-blue">
            <div class="container">
                <div id="banners">
                <?php getSearchBanners($_GET['start'], $_GET['end']);?>
                </div>
				<div class="row">
					<div class="col-12 text-center">
						<iframe src="https://rcm-eu.amazon-adsystem.com/e/cm?o=2&p=48&l=ez&f=ifr&linkID=ef118230ccd6f3df64599d7e5e5efa8a&t=cambergetaway-21&tracking_id=cambergetaway-21" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
					</div>
				</div>
                <div class="row mt-4" id="progress">
                    <div class="col-12 text-center">
                        <p class="lead">Searching...</p>
                        <i class="fas fa-spinner fa-pulse fa-3x"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center" id="propertyCount"></div>
                </div>
                <div class="row" id="properties">
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <style>
            #progress{
                display: none;
            }
        </style>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="/js/searchForm.js?v6"></script>
        <script>
            $(function() {
                countSelectedOptions();
                submitFormGeneral();
            });
        </script>
    </body>
</html>
