<?php
$query = array();

if(!empty($_POST['people'])){
    array_push($query, 'people='.$_POST['people']);
}

if(!empty($_POST['startDate'])){
    array_push($query, 'start='.$_POST['startDate']);
}

if(!empty($_POST['endDate'])){
    array_push($query, 'end='.$_POST['endDate']);
}

$queryString = implode('&',$query);

if(empty($queryString)){
    header('Location: /search');
}else{
    header('Location: /search?'.$queryString);
}

if($_POST['type'] == 'holiday-homes'){
    if(empty($queryString)){
        header('Location: /search/holiday-houses');
    }else{
        header('Location: /search/holiday-houses?'.$queryString);
    }
}elseif ($_POST['type'] == 'holiday-park') {
    if(empty($queryString)){
        header('Location: /search/holiday-park');
    }else{
        header('Location: /search/holiday-park?'.$queryString);
    }
}else{
    if(empty($queryString)){
        header('Location: /search');
    }else{
        header('Location: /search?'.$queryString);
    }
}

/*

$date1=date_create($_POST['startDate']);
    $date2=date_create($_POST['endDate']);
    $diff=date_diff($date1,$date2);
    $nights = $diff->format("%a");
    */
?>