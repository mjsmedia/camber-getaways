<?php
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$results = array();

if(empty($_POST['startdate']) || empty($_POST['enddate'])){
    
    $banners->status = 'fail';
    echo json_encode($banners);
    exit;
    
}else{
    $searchStartdate = $_POST['startdate'];
    $searchEnddate = $_POST['enddate'];


//Init connection
$conn = new mysqli($GLOBALS['sqlHOST'], $GLOBALS['sqlUSER'], $GLOBALS['sqlPASS'], $GLOBALS['sqlDATA']);

// check connection
if ($conn->connect_errno) {
    die("Connect failed: ".$conn->connect_error);
}

$query = "SELECT * from banners WHERE type = 1 AND (startdate <= '$searchEnddate' AND enddate >= '$searchStartdate') ORDER BY startdate DESC LIMIT 4";
$result = $conn->query($query);
$count = 0;
while($row = $result->fetch_array()){
    $count ++;
    array_push($results,array($row['style'],$row['title'],$row['content']));
}
mysqli_close($conn);

}

$banners->status = 'success';
$banners->results = $results;

echo json_encode($banners);
exit;
?>