<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$query = array();

$nextArrivalDate = nextArrivalDate('current');

$day = date('D',strtotime($nextArrivalDate));

if($day == 'Mon'){
    $nextDepartDate = date("Y-m-d",strtotime($nextArrivalDate."+4 days"));
}else{
    $nextDepartDate = date("Y-m-d",strtotime($nextArrivalDate."+3 days"));
}

array_push($query, 'start='.$nextArrivalDate);

array_push($query, 'end='.$nextDepartDate);

$queryString = implode('&',$query);

header('Location: /search?'.$queryString);
?>