<?php //PAGE CONFIG
$ROOTLOCATION = '../';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';
function errorPage(){
    include $GLOBALS['ROOTLOCATION'].'500.shtml';
    exit or die("Error: 500 Internal Server Error");
}

$propertyID = filter_var(urldecode($_GET['id']),FILTER_SANITIZE_NUMBER_INT);

if(!is_numeric($propertyID)){
	die("Error: 500 Internal Server Error");
}


if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['start'])) {
    $startDate = $_GET['start'];
} else {
    $startDate = '';
}
if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['end'])) {
    $endDate = $_GET['end'];
} else {
    $endDate = '';
}

if(!empty($startDate) && !empty($endDate)){
	$date1=date_create($startDate);
	$date2=date_create($endDate);
	$diff=date_diff($date1,$date2);
	$nights = $diff->format("%a");
	$nightslabel = $nights.' Nights';
}else{
	$nights = 0;
	$nightslabel = '??? Nights';
}

//die($nights);

$day = date("D", strtotime($startDate));

if(empty($startDate) || empty($endDate)){
    $bookNowLink = 'https://secure.supercontrol.co.uk/availability/availability_weekly.asp?ownerID=20618&siteID=42986&cottageID=cottageID_'.$propertyID;
}else{
    $bookNowLink = createBookNowLink($startDate,$nights,$propertyID);
}

$xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/property_xml.asp?siteID=42986&id=".$propertyID."&startdate=".$startDate."&numbernights=".$nights) or errorPage();

if(empty($xml->property->propertycode)){
    include $ROOTLOCATION.'404.shtml';
    exit or die("Error: 404 Not Found");
}

$menuTransparent = false;

if($_GET['c'] == 1){
    $classyMode = 'large';
    $pageSlug = 'holiday-homes';
    $pageTitle = $xml->property->propertyname.' - Holiday Houses';
    
    header("Location: https://questcottages.co.uk/search/".$propertyID);
}else{
    $classyMode = false;
    $pageSlug = 'parkdean';
    $pageTitle = $xml->property->propertyname.' - Camber Holiday Park';
}

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = 'https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$xml->property->photos->img->photoid.'.jpg';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = $xml->property->shortdescription;   //Meta Description



//$bookNowButton = '<a href="'.$bookNowLink.'" class="propertyAction propertyAction-book"><i class="fas fa-shopping-cart"></i> Book Now From £'.$minprice.'</a>';

//$bookNowButton = '<a href="'.$bookNowLink.'" class="propertyAction propertyAction-book"><i class="fas fa-shopping-cart"></i> Book Now From <span class="strike">£'.$originalPrice.'</span> <span class="discountPrice">£'.$discountPrice.'</span></a>';

$getPrices = getPropertyPrice($propertyID,$startDate,$nights);
$originalPrice = $getPrices['originalPrice'];
$discountedPrice = $getPrices['discountedPrice'];
if(empty($startDate) || empty($endDate)){
    $bookNowButton = '<a href="#" data-toggle="modal" data-target="#availabilityModal" class="propertyAction propertyAction-book"><i class="fas fa-shopping-cart"></i> Book Now From £'.$originalPrice.'</a>';
    $openBookingCartModalScript = '';
}else{
    if($originalPrice <= $discountedPrice){
        $bookNowButton = '<a href="#" class="propertyAction propertyAction-book openBookingCartModal"><i class="fas fa-shopping-cart"></i> Book Now From £'.$originalPrice.'</a>';
    }else{
        $bookNowButton = '<a href="#" class="propertyAction propertyAction-book openBookingCartModal"><i class="fas fa-shopping-cart"></i> Book Now From <span class="strike">£'.$originalPrice.'</span> <span class="discountPrice">£'.$discountedPrice.'</span></a>';
    }

    $bookingCartDateStart = date("d/m/Y", strtotime($startDate));
    $bookingCartDateEnd = date("d/m/Y", strtotime($endDate));

    $openBookingCartModalScript = '
    <script>
        $(document).ready(function(){
            $(".openBookingCartModal").click(function(e){
                e.preventDefault();
                cartDetails = {
                    action: "cartOpen",
                    arrivalDate: "'.$bookingCartDateStart.'",
                    departureDate: "'.$bookingCartDateEnd.'",
                    numberNights: '.$nights.',
                    cottageID: '.$propertyID.',
                }
                
                window.postMessage(cartDetails,"*");

            });
        });
    </script>
    ';
}
//$bookNowButton = '<a href="'.$bookNowLink.'" class="propertyAction propertyAction-book"><i class="fas fa-shopping-cart"></i> Book Now From £100.00</a>';
//$bookNowButton = '<a href="'.$bookNowLink.'" class="propertyAction propertyAction-book"><i class="fas fa-shopping-cart"></i> Book Now From <span class="strike">£100.00</span> <span class="discountPrice">£50.00</span></a>';


?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <script src="https://secure.supercontrol.co.uk/components/embed.js"></script>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div id="pageHero" class="container-fluid hero hero-75" style="background: url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/<?php echo $xml->property->photos->img->photoid;?>.jpg); background-position:center!important;">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-12 align-self-center text-center text-md-right d-none d-md-block">
                        <div class="propertyTitleBlock">
                            <h1><?php echo $xml->property->propertyname;?></h1>
                            <p><?php echo $xml->property->propertyaddress;?></p>
                        </div>
                    </div>
                    <div class="col-12 align-self-end text-center text-md-right d-md-none mb-4">
                        <div class="propertyTitleBlock">
                            <h1><?php echo $xml->property->propertyname;?></h1>
                            <p><?php echo $xml->property->propertyaddress;?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid background-default background-stitch-blue">
            <div class="container property-actions">
                <?php
                if(!empty($startDate) && !empty($endDate)){echo'
                <div class="row">
                    <div class="col-12 mb-2">
                        <h6 class="mb-0">Your Booking... <a title="Clear Dates" href="?"><i class="fas fa-times-circle"></i></a></h6>
                        <p>From: '.date("l jS \of F Y", strtotime($startDate)).' - '.$nightslabel.'</p>
                    </div>
                </div>
                ';}?>
                <div class="row">
                    <div class="col-12 col-lg-5">
                        <?php echo $bookNowButton;?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <a href="#" data-toggle="modal" data-target="#availabilityModal" class="propertyAction"><i class="far fa-calendar-check"></i> Check Availability</a>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <a href="/contact/<?php echo $propertyID;?>" class="propertyAction"><i class="far fa-question-circle"></i> Ask a Question</a>
                    </div>
                </div>
            </div>
            <div class="container page-section pt-0">
                <div class="row">
                    <div class="col-12">
                        <p class="lead mb-4 mt-4"><?php echo $xml->property->shortdescription;?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid background-secondary page-section">
            <div class="container">
                <div class="row">
                    <?php foreach ($xml->property->variables->varcat as $element) {
    //Skip loop iteration for Airbnb
    if($element->varcatname == 'Airbnb'){
        continue;
    }

    //Set header icon based on name
    switch($element->varcatname){
        case 'Internal Features':
            $categoryIcon = 'fas fa-tv';
            break;
        case 'External Features':
            $categoryIcon = 'fas fa-tree';
            break;
        case 'Accommodation':
            $categoryIcon = 'fas fa-bed';
            break;
        case 'Location':
            $categoryIcon = 'fas fa-map-marker-alt';
            break;
    }
    //Markup for the Feature list
    echo'
        <div class="col-12 col-md-6 col-lg-3 mb-4 text-center text-md-left">
        <i class="'.$categoryIcon.' fa-2x mb-2 d-block d-md-none"></i>
        <h6 class="mb-3"><i class="'.$categoryIcon.' d-none d-md-inline-block"></i> '.$element->varcatname.'</h6>
        <ul class="propertyFeatures">
        ';
    foreach ($element->varcatitems->varcatitem as $varcatitem){
        echo'<li><span>'.$varcatitem->variable.'</span></li>';
    }
    echo'
        </ul>
        </div>
        ';
}?>
                </div>
            </div>
        </div>
        <?php if(!empty(get360Player($propertyID))){
    $vrPlayerId = get360Player($propertyID);
    echo'
        <div class="container-fluid background-dark page-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="text-blue mb-4">Take a look around!</h2>
                    </div>
                    <div class="col-12">
                        <script src="https://360player.io/static/dist/scripts/embed.js" async></script>
                        <iframe class="photoplayer" src="https://360player.io/p/'.$vrPlayerId.'/" frameborder="0" allowfullscreen data-token="'.$vrPlayerId.'"></iframe>
                    </div>
                </div>
            </div>
        </div>
        ';}?>
        <?php
        $count = 0;
        $imageIDs = array();
        foreach ($xml->property->photos->img as $element) {
            $count ++;
            array_push($imageIDs,$element->photoid);
        }
        if($count >= 4){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[0].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[1].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[2].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6 col-lg-3">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[3].'.jpg)">
                        </div>
                    </div>
                </div>
                <div class="galleryLinkOverlay">
                    <div class="content text-center">
                        <h2 class="mb-2">Photo Gallery</h2>
                        <button class="button m-0" onclick="openGallery()">View '.$count.' Photos</button>
                    </div>
                </div>
            </div>
            ';
        }elseif($count == 3){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-4">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[0].'.jpg)">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[1].'.jpg)">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[2].'.jpg)">
                        </div>
                    </div>
                </div>
                <div class="galleryLinkOverlay">
                    <div class="content text-center">
                        <h2 class="mb-2">Photo Gallery</h2>
                        <button class="button m-0" onclick="openGallery()">View '.$count.' Photos</button>
                    </div>
                </div>
            </div>
            ';
        }elseif($count == 2){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-6">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[0].'.jpg)">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="propertyGalleryImage" style="background-image:url(https://c621446.ssl.cf3.rackcdn.com/images/cottages/'.$imageIDs[1].'.jpg)">
                        </div>
                    </div>
                </div>
                <div class="galleryLinkOverlay">
                    <div class="content text-center">
                        <h2 class="mb-2">Photo Gallery</h2>
                        <button class="button m-0" onclick="openGallery()">View '.$count.' Photos</button>
                    </div>
                </div>
            </div>
            ';
        }elseif($count == 1){
            echo'
            <div class="container-fluid background-default p-0" style="position:relative;">
                <div class="row no-gutters">
                    <div class="col-12">
                        <img src="https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/'.$imageIDs[0].'.jpg" style="width:100%;">
                    </div>
                </div>
            </div>
            ';
        }

        ?>
        <div class="container-fluid background-secondary page-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 webdescription">
                        <?php echo $xml->property->webdescription;?>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="container-fluid background-default page-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="text-blue mb-2">When do you want to stay?</h2>
                        <p class="lead mb-4">Check availability and plan your break</p>
                        <div
                            data-calendar-key="D930D203AB4864A353F4946501F3F717574005C0682BFDF8EB8319B7C671792EE93F2BB906196CAC0DC1A074A9433917830DB9E886D12216"
                            data-calendar-widescreen-months="2"
                            data-calendar-property-id="<?php echo $propertyID;?>">
                            Your widget will appear here.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    -->
        <div class="container-fluid background-default map-section">
            <div id="map"></div>
            <script>
                var map;
                function initMap() {
                    var location = {lat: <?php echo $xml->property->latitude;?>, lng: <?php echo $xml->property->longitude;?>};
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: location,
                        zoom: 17,
                        styles: [
                            {
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ebe3cd"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#523735"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#f5f1e6"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#c9b2a6"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#dcd2be"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#ae9e90"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.neighborhood",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dfd2ae"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dfd2ae"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#93817c"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#a5b076"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#447530"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#f5f1e6"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#fdfcf8"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#f8c967"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#e9bc62"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway.controlled_access",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e98d58"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway.controlled_access",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#db8555"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#806b63"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dfd2ae"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#8f7d77"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#ebe3cd"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dfd2ae"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#b9d3c2"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#92998d"
                                    }
                                ]
                            }
                        ]
                    });
                    var marker = new google.maps.Marker({
                        position: location,
                        map: map
                    });
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABc5r_DEMHXtnMJPHsABqg1pHwaSlaf_k&callback=initMap"
                    async defer></script>
        </div>

<!-- Availability Search Modal -->
<div class="modal fade" id="availabilityModal" tabindex="-1">
  <div class="modal-dialog modal-lg" style="max-width: 1140px;">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="text-blue">When do you want to stay?</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="padding-top:0;">
      <div
                            data-calendar-key="D930D203AB4864A353F4946501F3F717574005C0682BFDF8EB8319B7C671792EE93F2BB906196CAC0DC1A074A9433917830DB9E886D12216"
                            data-calendar-widescreen-months="2"
                            data-calendar-property-id="<?php echo $propertyID;?>">
                            Your widget will appear here.
                        </div>
      </div>
    </div>
  </div>
</div>
<!-- /Availability Search Modal -->

        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <script>
            function openGallery(){
                $.fancybox.open([
                    <?php
                    foreach ($xml->property->photos->img as $element) {
                        echo"
                        {
                            src  : 'https://c621446.ssl.cf3.rackcdn.com/images/cottages/custom/large/".$element->photoid.".jpg',
                            opts : {
                                thumb   : 'https://c621446.ssl.cf3.rackcdn.com/images/cottages/".$element->photoid.".jpg'
                            }
                        },
                        ";
                    }
                    ?>
                ], {
                    loop : true,
                    protect: true,
                    thumbs : {
                        autoStart : false
                    },
                    buttons: [
                        "zoom",
                        "slideShow",
                        "thumbs",
                        "close"
                    ],
                    idleTime: 0
                });
            }
        </script>
        <?php echo $openBookingCartModalScript;?>
    </body>
</html>
