<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Contact us';            //Page Title
$pageSlug = 'contact';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'Got a question or concern? We would love to hear from you!';   //Meta Description

if(isset($_GET['id'])){
    $xml=simplexml_load_file("https://api.supercontrol.co.uk/xml/property_xml.asp?siteID=42986&id=".$_GET['id']) or die("Error: Cannot create object");

    $propertyName = $xml->property->propertyname;
}else{
    $propertyName = '';
}
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 50%,rgb(255, 255, 255) 100%),url(/images/beach0.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center">
                        <h1>Contact us</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section  background-default">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 text-center text-md-left mb-4">
                        <h2 class="text-blue mb-4">Got a question or concern?</h2>
                        <p class="lead mb-0">We'd love to hear from you!</p>
                        <p class="lead">Contact us with the details below, <br>or use the contact form.</p>
                        <p class="mb-0">Email: <a href="mailto:info@cambergetaways.co.uk">info@cambergetaways.co.uk</a></p>
                        <p>Phone: <a href="tel:01797227431">01797 227431</a></p>
                        <p class="mb-0"><strong>Office Hours:</strong></p>
                        <p class="mb-0">Mon-Fri: 9:00 - 17:00</p>
                        <p class="mb-0">Sat: 10:00 - 13:00</p>
                        <p>Sun: Closed</p>
                        <p class="mb-0"><strong>During Summer Holidays:</strong></p>
                        <p class="mb-0">Mon, Fri: 9:00 - 19:00</p>
                        <p class="mb-0">Tue, Wed, Thu: 9:00 - 17:00</p>
                        <p class="mb-0">Sat: 10:00 - 13:00</p>
                        <p>Sun: Closed</p>
                        <p>84 Fishmarket Rd, <br>Rye TN31 7LP</p>
                        <ul class="social-icons">
                            <li><a href="https://www.facebook.com/Cambergetaways/" target="_blank"><i class="fab fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/cambergetaways/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                        <p class="mt-4 mb-0"><small><strong>UK GETAWAYS LTD</strong></small></p>
                        <p class="my-0"><small>Company Number: 10535597</small></p>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="contact-form" style="background-image: linear-gradient(to top, #48c6ef 0%, #6f86d6 100%); color:white;">
                            <?php if(isset($_GET['id'])){echo'
                            <div class="propertyNote">
                                <p><i class="fas fa-info-circle"></i> You are contacting us about:</p><p><strong>'.$propertyName.'</strong></p>
                            </div>
                            ';}?>
                            <form id="contactForm">
                                <input id="propertyName" type="hidden" name="propertyName" value="<?php echo $propertyName;?>">
                                <div class="form-group">
                                    <label>Your Name</label>
                                    <input id="name" name="name" type="text" class="form-control custom-form-control">
                                </div>
                                <div class="form-group">
                                    <label>Your Email Address</label>
                                    <input id="email" name="email" type="email" class="form-control custom-form-control">
                                </div>
                                <div class="form-group">
                                    <label>Your Message</label>
                                    <textarea id="message" name="message" class="form-control custom-form-control" rows="5" style="height:auto!important;"></textarea>
                                </div>
                                <div class="form-group">
                                    <button id="submit" type="button" class="button button-search m-0 mt-2" onclick="submitForm()">Send <i class="fas fa-paper-plane"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center text-md-left">
                        <h2>How to find us:</h2>
                        <?php graphite_pageSection('5N5DP36KR2P');?>
                        <style>
                            #map{
                                width: 100%;
                                height: 400px;
                                border-radius: 10px;
                            }
                        </style>
                        <div id="map"></div>
                        <a target="_blank" href="https://goo.gl/maps/YpLCw8gaqAU2" class="button m-0 mt-4">Get Directions</a>
                        <script>
                            var map;
                            function initMap() {
                                var location = {lat: 50.95325938034893, lng: 0.736192928859401};
                                
                                map = new google.maps.Map(document.getElementById('map'), {
                                    center: location,
                                    zoom: 17
                                });
                                var marker = new google.maps.Marker({
                                    position: location,
                                    map: map
                                });
                            }
                        </script>
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABc5r_DEMHXtnMJPHsABqg1pHwaSlaf_k&callback=initMap"
                                async defer></script>
                    </div>
                </div>

				<div class="row align-items-center" style="padding:30px; margin-left:0px; margin-right:0; margin-top:100px; background:#0092d6;color:white;border-radius:10px; ">
                    <div class="col-12 col-lg-6 my-2">
                        <h2 class="mb-0">Subscribe to our mailing list</h2>
                        <p class="mb-0">For Latest News, Offers and more!</p>
                    </div>
                    <div class="col-12 col-lg-6  my-2 text-center text-lg-right">
                        <div id="mc_embed_signup">
                            <form action="https://cambergetaways.us10.list-manage.com/subscribe/post?u=844ff14c1e3a54ac681a558a8&amp;id=33e6f4b52c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="row">
                                        <div class="col-12 col-lg-8 my-2">
                                            <div class="form-group mb-0">
                                                <input type="email" value="" name="EMAIL" class="form-control custom-form-control" id="mce-EMAIL" required autocomplete="email" placeholder="Your Email Address">
                                            </div>
                                            <div id="mce-responses" class="clear">
                                                <div class="response" id="mce-error-response" style="display:none"></div>
                                                <div class="response" id="mce-success-response" style="display:none"></div>
                                            </div>
                                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_844ff14c1e3a54ac681a558a8_4af90fffa8" tabindex="-1" value=""></div>
                                        </div>
                                        <div class="col-12 col-lg-4 my-2">
                                            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button mr-0 mb-0" style="background:white;border:none;color:#0092d6;width:100%;height:54px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="/js/contactForm.js"></script>
    </body>
</html>
