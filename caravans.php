<?php //PAGE CONFIG
$ROOTLOCATION = '';         //Root Location (E.g. '../../')
require_once $ROOTLOCATION.'php/execute.php';

$pageTitle = 'Caravans for Sale';            //Page Title
$pageSlug = 'caravans-for-sale';             //Page Slug
$menuTransparent = true;

$seoActive = true;         //Enable SEO Meta Tags (true/false)
$seoMetaURL = '';           //Website URL (Default: CURRENT-URL)
$seoMetaImageURL = '';      //SEO Image Link (Default: ROOT-URL/images/seo.jpg)
$seoMetaDescription = 'We work alongside Parkdean Resorts to offer you a selection of Caravans and Lodges to buy in Camber Sands. By working directly with the park we can help secure you VIP treatment and excellent prices.';   //Meta Description
?>
<!DOCTYPE html>
<html>
    <?php include_once $ROOTLOCATION.'includes/head.php';?>
    <?php include_once $ROOTLOCATION.'includes/analytics.php';?>
    <body>
        <?php include_once $ROOTLOCATION.'includes/navbar.php';?>
        <div class="container-fluid hero hero-50" style="background: linear-gradient(rgba(0, 0, 0, 0.0),rgba(0, 0, 0, 0.0)),url(/images/caravansforsale.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12 align-self-center text-center text-md-right">
                        <h1>Caravans</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default background-stitch-blue">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <p>Below are a selection of the properties currently available. Please note that these change on a regular basis, if you don't find what you're looking for, feel free to reach out to us for more information.</p>
                        <a href="/contact" class="button m-0 mt-4">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid page-section background-default pt-0">
            <div class="container">
                <div class="row justify-content-center">
                    
                    <?php getCaravans();?>
                </div>
            </div>
        </div>
        <?php include_once $ROOTLOCATION.'includes/footer.php';?>
        <?php include_once $ROOTLOCATION.'includes/scripts.php';?>
        <?php graphite_GraphiteFooter();?>
    </body>
</html>